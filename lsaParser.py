from docx import Document
from docx.shared import RGBColor
import numpy as np
import collections
from collections import OrderedDict
import re
import math
import datetime
from openpyxl import Workbook
from openpyxl.styles import Font, Alignment
from openpyxl.utils import get_column_letter
import os
import statistics
from statistics import mode

# excel file constants
conn_sum_string = 'connection sums with others'
self_conn_string = 'self connection'
act_sum_string_units = 'unit activation sums'
act_sum_string_cycles = 'cycle activation sums'
min_col_width = 4
decimal_round = 4
indentation = 2
darkgreen = 'FF008000'
darkred = 'FF800000'
darkblue = 'FF000080'

parameter_explanations = {'input_path': 'path of docx file for input',
                          'output_path': 'path for output folder',
                          'output_name': 'name for output folder',
                          'default_config_path': 'path for default parameters',
                          'load_config_path': 'path for loading parameters',
                          'save_config_path': 'path for saving parameters',
                          'decimal_round': 'number of decimal digits to round results to',
                          # 'initial_connection_matrix': 'connection matrix initialized using LSA',
                          'lsaChoice': 'LSA general reading up to grade',
                          'loaded_connection_matrix': 'load initial connections from file',
                          'connection_matrix_path': 'initial connections file path',
                          'edited_text_type': 'what type of document to save as edited output',
                          'is_mem_cap': 'using a working memory cap',
                           'is_keep_reading_max' : 'using keep reading max',
                          'is_keep_cap_max' : 'using keep CAP max',
                          'is_without_correction': 'without correction of cap memory',
                          'working_mem_cap': 'working memory cap',
                          'm': 'mention value',
                          'learning_rate': 'learning rate',
                          'cohort': 'cohort value',
                          'decay': 'decay value',
                          'activation_threshold': 'activation threshold',
                          'semantic_weight': 'semantic weight',
                          'episodic_weight': 'episodic weight',
                          'connection_threshold': 'connection threshold',
                          # 'self_activation': 'allowing self activation',
                          'forward_activation': 'allowing forward activation',
                          'blank_cycle': 'using a blank cycle',
                          'negative_connections': 'allowing negative connections',
                          'reduced_expectancy': 'using reduced expectancy',
                          'linear_function': 'using a linear sigmoid',
                          'sigmoid_connections': 'allowing sigmoid connections',
                          'sigmoid_min': 'minimal sigmoid value',
                          'sigmoid_max': 'maximal sigmoid value',
                          'remove_apostrophe': 'removes apostrophes',
                          'contractions_replace': 'replacing words with apostrophes',
                          'numbers_replace': 'replacing numbers with words',
                          'ordinals_replace': 'replacing ordinals with words',
                          'limit_cycles': 'limiting cycles to a certain number',
                          'iteration_limit': 'the actual number for the cycle limit',
                          'word_is_cycle': 'every word is its own cycle',
                          'case_sensitive': 'all words are case sensitive',
                          'ignored_words': 'ignored words',
                          'ignored_chars': 'ignored characters',
                          'replaced_words': 'words to replace',
                          'replaced_chars': 'characters to replace',
                          'ignored_words_inactive': 'inactive ignored words',
                          'ignored_chars_inactive': 'inactive ignored characters',
                          'replaced_words_inactive': 'inactive words to replace',
                          'replaced_chars_inactive': 'inactive characters to replace',
                          'same_folder': 'save output files in input folder',
                          'append_datetime': 'append date and time to output file names',
                          'show_all_columns': 'show all columns in output summary',
                          'show_columns': 'show specific columns in output summary',
                          'error_flag': 'error during simulation',
                          'error_text': 'error message',
                          'summary_text': 'summary message',
                          'unrecognised_words': 'words not recognised by LSA website or import file',
                          'spare_parameter': '?',
                          'irregular_text_id': 'irregular text id by number of sub titles'}

parameter_hidden = ['error_flag',
                    'error_text',
                    'summary_text',
                    'unrecognised_words',
                    'irregular_text_id',
                    'spare_parameter',
                    'edited_text_type',
                    'remove_apostrophe',
                    'case_sensitive',
                    'same_folder',
                    'append_datetime',
                    'show_all_columns',
                    'show_columns',
                    'ignored_words',
                    'ignored_chars',
                    'replaced_words',
                    'replaced_chars',
                    'ignored_words_inactive',
                    'ignored_chars_inactive',
                    'replaced_words_inactive',
                    'replaced_chars_inactive']


# a block is one text to be analyzed, from and entire document cvontaining multiple texts
# members:
#   id = number of block in the file, by order of writing
#   title = the title of the text if given
#   file_title = a title of the text with its id if a title is given (otherwise just id)
#   inferred_units = a list of inferred units
#   target_units = a list of target units
#   cycles = a list of all the sentences in the text, ignored words and chars removed
#
#   activation matrix = will hold the final activation matrix after the algorithm
#   connection matrix = will hold the final connection matrix after the algorithm
#   lsa matrix = will hold the initial lsa colorado connection matrix if imported (otherwise value is None)
#   index2word = a dictionary that when given an index (int), yields the matching word (string)
#   word2index = a dictionary that when given a word (string), yields the matching index (int)
#   cycle2words = a dictionary that when given a cycle (int), yields a matching list of words in it (list of strings)
#   word2cycles = a dictionary that when given a word (string), yields a matching list of cycle it is in (list of ints)
class Block:
    # block members
    id = 0
    title = ''
    file_title = ''
    inferred_units = []
    target_units = []
    units = []
    cycles = []
    conditions = []

    # data members
    activation_matrix = None
    connection_matrix = None
    lsa_matrix = None
    index2word = OrderedDict()
    word2index = OrderedDict()
    cycle2words = OrderedDict()
    word2cycles = OrderedDict()

    delimiter = '%del%'

    def __init__(self, id, title, inferred_units, target_units, cycles, cond_list=[], delim='%del%'):
        self.id = id
        self.title = title.replace('/', '').replace('\\', '')
        temp = title.replace('/', ';').replace('\\', ';')
        self.sub_titles = temp.split(';')
        for sub_title in self.sub_titles:
            if sub_title == "":
                self.sub_titles.remove(sub_title)
        self.inferred_units = inferred_units
        self.target_units = target_units
        self.cycles = cycles
        self.delimiter = delim

        if cond_list:
            self.conditions = cond_list

        # file title will be id if there was no given title
        if title == '':
            self.file_title = str(id)

        # file title will be id-title if there was a given title (e.g. "2-love story")
        else:
            self.file_title = str(id) + '-' + self.title

        # makes an ordered list of units from all words in each cycle in the block, no repetitions
        units = []
        for cycle in cycles:
            units.extend(cycle.split())

        self.units = list(OrderedSet(units))

        rows = len(cycles)
        cols = len(units)

        # initializing matrix members
        self.activation_matrix = np.zeros((rows, cols), dtype=float)
        self.connection_matrix = np.zeros((cols, cols), dtype=float)
        self.lsa_matrix = np.zeros((cols, cols), dtype=float)

        # initializing ordered dictionary members
        self.index2word = OrderedDict()
        self.word2index = OrderedDict()
        self.cycle2words = OrderedDict()
        self.word2cycles = OrderedDict()

    def __repr__(self):
        return self.file_title

    def __len__(self):
        return len(self.cycles)


# ordered set class from the web
class OrderedSet(collections.MutableSet):
    def __init__(self, iterable=None):
        self.end = end = []
        end += [None, end, end]  # sentinel node for doubly linked list
        self.map = {}  # key --> [key, prev, next]
        if iterable is not None:
            self |= iterable

    def __len__(self):
        return len(self.map)

    def __contains__(self, key):
        return key in self.map

    def add(self, key):
        if key not in self.map:
            end = self.end
            curr = end[1]
            curr[2] = end[1] = self.map[key] = [key, curr, end]

    def discard(self, key):
        if key in self.map:
            key, prev, next = self.map.pop(key)
            prev[2] = next
            next[1] = prev

    def __iter__(self):
        end = self.end
        curr = end[2]
        while curr is not end:
            yield curr[0]
            curr = curr[2]

    def __reversed__(self):
        end = self.end
        curr = end[1]
        while curr is not end:
            yield curr[0]
            curr = curr[1]

    def pop(self, last=True):
        if not self:
            raise KeyError('set is empty')
        key = self.end[1][0] if last else self.end[2][0]
        self.discard(key)
        return key

    def __repr__(self):
        if not self:
            return '%s()' % (self.__class__.__name__,)
        return '%s(%r)' % (self.__class__.__name__, list(self))

    def __eq__(self, other):
        if isinstance(other, OrderedSet):
            return len(self) == len(other) and list(self) == list(other)
        return set(self) == set(other)


# utility function for column width manipulation in xlsx files
def as_text(value):
    if value is None:
        return ""

    return str(value)


# utility function to round up the width of columns in xlsx files
def round_width(value):
    return int(math.ceil(value * 1.05))


# utility function to turn numbers to ordinals in words (e.g. 81 => eighty-first)
def number2ordinal_words(num):
    word2numDict = {'zero': 'zeroth', 'one': 'first', 'two': 'second', 'three': 'third', 'five': 'fifth',
                    'eight': 'eighth', 'nine': 'ninth',
                    'twelve': 'twelfth', 'twenty': 'twentieth', 'thirty': 'thirtieth', 'forty': 'fortieth',
                    'fifty': 'fiftieth',
                    'sixty': 'sixtieth', 'seventy': 'seventieth', 'eighty': 'eightieth', 'ninety': 'ninetieth'}
    w = number2words(num)
    i = len(w) - 1
    contBool = True
    while contBool and i > -1:
        contBool = (w[i] != ' ') and (w[i] != '-')
        i = i - 1
    i = i + 2
    if contBool:
        i = 0
    wPrev = ''
    if i > 0:
        wPrev = w[0:i]
    wNext = w[i:len(w)]
    if (wNext in word2numDict.keys()):
        wNext = word2numDict[wNext]
    else:
        wNext = wNext + 'th'
    return wPrev + wNext


# utility function to turn ordinals to words (e.g. 81st => eighty-first)
def ordinal2words(ordinal):
    num = int(ordinal[:-2])
    return number2ordinal_words(num)


# utility function to turn numbers to words (e.g. 81 => eighty-one)
def number2words(num):
    num2words1 = {1: 'One', 2: 'Two', 3: 'Three', 4: 'Four', 5: 'Five',
                  6: 'Six', 7: 'Seven', 8: 'Eight', 9: 'Nine', 10: 'Ten',
                  11: 'Eleven', 12: 'Twelve', 13: 'Thirteen', 14: 'Fourteen',
                  15: 'Fifteen', 16: 'Sixteen', 17: 'Seventeen', 18: 'Eighteen', 19: 'Nineteen'}
    num2words2 = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety']

    ansNeg = ''
    if num < 0:
        ansNeg = 'negative '
        num = -num
    if num == 0:
        return 'zero'

    if 1 <= num <= 19:
        return num2words1[num].lower()
    elif 20 <= num <= 99:
        tens, below_ten = divmod(num, 10)

        if below_ten == 0:
            return ansNeg + num2words2[tens - 2].lower()

        return ansNeg + (num2words2[tens - 2] + '-' + num2words1[below_ten]).lower()
    elif num == 100:
        return ansNeg + 'one hundred'


# a function that replaces words within a text according to a dictionary
def replace_words(text, replace_dic, delim='%del%'):
    results = text
    temp_pars = []
    for par in text.split(delim):
        temp_lines = []
        for line in par.splitlines():
            temp_words = []
            for word in line.split():
                if word.lower() in replace_dic:
                    temp_words.append(replace_dic[word.lower()])
                else:
                    temp_words.append(word)
            temp_lines.append(' '.join(temp_words))
        temp_pars.append('\n'.join(temp_lines))
    results = delim.join(temp_pars)
    return results


# a function that removes words in a text according to a list
def remove_words(text, ignored_words, delim='%del%'):
    results = text
    temp_pars = []
    for par in text.split(delim):
        temp_lines = []
        for line in par.splitlines():
            temp_words = []
            for word in line.split():
                if not word.lower() in ignored_words:
                    temp_words.append(word)
            temp_lines.append(' '.join(temp_words))
        temp_pars.append('\n'.join(temp_lines))
    results = delim.join(temp_pars)
    return results


# a function that replaces chars in a string according to a dictionary
def replace_chars(text, replaced_dic):
    new_text = text
    for old in replaced_dic.keys():
        new_text = new_text.replace(old, replaced_dic[old])
    return new_text


# a function that removes chars from a string according to a list
def remove_chars(text, removed_chars):
    new_text = text
    for i in removed_chars:
        new_text = new_text.replace(i, "")
    return new_text


# replaces all numbers (from 0 to 100) in a string with words
def replace_numbers(text, delim='%del%', newline_flag=True):
    results = ""

    temp_pars = []
    for par in text.split(delim):
        temp_lines = []
        for line in par.splitlines():
            text_words = line.split()

            temp_words = []
            for word in text_words:
                word2 = word
                w = word.replace(',', '')
                try:
                    num = int(w)

                    if num <= 100 and num >= 0:
                        word2 = number2words(num)
                    else:
                        word2 = word

                except ValueError:
                    word2 = word

                temp_words.append(word2)

            # temp_lines.append(' '.join(temp_words))
            temp_lines.append(' '.join(str(i) for i in temp_words))
        # temp_pars.append('\n'.join(temp_lines))
        temp_pars.append('\n'.join([str(i) for i in temp_lines]))
    results = delim.join(temp_pars)
    if newline_flag:
        results += "\n"

    return results


# replaces all ordinals (from 1st to 100th) in a string with words
def replace_ordinals(text, delim='%del%', newline_flag=True):
    results = ""
    ordinalPoss = ['0th', '1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th']

    temp_pars = []
    for par in text.split(delim):
        # print(par)
        temp_lines = []
        for line in par.splitlines():


            text_words = line.split()

            temp_words = []
            for word in text_words:
                word2 = word
                w = word.replace(',', '')
                wordLen = len(w)
                if wordLen > 2:
                    if w[wordLen - 3:wordLen].lower() in ordinalPoss:
                        try:
                            num = int(w[:-2])
                            if num <= 100 and num >= 0:
                                word2 = ordinal2words(w)
                            else:
                                word2 = word

                        except ValueError:
                            word2 = word

                temp_words.append(word2)

            temp_lines.append(' '.join(temp_words))
        temp_pars.append('\n'.join(temp_lines))
    results = delim.join(temp_pars)
    if newline_flag:
        results += "\n"

    return results


def remove_empty_cycles(text, delim='%del%', newline_flag=True):
    results = ""

    temp_pars = []
    for par in text.split(delim):
        emptyCycle = True
        i = 0
        while emptyCycle and i < len(par):
            currentChar = str(par[i])
            if not (currentChar in [' ', '\t', '\n']):
                emptyCycle = False
            i = i + 1
        if emptyCycle == False:
            temp_pars.append(par)

    results = delim.join(temp_pars)
    if newline_flag:
        results += "\n"

    return results


# returns a list of blocks from the original text file
def parse(path, params, delimiter='%del%'):
    # a delimiter for parsing text
    lines = []
    i = len(path) - 1
    while i > -1 and path[i] != '.':
        i = i - 1
    inputType = path[i + 1:len(path)]

    if inputType == "docx" or inputType == "doc":
        try:
            doc = Document(path)
        except:
            return []
        for para in doc.paragraphs:
            for linePara in para.text.split('\n'):
                if linePara == "":
                    lines.append(delimiter)
                else:
                    lines.append(linePara)
            lines.append(delimiter)
    else:
        try:
            docTxt = open(path, "r")
        except:
            return []
        for line in docTxt.read().split('\n'):
            if line == "":
                lines.append(delimiter)
            else:
                lines.append(line)
        docTxt.close()

    ignored_chars = params['ignored_chars']
    ignored_words = params['ignored_words']
    replaced_chars = params['replaced_chars']
    replaced_words = params['replaced_words']
    # the final list of texts to be returned
    blocks = []

    # creating a string of every paragraph in the document, divided by a newline
    full_text = ""
    commentLines_text = ""
    titleLines = []
    titleSearch = False
    for linePara in lines:
        if len(linePara) > 0:
            if linePara[0] == "&":
                commentLines_text = commentLines_text + linePara + "\n"
                full_text = full_text + linePara + "\n"
                if titleSearch:
                    titleLines.append("")
                titleSearch = True
            else:
                # solamit
                if linePara[0] == "#" or (linePara[0] == "*" and titleSearch):
                    commentLines_text = commentLines_text + linePara + "\n"
                    if linePara[0] == "*" and titleSearch:
                        titleLines.append(linePara)
                        titleSearch = False
                else:
                    full_text = full_text + linePara + "\n"
        else:
            full_text = full_text + "\n"
    if titleSearch:
        titleLines.append("")
    # apostrophe fix, ’ to '
    full_text = full_text.replace("’", "'")
    full_text = full_text.replace("`", "'")

    # dictionary to replace n't words with "not"
    contraction_replacements = {"didnt": "did not",
                                "doesnt": "does not",
                                "havent": "have not",
                                "hadnt": "had not",
                                "hasnt": "has not",
                                "dont": "do not",
                                "couldnt": "could not",
                                "wasnt": "was not",
                                "wouldnt": "would not",
                                "shouldnt": "should not",
                                "wont": "will not",
                                "cant": "can not",
                                "shant": "shall not",
                                "isnt": "is not",
                                "arent": "are not",
                                "werent": "were not",
                                "didn't": "did not",
                                "doesn't": "does not",
                                "haven't": "have not",
                                "hadn't": "had not",
                                "hasn't": "has not",
                                "don't": "do not",
                                "couldn't": "could not",
                                "wasn't": "was not",
                                "wouldn't": "would not",
                                "shouldn't": "should not",
                                "won't": "will not",
                                "can't": "can not",
                                "cannot": "can not",
                                "shan't": "shall not",
                                "isn't": "is not",
                                "aren't": "are not",
                                "weren't": "were not",
                                "who's": "who is",
                                "who'd": "who would",
                                "who've": "who have",
                                "could've": "could have",
                                "would've": "would have",
                                "should've": "sould have",
                                "might've": "might have",
                                "mightn't": "might not",
                                "im": "i am",
                                "Im": "I am",
                                "hes": "he is",
                                "shes": "she is",
                                "theyre": "they are",
                                "youre": "you are",
                                "lets": "let us",
                                "theyll": "they will",
                                "youll": "you will",
                                "itll": "it will",
                                "itd": "it would",
                                "Ive": "I have",
                                "ive": "i have",
                                "youve": "you have",
                                "shes": "she has",
                                "hes": "he has",
                                "theyve": "they have",
                                "hed": "he would",
                                "shed?": "she would",
                                "theyd": "they would",
                                "youd": "you would",
                                "i'm": "i am",
                                "I'm": "I am",
                                "he's": "he is",
                                "she's": "she is",
                                "they're": "they are",
                                "you're": "you are",
                                "let's": "let us",
                                "I'll": "I will",
                                "i'll": "i will",
                                "he'll": "he will",
                                "she'll": "she will",
                                "they'll": "they will",
                                "you'll": "you will",
                                "it'll": "it will",
                                "it'd": "it would",
                                "it's": "it is",
                                "I've": "I have",
                                "i've": "i have",
                                "you've": "you have",
                                "she's": "she has",
                                "he's": "he has",
                                "they've": "they have",
                                "I'd": "I would",
                                "i'd": "i would",
                                "he'd": "he would",
                                "she'd": "she would",
                                "they'd": "they would",
                                "you'd": "you would",
                                "whos": "who is",
                                "whod": "who would",
                                "whove": "who have",
                                "couldve": "could have",
                                "wouldve": "would have",
                                "shouldve": "sould have",
                                "mightve": "might have",
                                "mightnt": "might not",
                                "?Ill?": "I will",
                                "?ill?": "i will",
                                "?hell?": "he will",
                                "?shell?": "she will",
                                "?Id?": "I would",
                                "?id?": "i would"}

    # lowercase everything if it is not sensitive
    if not params['case_sensitive']:
        full_text = full_text.lower()

    # replacing defined words
    if bool(replaced_words):
        full_text = replace_words(full_text, replaced_words, delimiter)

    # removing the ignored words and chars
    full_text = remove_chars(full_text, ignored_chars)

    # replacing contractions (words with apostrophies) with full word
    if bool(contraction_replacements):
        if params['contractions_replace']:
            full_text = replace_words(full_text, contraction_replacements, delimiter)

    # removing remove_apostrophe
    if params['remove_apostrophe']:
        full_text = remove_chars(full_text, ["\'"])

    # replacing characters
    if bool(replaced_chars):
        full_text = replace_chars(full_text, replaced_chars)
    full_text = remove_words(full_text, ignored_words)

    # replacing numbers (0-100) with words
    if params['numbers_replace']:
        full_text = replace_numbers(full_text, delimiter)

    # replacing ordinals (1st-100th) with words
    if params['ordinals_replace']:
        full_text = replace_ordinals(full_text, delimiter)

    # gets rid of all whitespaces that are not a single space
    full_text = re.sub('\s+', ' ', full_text).strip()

    # gets rid of all empty cycles
    full_text = remove_empty_cycles(full_text, delimiter, False)

    # splits the text into a list of blocks
    blocks_list = list(filter(None, full_text.split('&')))

    i = 1
    for b in blocks_list:
        block = list(filter(None, b.split(delimiter)))

        for sentence in block:
            if not sentence.strip(' \t\n\r'):
                block.remove(sentence)

        for sentence in block:
            block[block.index(sentence)] = sentence.strip(' \t\n\r')

        title = ''
        inferred_units = []
        target_units = []
        cond_list = []

        if len(titleLines[i - 1]) > 1:
            title = titleLines[i - 1][1:]
            if '*' in title:
                cond_list = title.split('*')

                for cond in cond_list:
                    if cond == '':
                        cond_list.remove(cond)

                title = ''.join(cond_list)

        # flag for when a inferred or target word is found
        special_word = False

        # iterating over every sentence in a block
        sentenceIndex = 0
        for sentence in block:
            sentence = sentence.strip(' \t\n\r')

            # disregard empty sentences
            if not sentence:
                continue

            block[sentenceIndex] = sentence

            # iterating over every unit in a sentence to collect inferred and target words
            for word in [word.strip(' \t\n\r') for word in sentence.split()]:
                if len(word) > 1:
                    if word != "0@" and word != "@0":
                        if word[0] == '0':
                            special_word = True

                            if word[1] == '@':
                                target_units.append(word[2:])
                                inferred_units.append(word[2:])
                            else:
                                inferred_units.append(word[1:])

                        if word[0] == '@':
                            special_word = True

                            if word[1] == '0':
                                inferred_units.append(word[2:])
                                target_units.append(word[2:])
                            else:
                                target_units.append(word[1:])

            # if an inferred or target word was found, remove the chars that mark it from the sentence
            if special_word:
                special_replace = {}
                for specialw in inferred_units:
                    special_replace['0' + specialw] = specialw
                    special_replace['0@' + specialw] = specialw
                for specialw in target_units:
                    special_replace['@' + specialw] = specialw
                    special_replace['@0' + specialw] = specialw
                block[sentenceIndex] = replace_words(sentence, special_replace, delimiter)
                special_word = False
            sentenceIndex = sentenceIndex + 1

        new_block = block

        if params['word_is_cycle']:
            new_block = []
            for cycle in block:
                new_block.extend(cycle.split())

                # if params['blank_cycle']:
                #     temp_list = []
                #     units = []
                #     first = 1
                #     # temp_list.append(new_block[0])
                #     extra_cycle = ""
                #     for block in new_block:
                #         block = block.split(' ')
                #         for unit in block:
                #             if not unit in units:
                #                 units.append(unit)
                #                 if first == 1:
                #                     extra_cycle += unit
                #                     first =0
                #                 else:
                #                     extra_cycle += ' '+ unit

                # temp_list.append(extra_cycle)
                # new_block.extend(temp_list)
        blocks.append(Block(i, title, inferred_units, target_units, new_block, cond_list, delimiter))
        i += 1

    return blocks


# sets default parameters
def resetParams():
    params = defaultCodeParams()
    defaultPath = params['default_config_path']
    # general vars
    try:
        doc = open(defaultPath, "r")
    except FileNotFoundError:
        return defaultCodeParams()
    doc.close()
    return configure(defaultPath)


# sets default parameters
def defaultCodeParams():
    # default parameters
    params = OrderedDict()
    # params['input_path'] = in_path
    params['input_path'] = ''
    # params['output_path'] = out_path
    params['output_path'] = ''
    params['output_name'] = ''
    # params['same_folder'] = same_fold
    params['same_folder'] = True
    params['save_config_path'] = 'Default_Parameters1.txt'
    params['load_config_path'] = 'Default_Parameters1.txt'
    params['default_config_path'] = 'parametersDefault.txt'
    params['decimal_round'] = 4
    # params['initial_connection_matrix'] = True
    params['loaded_connection_matrix'] = False
    params['connection_matrix_path'] = ''
    params['lsaChoice'] = 1
    params['case_sensitive'] = False
    params['is_mem_cap'] = True
    params['is_keep_reading_max'] = False
    params['is_keep_cap_max'] = False
    params['is_without_correction'] = True
    params['working_mem_cap'] = 30
    params['m'] = 5
    params['learning_rate'] = 0.9
    params['cohort'] = 0.03
    params['decay'] = 1
    params['activation_threshold'] = 0.0001
    params['connection_threshold'] = 0.0001
    params['semantic_weight'] = 1
    params['episodic_weight'] = 1
    # params['self_activation'] = True
    params['forward_activation'] = True
    params['blank_cycle'] = False
    params['negative_connections'] = True
    params['reduced_expectancy'] = True
    params['linear_function'] = False
    params['sigmoid_connections'] = True
    params['sigmoid_min'] = 0
    params['sigmoid_max'] = 2
    params['remove_apostrophe'] = False
    params['contractions_replace'] = True
    params['numbers_replace'] = True
    params['ordinals_replace'] = True
    params['limit_cycles'] = False
    params['word_is_cycle'] = False
    params['iteration_limit'] = 0
    params['append_datetime'] = False
    params['show_all_columns'] = False
    params['show_columns'] = [True, True, True, True, True, True, True, True, True, True, True, True, True, True, True]
    params['ignored_words'] = ["and", "were", "about", "the", "to", "from", "a", "an", "still", "on", "so", "was", "in",
                               "as", "with", "it", "its", "by", "but", "that", "had", "for", "of", "just", "into ",
                               "at", "when", "despite", "quite", "where", "addition", "within", "would", "there", "be",
                               "almost", "also", "some", "should", "which", "couple", "throughout", "been", "onto",
                               "something", "did", "is", "has", "while", "than", "really", "if", "have", "having",
                               "then", "even", "who", "does", "these", "instead", "are", "least", "due", "such", "able",
                               "will", "willing", "rather", "because", "whether"]
    params['ignored_chars'] = ["'", '"', "''", '.', ',', '?', '!', "\'s", "\'d", "s\'"]
    params['replaced_words'] = {}
    params['replaced_chars'] = {}
    params['ignored_words_inactive'] = []
    params['ignored_chars_inactive'] = []
    params['replaced_words_inactive'] = {}
    params['replaced_chars_inactive'] = {}
    params['error_flag'] = 0
    params['error_text'] = ''
    params['summary_text'] = ''
    params['edited_text_type'] = 'docx'
    params['unrecognised_words'] = []
    params['spare_parameter'] = False
    params['irregular_text_id'] = []
    return params


# pulls data from the configuration file and returns an updated parameters dictionary
def configure(path):
    trueStrings = ['yes', 'true', '1', 'y', 't']
    # default parameters
    params = defaultCodeParams()
    # general vars
    try:
        doc = open(path, "r")
    except FileNotFoundError:
        i = len(path) - 1
        while i > -1 and (path[i] != '\\' and path[i] != '/'):
            i = i - 1
        params['error_flag'] = 2
        params['error_text'] = params['error_text'] + '\nERROR: file ' + path[i + 1:len(path) - 1] + ' not found'
        return params

    # dictionary entries
    ignored_words = []
    ignored_chars = []
    replaced_words = {}
    replaced_chars = {}
    ignored_words_case_sensitive = []
    ignored_chars_case_sensitive = []
    replaced_words_case_sensitive = {}
    replaced_chars_case_sensitive = {}

    ignored_words_inactive = []
    ignored_chars_inactive = []
    replaced_words_inactive = {}
    replaced_chars_inactive = {}
    ignored_words_inactive_case_sensitive = []
    ignored_chars_inactive_case_sensitive = []
    replaced_words_inactive_case_sensitive = {}
    replaced_chars_inactive_case_sensitive = {}

    # converting the entire txt file into a list of lines
    lines = []

    for line in doc.readlines():
        new_line = line.strip()
        ##        if new_line != "":
        ##            lines.append(new_line)
        lines.append(new_line)

    doc.close()

    # iterating over the configuration file lines
    i = 0
    while i < len(lines) - 1:

        # if ignored words is reached, put all of them in a list (until <> is reached)
        if lines[i] == "<ignored words>":
            i += 1
            endFound = False
            while i < len(lines) - 1 and endFound == False:
                if lines[i] == "<end>":
                    endFound = True
                else:
                    if lines[i] != "":
                        ignored_words_case_sensitive.append(lines[i])
                        ignored_words.append(lines[i].lower())
                    i += 1

        # if ignored characters is reached, put all of them in a list (until <> is reached)
        if lines[i] == "<ignored characters>":
            i += 1
            endFound = False
            while i < len(lines) - 1 and endFound == False:
                if lines[i] == "<end>":
                    endFound = True
                else:
                    if lines[i] != "":
                        if lines[i] == "\'" or (lines[i] == "`" or lines[i] == "’"):
                            params['remove_apostrophe'] = True
                        else:
                            ignored_chars_case_sensitive.append(lines[i])
                            ignored_chars.append(lines[i].lower())
                        i += 1

        # if replaced words is reached, put all of them in a list (until <> is reached)
        if lines[i] == "<replaced words>":
            i += 1
            endFound = False
            while i < len(lines) - 2 and endFound == False:
                if lines[i] == "<end>" or lines[i + 1] == "<end>":
                    endFound = True
                    if lines[i + 1] == "<end>":
                        i += 1
                else:
                    if lines[i] != "":
                        replaced_words_case_sensitive[lines[i]] = lines[i + 1]
                        replaced_words[lines[i].lower()] = lines[i + 1].lower()
                    i += 2

        # if replaced characters is reached, put all of them in a list (until <> is reached)
        if lines[i] == "<replaced characters>":
            i += 1
            endFound = False
            while i < len(lines) - 2 and endFound == False:
                if lines[i] == "<end>" or lines[i + 1] == "<end>":
                    endFound = True
                    if lines[i + 1] == "<end>":
                        i += 1
                else:
                    if lines[i] != "":
                        replaced_chars_case_sensitive[lines[i]] = lines[i + 1]
                        replaced_chars[lines[i].lower()] = lines[i + 1].lower()
                    i += 2

        # if inactive ignored words is reached, put all of them in a list (until <> is reached)
        if lines[i] == "<inactive ignored words>":
            i += 1
            endFound = False
            while i < len(lines) - 1 and endFound == False:
                if lines[i] == "<end>":
                    endFound = True
                else:
                    if lines[i] != "":
                        ignored_words_inactive_case_sensitive.append(lines[i])
                        ignored_words_inactive.append(lines[i].lower())
                    i += 1

        # if inactive ignored characters is reached, put all of them in a list (until <> is reached)
        if lines[i] == "<inactive ignored characters>":
            i += 1
            endFound = False
            while i < len(lines) - 1 and endFound == False:
                if lines[i] == "<end>":
                    endFound = True
                else:
                    if lines[i] != "":
                        if lines[i] == "\'" or (lines[i] == "`" or lines[i] == "’"):
                            ignored_chars_inactive_case_sensitive.append("\'")
                            ignored_chars_inactive.append("\'")
                        else:
                            ignored_chars_inactive_case_sensitive.append(lines[i])
                            ignored_chars_inactive.append(lines[i].lower())
                        i += 1

        # if inactive replaced words is reached, put all of them in a list (until <> is reached)
        if lines[i] == "<inactive replaced words>":
            i += 1
            endFound = False
            while i < len(lines) - 2 and endFound == False:
                if lines[i] == "<end>" or lines[i + 1] == "<end>":
                    endFound = True
                    if lines[i + 1] == "<end>":
                        i += 1
                else:
                    if lines[i] != "":
                        replaced_words_inactive_case_sensitive[lines[i]] = lines[i + 1]
                        replaced_words_inactive[lines[i].lower()] = lines[i + 1].lower()
                    i += 2

        # if inactive replaced characters is reached, put all of them in a list (until <> is reached)
        if lines[i] == "<inactive replaced characters>":
            i += 1
            endFound = False
            while i < len(lines) - 2 and endFound == False:
                if lines[i] == "<end>" or lines[i + 1] == "<end>":
                    endFound = True
                    if lines[i + 1] == "<end>":
                        i += 1
                else:
                    if lines[i] != "":
                        replaced_chars_inactive_case_sensitive[lines[i]] = lines[i + 1]
                        replaced_chars_inactive[lines[i].lower()] = lines[i + 1].lower()
                    i += 2

        # show all output columns
        if lines[i] == "<show columns>":
            i += 1
            endFound = False
            i2 = 0
            while (i < len(lines) - 1 and endFound == False) and i2 < 12:
                if lines[i] == "<end>":
                    endFound = True
                else:
                    if lines[i].lower() in trueStrings:
                        params['show_columns'][i2] = True
                    else:
                        params['show_columns'][i2] = False
                    i2 += 1
                    i += 1

        # store default input path
        if lines[i] == "<input path>":
            params['input_path'] = lines[i + 1]

        # store default output path
        if lines[i] == "<output path>":
            params['output_path'] = lines[i + 1]

        # store reset configuration path
        if lines[i] == "<default configuration>":
            params['default_config_path'] = lines[i + 1]

        # store configuration save path
        if lines[i] == "<configuration path save>":
            params['save_config_path'] = lines[i + 1]

        # store configuration load path
        if lines[i] == "<configuration path load>":
            params['load_config_path'] = lines[i + 1]

        # append extra descriptor to output name
        if lines[i] == "<output name>":
            params['output_name'] = lines[i + 1]

        # append time to output name
        if lines[i] == "<append time to output file name>":
            if lines[i + 1].lower() in trueStrings:
                params['append_datetime'] = True
            else:
                params['append_datetime'] = False

        # save edited text as file type
        if lines[i] == "<edited text type>":
            params['edited_text_type'] = lines[i + 1]

        # store output in input folder
        if lines[i] == "<save output in input folder>":
            if lines[i + 1].lower() in trueStrings:
                params['same_folder'] = True
            else:
                params['same_folder'] = False

        # show all output columns
        if lines[i] == "<show all columns>":
            if lines[i + 1].lower() in trueStrings:
                params['show_all_columns'] = True
            else:
                params['show_all_columns'] = False

        # store decimal rounding constant
        if lines[i] == "<output decimal rounding>":
            try:
                params['decimal_round'] = int(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Output decimal rounding must be an integer'

        # # check whether to import connection matrix from lsa colorado site
        # if lines[i] == "<import connection matrix from web>":
        #     if lines[i + 1].lower() in trueStrings:
        #         params['initial_connection_matrix'] = True
        #     else:
        #         params['initial_connection_matrix'] = False

        # store lsa reading choice value
        if lines[i] == "<lsa colorado - general reading up to which year>":
            try:
                params['lsaChoice'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: lsa reading values must be 1, 3, 6 or 12'
            else:
                if params['lsaChoice'] not in [1, 3, 6, 12]:
                    params['error_flag'] = 1
                    params['error_text'] = params['error_text'] + '\nERROR: lsa reading values must be 1, 3, 6 or 12'

        # check whether to load initial connections from a file
        if lines[i] == "<load initial connections from file>":
            if lines[i + 1].lower() in trueStrings:
                params['loaded_connection_matrix'] = True
            else:
                params['loaded_connection_matrix'] = False

        # initial connections file path
        if lines[i] == "<initial connections file path>":
            params['connection_matrix_path'] = lines[i + 1]

        # check if memory cap is used
        if lines[i] == "<use memory cap>":
            if lines[i + 1].lower() in trueStrings:
                params['is_mem_cap'] = True
            else:
                params['is_mem_cap'] = False

        # store memory cap value
        if lines[i] == "<memory cap value>":
            try:
                params['working_mem_cap'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Memory cap value must be a number'

        # check if keep reading max is selected
        if lines[i] == "<use keep reading max>":
            if lines[i + 1].lower() in trueStrings:
                params['is_keep_reading_max'] = True
            else:
                params['is_keep_reading_max'] = False

        # check if keep cap max is selected
        if lines[i] == "<use keep cap max>":
            if lines[i + 1].lower() in trueStrings:
                params['is_keep_cap_max'] = True
            else:
                params['is_keep_cap_max'] = False

        # check if without correction is selected
        if lines[i] == "<use without correction>":
            if lines[i + 1].lower() in trueStrings:
                params['is_without_correction'] = True
            else:
                params['is_without_correction'] = False

        # store mention value
        if lines[i] == "<mention value>":
            try:
                params['m'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Mention value must be a number'

        # store learning rate
        if lines[i] == "<learning rate>":
            try:
                params['learning_rate'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Learning rate must be a number'

        # store cohort value
        if lines[i] == "<cohort value>":
            try:
                params['cohort'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Cohort value must be a number'

        # store decay value
        if lines[i] == "<decay value>":
            try:
                params['decay'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Cohort value must be a number'

        # store activation threshold
        if lines[i] == "<activation threshold>":
            try:
                params['activation_threshold'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Activation threshold must be a number'

        # store semantic weight
        if lines[i] == "<semantic weight>":
            try:
                params['semantic_weight'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Semantic Connection Weight must be a number'

        # store connection threshold
        if lines[i] == "<connection threshold>":
            try:
                params['connection_threshold'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Connection threshold must be a number'

        # store episodic weight
        if lines[i] == "<episodic weight>":
            try:
                params['episodic_weight'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Episodic Connection Weight must be a number'

        # # check if self activation is allowed
        # if lines[i] == "<self activation>":
        #     if lines[i + 1].lower() in trueStrings:
        #         params['self_activation'] = True
        #     else:
        #         params['self_activation'] = False

        # check if self forward is allowed
        if lines[i] == "<forward activation>":
            if lines[i + 1].lower() in trueStrings:
                params['forward_activation'] = True
            else:
                params['forward_activation'] = False

        # check if a blank cycle is used in the end of each text
        if lines[i] == "<blank cycle>":
            if lines[i + 1].lower() in trueStrings:
                params['blank_cycle'] = True
            else:
                params['blank_cycle'] = False

        # check if self negative connections are allowed
        if lines[i] == "<allow negative connections>" or lines[i] == "<allow negtive connections>":
            if lines[i + 1].lower() in trueStrings:
                params['negative_connections'] = True
            else:
                params['negative_connections'] = False

        # check if reduced expectancy is used
        if lines[i] == "<reduced expectancy>":
            if lines[i + 1].lower() in trueStrings:
                params['reduced_expectancy'] = True
            else:
                params['reduced_expectancy'] = False

        # check if a linear function is used instead of sigmoid
        if lines[i] == "<linear function instead of sigmoid>":
            if lines[i + 1].lower() in trueStrings:
                params['linear_function'] = True
            else:
                params['linear_function'] = False

        # check if a sigmoid connections are used
        if lines[i] == "<sigmoid connections>":
            if lines[i + 1].lower() in trueStrings:
                params['sigmoid_connections'] = True
            else:
                params['sigmoid_connections'] = False

        # store sigmoid minimum
        if lines[i] == "<sigmoid minimum>":
            try:
                params['sigmoid_min'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Sigmoid minimum must be a number'

        # store sigmoid maximum
        if lines[i] == "<sigmoid maximum>":
            try:
                params['sigmoid_max'] = float(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Sigmoid maximum must be a number'

        # check if whole text is case sensitive
        if lines[i] == "<case sensitive>":
            if lines[i + 1].lower() in trueStrings:
                params['case_sensitive'] = True
            else:
                params['case_sensitive'] = False

        # check whether to replace not words
        if lines[i] == '<replace contractions>':
            if lines[i + 1].lower() in trueStrings:
                params['contractions_replace'] = True
            else:
                params['contractions_replace'] = False

        # check whether to replace numbers up to 100 with words
        if lines[i] == '<replace numbers up to 100 with words>':
            if lines[i + 1].lower() in trueStrings:
                params['numbers_replace'] = True
            else:
                params['numbers_replace'] = False

        # check whether to replace ordinals up to 100th with words
        if lines[i] == '<replace ordinals up to 100th with words>':
            if lines[i + 1].lower() in trueStrings:
                params['ordinals_replace'] = True
            else:
                params['ordinals_replace'] = False

        # check whether to limit the number of iterations
        if lines[i] == '<limit number of cycles>':
            if lines[i + 1].lower() in trueStrings:
                params['limit_cycles'] = True
            else:
                params['limit_cycles'] = False

        # check spare parameter
        if lines[i] == '<spare parameter>':
            if lines[i + 1].lower() in trueStrings:
                params['spare_parameter'] = True
            else:
                params['spare_parameter'] = False

        # store iteration limit
        if lines[i] == "<iteration limit>":
            try:
                params['iteration_limit'] = int(lines[i + 1])
            except type:
                params['error_flag'] = 1
                params['error_text'] = params['error_text'] + '\nERROR: Iteration limit must be an integer'

        # check whether to make every word it's own cycle
        if lines[i] == '<every word is a cycle>':
            if lines[i + 1].lower() in trueStrings:
                params['word_is_cycle'] = True
            else:
                params['word_is_cycle'] = False

        i += 1

    # populating dictionary
    if params['case_sensitive']:
        params['ignored_words'] = ignored_words_case_sensitive
        params['ignored_chars'] = ignored_chars_case_sensitive
        params['replaced_words'] = replaced_words_case_sensitive
        params['replaced_chars'] = replaced_chars_case_sensitive
        params['ignored_words_inactive'] = ignored_words_inactive_case_sensitive
        params['ignored_chars_inactive'] = ignored_chars_inactive_case_sensitive
        params['replaced_words_inactive'] = replaced_words_inactive_case_sensitive
        params['replaced_chars_inactive'] = replaced_chars_inactive_case_sensitive
    else:
        params['ignored_words'] = ignored_words
        params['ignored_chars'] = ignored_chars
        params['replaced_words'] = replaced_words
        params['replaced_chars'] = replaced_chars
        params['ignored_words_inactive'] = ignored_words_inactive
        params['ignored_chars_inactive'] = ignored_chars_inactive
        params['replaced_words_inactive'] = replaced_words_inactive
        params['replaced_chars_inactive'] = replaced_chars_inactive

    return params


# creates new default configuration file from code default
def saveDefaultConfigurationFile():
    params = defaultCodeParams()
    savePath = os.path.normpath(params['default_config_path'])
    if not savePath.endswith(".txt"):
        savePath = savePath + ".txt"
    path = os.path.normpath(savePath);
    return saveConfigurationFile(path, params)


# creates new configuration file from parameters dictionary
def saveNewConfigurationFile(params):
    savePath = params['save_config_path']
    if not savePath.endswith(".txt"):
        savePath = savePath + ".txt"
    path = os.path.normpath(savePath);
    return saveConfigurationFile(path, params);


# saves configuration file from parameters dictionary
def saveConfigurationFile(path, params):
    ignored_chars = params['ignored_chars']
    ignored_words = params['ignored_words']
    replaced_chars = params['replaced_chars']
    replaced_words = params['replaced_words']
    ignored_chars_inactive = params['ignored_chars_inactive']
    ignored_words_inactive = params['ignored_words_inactive']
    replaced_chars_inactive = params['replaced_chars_inactive']
    replaced_words_inactive = params['replaced_words_inactive']
    configText = ""
    configText += "<input path>\n"
    configText += str(params['input_path'])
    configText += "\n\n<output path>\n"
    configText += str(params['output_path'])
    configText += "\n\n<output name>\n"
    configText += str(params['output_name'])
    configText += "\n\n<append time to output file name>\n"
    configText += str(params['append_datetime'])
    configText += "\n\n<edited text type>\n"
    configText += str(params['edited_text_type'])
    configText += "\n\n<save output in input folder>\n"
    configText += str(params['same_folder'])
    configText += "\n\n<default configuration path>\n"
    configText += str(params['default_config_path'])
    configText += "\n\n<configuration path load>\n"
    configText += str(params['load_config_path'])
    configText += "\n\n<configuration path save>\n"
    configText += str(params['save_config_path'])
    configText += "\n\n<output decimal rounding>\n"
    configText += str(params['decimal_round'])
    # configText += "\n\n<import connection matrix from web>\n"
    # configText += str(params['initial_connection_matrix'])
    configText += "\n\n<lsa colorado - general reading up to which year>\n"
    configText += str(params['lsaChoice'])
    configText += "\n\n<load initial connections from file>\n"
    configText += str(params['loaded_connection_matrix'])
    configText += "\n\n<initial connections file path>\n"
    configText += str(params['connection_matrix_path'])
    configText += "\n\n<use memory cap>\n"
    configText += str(params['is_mem_cap'])
    configText += "\n\n<memory cap value>\n"
    configText += str(params['working_mem_cap'])
    configText += "\n\n<use keep reading max>\n"
    configText += str(params['is_keep_reading_max'])
    configText += "\n\n<use keep cap max>\n"
    configText += str(params['is_keep_cap_max'])
    configText += "\n\n<use without correction>\n"
    configText += str(params['is_without_correction'])
    configText += "\n\n<mention value>\n"
    configText += str(params['m'])
    configText += "\n\n<learning rate>\n"
    configText += str(params['learning_rate'])
    configText += "\n\n<cohort value>\n"
    configText += str(params['cohort'])
    configText += "\n\n<decay value>\n"
    configText += str(params['decay'])
    configText += "\n\n<activation threshold>\n"
    configText += str(params['activation_threshold'])
    configText += "\n\n<semantic weight>\n"
    configText += str(params['semantic_weight'])
    configText += "\n\n<episodic weight>\n"
    configText += str(params['episodic_weight'])

    configText += "\n\n<connection threshold>\n"
    configText += str(params['connection_threshold'])

    # configText += "\n\n<self activation>\n"
    # configText += str(params['self_activation'])
    configText += "\n\n<forward activation>\n"
    configText += str(params['forward_activation'])
    configText += "\n\n<blank cycle>\n"
    configText += str(params['blank_cycle'])
    configText += "\n\n<allow negative connections>\n"
    configText += str(params['negative_connections'])
    configText += "\n\n<reduced expectancy>\n"
    configText += str(params['reduced_expectancy'])
    configText += "\n\n<linear function instead of sigmoid>\n"
    configText += str(params['linear_function'])
    configText += "\n\n<sigmoid connections>\n"
    configText += str(params['sigmoid_connections'])
    configText += "\n\n<sigmoid minimum>\n"
    configText += str(params['sigmoid_min'])
    configText += "\n\n<sigmoid maximum>\n"
    configText += str(params['sigmoid_max'])
    configText += "\n\n<limit number of cycles>\n"
    configText += str(params['limit_cycles'])
    configText += "\n\n<iteration limit>\n"
    configText += str(params['iteration_limit'])
    configText += "\n\n<every word is a cycle>\n"
    configText += str(params['word_is_cycle'])
    ##    configText += "\n\n<spare parameter>\n"
    ##    configText += str(params['spare_parameter'])
    configText += "\n\n<replace contractions>\n"
    configText += str(params['contractions_replace'])
    configText += "\n\n<replace numbers up to 100 with words>\n"
    configText += str(params['numbers_replace'])
    configText += "\n\n<replace ordinals up to 100th with words>\n"
    configText += str(params['ordinals_replace'])
    configText += "\n\n<case sensitive>\n"
    configText += str(params['case_sensitive'])
    configText += "\n\n<show all columns>\n"
    configText += str(params['show_all_columns'])

    configText += "\n\n<show columns>\n"
    for wB in params['show_columns']:
        configText += str(wB)
        configText += "\n"
    configText += "<end>\n\n<replaced words>\n"
    for wR in replaced_words.keys():
        if (wR != ""):
            configText += str(wR)
            configText += "\n"
            configText += str(replaced_words[wR])
            configText += "\n"
    configText += "<end>\n\n<replaced characters>\n"
    for wR in replaced_chars.keys():
        if (wR != ""):
            configText += str(wR)
            configText += "\n"
            configText += str(replaced_chars[wR])
            configText += "\n"
    configText += "<end>\n\n<ignored words>\n"
    for wR in ignored_words:
        if (wR != ""):
            configText += str(wR)
            configText += "\n"
    configText += "<end>\n\n<ignored characters>\n"
    if params['remove_apostrophe']:
        configText += "\'\n";
    for wR in ignored_chars:
        if (wR != ""):
            configText += str(wR)
            configText += "\n"
    configText += "<end>\n\n<inactive replaced words>\n"
    for wR in replaced_words_inactive.keys():
        if (wR != ""):
            configText += str(wR)
            configText += "\n"
            configText += str(replaced_words_inactive[wD])
            configText += "\n"
    configText += "<end>\n\n<inactive replaced characters>\n"
    for wR, wD in replaced_chars_inactive.keys():
        if (wR != ""):
            configText += str(wR)
            configText += "\n"
            configText += str(replaced_chars_inactive[wD])
            configText += "\n"
    configText += "<end>\n\n<inactive ignored words>\n"
    for wR in ignored_words_inactive:
        if (wR != ""):
            configText += str(wR)
            configText += "\n"
    configText += "<end>\n\n<inactive ignored characters>\n"
    for wR in ignored_chars_inactive:
        if (wR != ""):
            configText += str(wR)
            configText += "\n"
    configText += "<end>\n"

    # write file
    try:
        doc = open(path, "w")
        doc.write(configText)
        doc.close()
    except Exception:
        return True
    return False


# create output directory
def output_directory(pathDir):
    folderExists = True
    pathDir2 = pathDir
    if pathDir.endswith('\\') or pathDir.endswith('/'):
        pathDir2 = pathDir[0:len(pathDir) - 1]
    if not os.path.isdir(pathDir2):
        try:
            os.mkdir(pathDir2)
        except Exception:
            try:
                os.makedirs(pathDir2)
            except Exception:
                folderExists = False
    return folderExists


# outputs an excel file for a single block as requested
def single_output(params, block, path):
    global parameter_explanations
    global parameter_hidden

    # excel file constants
    global conn_sum_string
    global self_conn_string
    global act_sum_string_units
    global act_sum_string_cycles
    global min_col_width
    global indentation
    global darkgreen
    global darkred

    wb = Workbook()

    lsa_conn_unrecognised = {}
    connections_unrecognised = {}
    lsa_self_connections = []
    self_connections = []

    # removes default sheet
    temp_array = []
    for item in wb.sheetnames:
        temp_array.append(item)
    wb.remove(wb[temp_array[0]])

    # creating parameters sheet
    ws_params = wb.create_sheet(title='Parameters')

    # colwidths = {}
    #
    # # Store the defaults.
    # for col in range(50):
    #     colwidths[col] = 25
    #
    # # Then set the column widths.
    # for col_num, width in colwidths.items():
    #     ws_params.set_column(col_num, col_num, width)

    # ws_params.set_column('A:A', 25)

    # populate parameters
    i = 1
    for key in params.keys():
        # parameters to be ignored for now
        if key in parameter_hidden:
            continue

        # writing parameter description
        ws_params['A' + str(i)] = parameter_explanations[key] + ":"
        ws_params['A' + str(i)].font = Font(bold=True)

        # writing parameter values
        val = params[key]
        ws_params['B' + str(i)] = val
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center

        # styling parameter value in case of True/False
        if str(params[key]) == 'True':
            ws_params['B' + str(i)].font = Font(color=darkgreen)
        elif str(params[key]) == 'False':
            ws_params['B' + str(i)].font = Font(color=darkred)

        i += 1

    i += 1
    ws_params['A' + str(i)] = parameter_explanations['ignored_words'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)

    # if the ignored words list is case sensitive, use it as is
    if params['case_sensitive']:
        ignored_words_xls = params['ignored_words']

    # otherwise, create a list with only lower case words
    else:
        ignored_words_xls = list(OrderedSet([x.lower() for x in params['ignored_words']]))

    # writing the ignored words list
    for item in ignored_words_xls:
        ws_params['B' + str(i)] = str(item)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing ignored chars list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['ignored_chars'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)

    for item in params['ignored_chars']:
        ws_params['B' + str(i)] = str(item)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing replaced words list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['replaced_words'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)
    for itemKey in params['replaced_words'].keys():
        itemData = params['replaced_words'][itemKey]
        ws_params['B' + str(i)] = str(itemKey) + " -> " + str(itemData)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing replaced chars list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['replaced_chars'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)
    for itemKey in params['replaced_chars'].keys():
        itemData = params['replaced_chars'][itemKey]
        ws_params['B' + str(i)] = str(itemKey) + " -> " + str(itemData)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing conditions
    i += 1
    ws_params['A' + str(i)] = "conditions:"
    ws_params['A' + str(i)].font = Font(bold=True)
    ### here
    for item in block.conditions:
        ws_params['B' + str(i)] = str(item)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1
    ### here
    # creating lsa sheet
    ws_lsa = wb.create_sheet(title='Initial Connections')

    # writing the rows and columns of units
    row = indentation

    ws_lsa['A' + str(indentation - 1)] = "unit index"
    ws_lsa['A' + str(indentation - 1)].font = Font(bold=True, color=darkgreen)
    ws_lsa['A' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for unit in block.units:
        ws_lsa['A' + str(row)] = block.word2index[unit] + 1
        ws_lsa['A' + str(row)].font = Font(bold=True)
        ws_lsa['A' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    ws_lsa['B' + str(indentation - 1)] = "cycle index"
    ws_lsa['B' + str(indentation - 1)].font = Font(bold=True, color=darkgreen)
    ws_lsa['B' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for unit in block.units:
        ws_lsa['B' + str(row)] = block.word2cycles[unit][0] + 1
        ws_lsa['B' + str(row)].font = Font(bold=True)
        ws_lsa['B' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    ws_lsa['C' + str(indentation-1)] = "unit content"
    ws_lsa['C' + str(indentation-1)].font = Font(bold=True,color=darkgreen)
    ws_lsa['C' + str(indentation-1)].alignment = Alignment(horizontal='center')
    row = indentation
    for unit in block.units:
        ws_lsa['C' + str(row)] = str(unit)
        ws_lsa['C' + str(row)].font = Font(bold=True)
        ws_lsa['C' + str(row)].alignment = Alignment(horizontal='center')

        row += 1



    col = indentation
    for unit in block.units:
        ws_lsa[get_column_letter(col+2) + '1'] = str(unit)
        ws_lsa[get_column_letter(col+2) + '1'].font = Font(bold=True)
        ws_lsa[get_column_letter(col+2) + '1'].alignment = Alignment(horizontal='center')

        col += 1

    # writing the lsa matrix
    temp_matrix = np.around(block.lsa_matrix, params['decimal_round'])

    for unit in block.units:

        index1 = block.word2index[unit]

        for unit2 in block.units:
            index2 = block.word2index[unit2]
            val = temp_matrix[index1, index2]
            in1 = str(index1 + indentation)
            in2 = get_column_letter(block.word2index[unit2] + indentation+2)
            ws_lsa[in2 + in1] = val

    lsa_self_connections, lsa_conn_unrecognised = self_and_unrecognised_connections(params, block, temp_matrix)

    # writing the connection sums row
    max_row = row - 1
    ws_lsa.column_dimensions['A'].width = 20
    ws_lsa['A' + str(row)] = conn_sum_string
    ws_lsa['A' + str(row)].font = Font(bold=True, color=darkred)
    ws_lsa['A' + str(row)].alignment = Alignment(horizontal='center')

    self_index = 0
    index = 0
    for i in range(indentation+2, (len(block.units) + indentation+2)):
        unrecognised_val = unrecognised_value(lsa_conn_unrecognised, index)
        self_conn = lsa_self_connections[self_index]
        ws_lsa[get_column_letter(i) + str(row)] = '=SUM({0}{1}:{0}{2}) - {3} -{4}'.format(
            get_column_letter(i), indentation, str(max_row), self_conn, unrecognised_val)
        ws_lsa[get_column_letter(i) + str(row)].font = Font(color=darkred)
        self_index += 1
        index += 1

    # creating connections sheet
    ws_conn = wb.create_sheet(title='Connections')

    # writing the rows and columns of units
    ws_conn['A' + str(indentation - 1)] = "unit index"
    ws_conn['A' + str(indentation - 1)].font = Font(bold=True, color=darkgreen)
    ws_conn['A' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for unit in block.units:
        ws_conn['A' + str(row)] = block.word2index[unit] + 1
        ws_conn['A' + str(row)].font = Font(bold=True)
        ws_conn['A' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    ws_conn['B' + str(indentation - 1)] = "cycle index"
    ws_conn['B' + str(indentation - 1)].font = Font(bold=True, color=darkgreen)
    ws_conn['B' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for unit in block.units:
        ws_conn['B' + str(row)] = block.word2cycles[unit][0] + 1
        ws_conn['B' + str(row)].font = Font(bold=True)
        ws_conn['B' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    ws_conn['C' + str(indentation - 1)] = "unit content"
    ws_conn['C' + str(indentation - 1)].font = Font(bold=True, color=darkgreen)
    ws_conn['C' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for unit in block.units:
        ws_conn['C' + str(row)] = str(unit)
        ws_conn['C' + str(row)].font = Font(bold=True)
        ws_conn['C' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    col = indentation
    for unit in block.units:
        ws_conn[get_column_letter(col+2) + '1'] = str(unit)
        ws_conn[get_column_letter(col+2) + '1'].font = Font(bold=True)
        ws_conn[get_column_letter(col+2) + '1'].alignment = Alignment(horizontal='center')

        col += 1

    # writing the connection matrix
    temp_matrix = np.around(block.connection_matrix, params['decimal_round']).transpose()  # here, not transpose?
    col = len(block.units)
    matrix_after_threshold = np.zeros((col, col), dtype=float)
    for unit in block.units:
        index1 = block.word2index[unit]

        for unit2 in block.units:
            index2 = block.word2index[unit2]
            val = temp_matrix[index1, index2]
            # connection threshold
            if (val < params['connection_threshold']):
                val = 0

            matrix_after_threshold[index1,index2] =val
            in1 = str(index1 + indentation)
            in2 = get_column_letter(block.word2index[unit2] + indentation+2)
            ws_conn[in2 + in1] = val
            # itemKey_unrecognised = 0
    self_connections, connections_unrecognised = self_and_unrecognised_connections(params, block, matrix_after_threshold)

    # writing the connection sums row
    max_row = row - 1
    ws_lsa.column_dimensions['A'].width = 20
    ws_conn['A' + str(row)] = self_conn_string
    ws_conn['A' + str(row)].font = Font(bold=True, color=darkred)
    ws_conn['A' + str(row)].font = Font(bold=True, color=darkred)
    ws_conn['A' + str(row)].alignment = Alignment(horizontal='center')

    ws_conn['A' + str(row + 1)] = conn_sum_string
    ws_conn['A' + str(row + 1)].font = Font(bold=True, color=darkred)
    ws_conn['A' + str(row + 1)].alignment = Alignment(horizontal='center')

    self_index = 0
    index_unrecognised = 0
    for i in range(indentation+2, (len(block.units) + indentation+2)):
        self_conn = self_connections[self_index]
        unrecognised_unit = unrecognised_value(connections_unrecognised, index_unrecognised)

        ws_conn[get_column_letter(i) + str(row)] = self_conn
        ws_conn[get_column_letter(i) + str(row)].font = Font(color=darkred)

        ws_conn[get_column_letter(i) + str(row + 1)] = '=SUM({0}{1}:{0}{2}) - {3} - {4}'.format(
            get_column_letter(i), indentation, str(max_row), self_conn, unrecognised_unit)
        ws_conn[get_column_letter(i) + str(row + 1)].font = Font(color=darkred)

        self_index += 1
        index_unrecognised += 1

    # creating activation sheet
    ws_act = wb.create_sheet(title='Activation')
    if params['blank_cycle']:
        len_block = len(block) + 1
    else:
        len_block = len(block)
    # writing the rows (cycles) and columns (units)
    ws_act['A' + str(indentation - 1)] = "unit index"
    ws_act['A' + str(indentation - 1)].font = Font(bold=True, color=darkgreen)
    ws_act['A' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for unit in block.units:
        ws_act['A' + str(row)] = block.word2index[unit] + 1
        ws_act['A' + str(row)].font = Font(bold=True)
        ws_act['A' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    ws_act['B' + str(indentation - 1)] = "cycle index"
    ws_act['B' + str(indentation - 1)].font = Font(bold=True, color=darkgreen)
    ws_act['B' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for unit in block.units:
        ws_act['B' + str(row)] = block.word2cycles[unit][0] + 1
        ws_act['B' + str(row)].font = Font(bold=True)
        ws_act['B' + str(row)].alignment = Alignment(horizontal='center')

        row += 1


    ws_act['C' + str(indentation - 1)] = "unit content"
    ws_act['C' + str(indentation - 1)].font = Font(bold=True, color=darkgreen)
    ws_act['C' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    i = 0
    for unit in block.units:
        row = i + indentation
        i += 1
        ws_act['C' + str(row)] = str(unit)
        ws_act['C' + str(row)].font = Font(bold=True, color=darkgreen)
        ws_act['C' + str(row)].alignment = Alignment(horizontal='center')



    col = indentation
    for cycle_num in range(len_block):
        ws_act[get_column_letter(col+2) + '1'] = 'cycle ' + str(cycle_num + 1)
        ws_act[get_column_letter(col+2) + '1'].font = Font(bold=True)
        ws_act[get_column_letter(col+2) + '1'].alignment = Alignment(horizontal='center')

        col += 1

    # writing the activation matrix
    temp_matrix = np.around(block.activation_matrix, params['decimal_round']).transpose()

    for i in range(indentation, (len(block.units) + indentation)):
        for j in range(indentation, (len_block + indentation)):
            if params['limit_cycles']:
                if j - indentation < params['iteration_limit']:
                    val = temp_matrix[i - indentation, j - indentation]
                else:
                    val = 0  # try here not the problem
            else:
                val = temp_matrix[i - indentation, j - indentation]

            in2 = get_column_letter(j+2)
            ws_act[in2 + str(i)] = val

            cur_unit = block.units[i - indentation]
            if params['blank_cycle'] and j == len_block + indentation - 1:
                if params['limit_cycles']:
                    if j - indentation < params['iteration_limit']:
                        ws_act[in2 + str(i)].font = Font(bold=True)
                        # else:
                        #     ws_act[in2 + str(i)].font = Font(bold=True)

            elif cur_unit in block.cycle2words[j - indentation] and cur_unit not in block.inferred_units:
                # if params['blank_cycle'] and j == len_block + indentation - 1:
                #     ws_act[in2 + str(i)].font = Font(bold=False)
                # else:
                if params['limit_cycles']:
                    if j - indentation < params['iteration_limit']:
                        ws_act[in2 + str(i)].font = Font(bold=True)
                else:
                    ws_act[in2 + str(i)].font = Font(bold=True)

    # writing the unit activation sums row
    max_row = row - 1
    row += 1
    ws_act['A' + str(row)] = act_sum_string_cycles
    ws_act['A' + str(row)].font = Font(bold=True, color=darkred)
    ws_act['A' + str(row)].alignment = Alignment(horizontal='center')

    for i in range(indentation+2, (len_block + indentation+2)):
        ws_act[get_column_letter(i) + str(row)] = '=SUM({0}{1}:{0}{2})'.format(
            get_column_letter(i), indentation, str(max_row + 1))
        ws_act[get_column_letter(i) + str(row)].font = Font(color=darkred)

    # writing the cycle activation sums col
    ws_act[get_column_letter(col+2) + '1'] = act_sum_string_units
    ws_act[get_column_letter(col+2) + '1'].font = Font(bold=True, color=darkred)
    ws_act[get_column_letter(col+2) + '1'].alignment = Alignment(horizontal='center')

    max_col = get_column_letter(col+2 - 1)
    for i in range(indentation, (len(block.units) + indentation)):
        ws_act[get_column_letter(col+2) + str(i)] = '=SUM({0}{1}:{2}{1})'.format(get_column_letter(indentation+2),
                                                                               str(i), max_col)
        ws_act[get_column_letter(col+2) + str(i)].font = Font(color=darkred)

    # set column widths to size of longest entry in column, for every column in every sheet
    for sheet in wb.sheetnames:

        if (sheet == "Parameters"):
            # for i in range(0,2):
            #     val = get_column_letter(i + 1)
            wb[sheet].column_dimensions['A'].width = 45
            wb[sheet].column_dimensions['B'].width = 65
        else:
            wb[sheet].freeze_panes = wb[sheet]['D2']
            wb[sheet].column_dimensions['A'].width = 20

        for row in wb[sheet].iter_rows():
            for cell in row:
                cell.alignment = cell.alignment.copy(wrapText=True)

    if not 'output_name' in params.keys():
        params['output_name'] = ""
    path2 = path
    if params['output_name'] != "":
        path2 = path2 + params['output_name'] + "_";
    if params['append_datetime']:
        path2 = path2 + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M_");

    # save excel file in path
    fullPath = os.path.normpath(path2 + block.file_title + ".xlsx")
    try:
        wb.save(fullPath)
    except Exception:
        params['error_flag'] = 1
        params['error_text'] = params['error_text'] + '\nERROR: Cannot create ' + block.file_title + '.xlsx\n'
        params['error_text'] = params[
                                   'error_text'] + 'Check if the this file is currently open, and if so, close it before trying again\n'
    return params


def most_frequent(List):
    dict = {}
    count, itm = 0, ''
    for item in reversed(List):
        dict[item] = dict.get(item, 0) + 1
        if dict[item] >= count:
            count, itm = dict[item], item
    return (itm)


def self_and_unrecognised_connections(params, block, matrix):
    self_connections = []
    conn_unrecognised = {}
    key_dic = 0
    for unit in block.units:

        index1 = block.word2index[unit]

        for unit2 in block.units:

            index2 = block.word2index[unit2]
            val = matrix[index1, index2]
            if index1 == index2:
                self_connections.append(val)
            if unit in params['unrecognised_words']:
                val2count = val
                if index1 == index2:
                    val2count = 0.0
                try:
                    conn_unrecognised[key_dic].append(val2count)
                except KeyError:
                    conn_unrecognised[key_dic] = [val2count]
                key_dic += 1

        key_dic = 0
    return self_connections, conn_unrecognised


def unrecognised_value(lsa_conn_unrecognised, index):
    values = lsa_conn_unrecognised.get(index)
    if values == None:
        unrecognised_val = 0.0
    else:
        unrecognised_val = sum(values)
    return unrecognised_val


def joint_output(params, block_list, path):
    global parameter_explanations
    global parameter_hidden

    # excel file constants
    global conn_sum_string
    global act_sum_string_units
    global act_sum_string_cycles
    global min_col_width
    global indentation
    global darkgreen
    global darkred

    global lsa_self_connections
    global self_connections
    global lsa_conn_unrecognised
    global connections_unrecognised

    len_list = []
    irregular_id = []
    # slashValues = []
    for block in block_list:
        len_temp = len(block.sub_titles)
        len_list.append(len_temp)
        # name2slash[block.title] = len_temp
    most_common = most_frequent(len_list)

    for block in block_list:
        len_temp = len(block.sub_titles)
        if len_temp != most_common:
            irregular_id.append(block.id)

    max_number = max(len_list)

    params['irregular_text_id'] = irregular_id

    categories = ['text index', 'text name']
    for i in range(1, max_number + 1):
        categories += ['condition ' + str(i)]

    # update the params['show_columns'] , update for multiple text names
    index_bool = 0
    temp_bool = []
    for bool in params['show_columns']:
        if index_bool == 2:
            for i in range(0, max_number + 1):
                temp_bool.append(bool)
        else:
            temp_bool.append(bool)
        index_bool = index_bool + 1

    params['show_columns'] = temp_bool

    categories += ['last seen in cycle', 'activation in one cycle before the last',
                   'activation in last cycle', 'activation sum', 'activation mean', 'initial connection sum',
                   'initial connection mean', 'self connection',
                   'connections sum column', 'connections mean column', 'connections sum row', 'connections mean row']
    # stop
    categoryConditionTitle = "condition "
    categoryBeforeConditionTitle = "text name"
    categoryAfterConditionTitle = "last seen in cycle"

    # creating a dictionary of all units, pointing to their respective blocks
    joint_units = []

    for block in block_list:
        for unit in block.units:
            joint_units.append((unit, block))

    # getting maximum condition length
    cond_num = 0
    for block in block_list:
        if block.conditions:
            if cond_num < len(block.conditions):
                cond_num = len(block.conditions)

    # inserting "condition 1, condition 2, ..." to the categories list
    if cond_num > 0:
        j = 0
        for i in range(cond_num):
            categories.insert(2 + j, categoryConditionTitle + str(i + 1))
            j += 1

    wb = Workbook()

    # removes default sheet
    temp_array = []
    for item in wb.sheetnames:
        temp_array.append(item)
    wb.remove(wb[temp_array[0]])

    # creating parameters sheet
    ws_params = wb.create_sheet(title='Parameters')

    # populate parameters
    i = 1
    for key in params.keys():

        # parameters to be ignored for now
        if key in parameter_hidden:
            continue

        # writing parameter description
        ws_params['A' + str(i)] = parameter_explanations[key] + ":"
        ws_params['A' + str(i)].font = Font(bold=True)

        # writing parameter values
        ws_params['B' + str(i)] = params[key]
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center

        # styling parameter value in case of True/False
        if str(params[key]) == 'True':
            ws_params['B' + str(i)].font = Font(color=darkgreen)
        elif str(params[key]) == 'False':
            ws_params['B' + str(i)].font = Font(color=darkred)

        i += 1

    i += 1
    ws_params['A' + str(i)] = parameter_explanations['ignored_words'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)

    # if the ignored words list is case sensitive, use it as is
    if params['case_sensitive']:
        ignored_words_xls = params['ignored_words']

    # otherwise, create a list with only lower case words
    else:
        ignored_words_xls = list(OrderedSet([x.lower() for x in params['ignored_words']]))

    # writing the ignored words list
    for item in ignored_words_xls:
        ws_params['B' + str(i)] = str(item)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing ignored chars list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['ignored_chars'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)

    for item in params['ignored_chars']:
        ws_params['B' + str(i)] = str(item)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing replaced words list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['replaced_words'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)
    for itemKey in params['replaced_words'].keys():
        itemData = params['replaced_words'][itemKey]
        ws_params['B' + str(i)] = str(itemKey) + " -> " + str(itemData)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing replaced chars list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['replaced_chars'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)
    for itemKey in params['replaced_chars'].keys():
        itemData = params['replaced_chars'][itemKey]
        ws_params['B' + str(i)] = str(itemKey) + " -> " + str(itemData)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # creating summary sheet
    ws = wb.create_sheet(title='Summary')

    # writing unit rows
    row = indentation
    for item in joint_units:
        ws['A' + str(row)] = str(item[0])
        ws['A' + str(row)].font = Font(bold=True)
        ws['A' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    ws['B' + str(indentation - 1)] = "unit index"
    ws['B' + str(indentation - 1)].font = Font(bold=True)
    ws['B' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for item in joint_units:
        ws['B' + str(row)] = item[1].word2index[item[0]] + 1
        ws['B' + str(row)].font = Font(bold=True)
        ws['B' + str(row)].alignment = Alignment(horizontal='center')

        row += 1
    #
    ws['C' + str(indentation - 1)] = "cycle index"
    ws['C' + str(indentation - 1)].font = Font(bold=True)
    ws['C' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for item in joint_units:
        ws['C' + str(row)] = item[1].word2cycles[item[0]][0] + 1
        ws['C' + str(row)].font = Font(bold=True)
        ws['C' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    col = indentation+2
    j2 = 0;
    if params['show_all_columns']:
        for category in categories:
            ws[get_column_letter(col) + '1'] = category
            ws[get_column_letter(col) + '1'].font = Font(bold=True)
            ws[get_column_letter(col) + '1'].alignment = Alignment(horizontal='center')
            col += 1
    else:
        for category in categories:
            if category == categoryAfterConditionTitle and cond_num > 0:
                j2 = j2 + 1
            if params['show_columns'][j2]:
                ws[get_column_letter(col) + '1'] = category
                ws[get_column_letter(col) + '1'].font = Font(bold=True)
                ws[get_column_letter(col) + '1'].alignment = Alignment(horizontal='center')
                col += 1
            if category[0:len(categoryConditionTitle)] == categoryConditionTitle:
                j2 = j2 - 1
            if category == categoryBeforeConditionTitle and cond_num < 1:
                j2 = j2 + 1
            j2 += 1

    i  = 2
    j=4
    for item in joint_units:

        # comfortable constants
        block = item[1]
        word = item[0]

        act_matrix = np.around(block.activation_matrix, params['decimal_round'])
        act_tran = np.around(block.activation_matrix, params['decimal_round']).transpose()
        lsa_matrix = np.around(block.lsa_matrix, params['decimal_round'])
        conn_matrix = np.around(block.connection_matrix, params['decimal_round'])
        j2 = 0

        if params['show_all_columns'] or params['show_columns'][j2]:
            # text index
            ws[get_column_letter(j) + str(i)] = block.id
            ws[get_column_letter(j) + str(i)].font = Font(color=darkblue)
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # text name
            ws[get_column_letter(j) + str(i)] = block.title
            ws[get_column_letter(j) + str(i)].font = Font(color=darkblue)
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # text name conditions
            for num in range(0, max_number):

                if num <= len(block.sub_titles) - 1:
                    val = block.sub_titles[num]
                    ws[get_column_letter(j) + str(i)] = val
                else:
                    ws[get_column_letter(j) + str(i)] = ""
                ws[get_column_letter(j) + str(i)].font = Font(italic=True)
                ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
                j += 1
        j2 += max_number - 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # conditions
            k = 0
            while k < len(block.conditions):
                ws[get_column_letter(j + k) + str(i)] = block.conditions[k]
                ws[get_column_letter(j + k) + str(i)].font = Font(italic=True)
                ws[get_column_letter(j + k) + str(i)].alignment = Alignment(horizontal='center')
                k = k + 1
            j += cond_num

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # last seen in cycle
            if params['limit_cycles']:
                temp_list = block.word2cycles[word]

                for i3 in temp_list:
                    if i3 + 1 > int(params['iteration_limit']):
                        temp_list.remove(i3)

                if not block.word2cycles[word]:
                    ws[get_column_letter(j) + str(i)] = 0
                else:
                    ws[get_column_letter(j) + str(i)] = max(block.word2cycles[word]) + 1
            else:
                ws[get_column_letter(j) + str(i)] = max(block.word2cycles[word]) + 1

            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1;
        if params['show_all_columns'] or params['show_columns'][j2]:
            # activation in second to last cycle
            if params['blank_cycle']:
                len_block = len(block) + 1
            else:
                len_block = len(block)
            if params['limit_cycles'] == True:
                v1 = min(params['iteration_limit'] - 2, len_block - 2)
            else:
                v1 = (len_block - 2)
            v2 = block.word2index[word]
            v3 = act_matrix[(len_block - 2), block.word2index[word]]
            limit = params['iteration_limit']
            if (v1 < 0):
                ws[get_column_letter(j) + str(i)] = 0
            else:
                ws[get_column_letter(j) + str(i)] = act_matrix[v1, block.word2index[word]]

            # ws[get_column_letter(j) + str(i)] = act_matrix[(len(block) - 2), block.word2index[word]]
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:

            # activation in last cycle
            if params['limit_cycles'] == True:
                v1 = min(params['iteration_limit'] - 1, len_block - 1)
            else:
                v1 = (len_block - 1)
            v2 = block.word2index[word]
            v3 = act_matrix[(len_block - 1), block.word2index[word]]
            # if params['limit_cycles'] == True and v1 >= params['iteration_limit']:
            #     # if v1 >= params['iteration_limit']:
            #         ws[get_column_letter(j) + str(i)] = 0
            # else:
            ws[get_column_letter(j) + str(i)] = act_matrix[v1, block.word2index[word]]
            # ws[get_column_letter(j) + str(i)] = act_matrix[(len(block) - 1), block.word2index[word]]
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1;
        if params['show_all_columns'] or params['show_columns'][j2]:
            # activation sum
            if params['limit_cycles']:
                last_cycle = min(params['iteration_limit'], len(block))
                sum_val = 0
                for k in range(0, last_cycle):
                    sum_val += block.activation_matrix[k, block.word2index[word]]
                act_sum = np.around(sum_val, params['decimal_round'])

            else:
                act_sum = np.around(np.sum(block.activation_matrix, 0)[block.word2index[word]], params['decimal_round'])
            # a = np.sum(block.activation_matrix, 0)[block.word2index[word]]
            # b = np.around(a)
            # act_sum = np.around(np.sum(block.activation_matrix, 0)[block.word2index[word]], params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = act_sum
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1;
        if params['show_all_columns'] or params['show_columns'][j2]:
            # activation mean
            if params['limit_cycles']:
                iteration_num = min(params['iteration_limit'], len(block))
                val_mean = act_sum / iteration_num
                act_mean = np.around(val_mean, params['decimal_round'])
            else:
                act_mean = np.around(np.mean(block.activation_matrix, 0)[block.word2index[word]],
                                     params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = act_mean
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1;
        if params['show_all_columns'] or params['show_columns'][j2]:
            # lsa sum
            word_num = block.word2index[word]
            matrix = np.around(block.lsa_matrix, params['decimal_round'])
            self, unrecognised = self_and_unrecognised_connections(params, block, matrix)

            self_conn = self[word_num]
            unrecognised_val = unrecognised_value(unrecognised, word_num)
            lsa_sum = np.around(np.around(np.sum(block.lsa_matrix, 0)[block.word2index[word]], params['decimal_round']),
                                params['decimal_round']) - self_conn - unrecognised_val
            ws[get_column_letter(j) + str(i)] = lsa_sum
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1;
        if params['show_all_columns'] or params['show_columns'][j2]:
            # lsa mean
            self_unrecognised_num = unrecognised_words_block(params, block.units,word)
            mean_val = lsa_sum / (len(block.word2index) - self_unrecognised_num)  # without the same connection
            lsa_mean = np.around(mean_val, params['decimal_round'])
            # lsa_mean = np.around(np.mean(block.lsa_matrix, 0)[block.word2index[word]], params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = lsa_mean
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # self connection
            word_number = block.word2index[word]
            temp_matrix = np.around(block.connection_matrix, params['decimal_round'])
            self_connection = temp_matrix[word_number][word_number]

            ws[get_column_letter(j) + str(i)] = self_connection
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # connection sum column
            word_number = block.word2index[word]
            temp_matrix = np.around(block.connection_matrix, params['decimal_round'])
            # matrix = np.around(block.connection_matrix, params['decimal_round']).transpose()
            # matrix after threshold
            matrix_temp= matrix_threshold(temp_matrix,len(block.units),params)
            matrix_after_threshold= matrix_temp.transpose()
            self, unrecognised = self_and_unrecognised_connections(params, block, matrix_after_threshold)
            self_connection = self[word_number]
            unrecognised_val = unrecognised_value(unrecognised, word_number)
            # sum the connections without the self-connection
            conn_sum_column = np.around(np.around(np.sum(matrix_after_threshold, 0)[block.word2index[word]],
                                                  params['decimal_round']) - self_connection - unrecognised_val,
                                        params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = conn_sum_column
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # connection mean column
            self_unrecognised_num = unrecognised_words_block(params, block.units, word)
            connection_mean_val_column = conn_sum_column / (
                (len(block.word2index)) - self_unrecognised_num)  # without the same connection
            conn_mean = np.around(connection_mean_val_column, params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = conn_mean
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # connection sum row
            word_number = block.word2index[word]
            # temp_matrix = np.around(block.connection_matrix, params['decimal_round'])
            matrix = matrix_after_threshold
            self, unrecognised = self_and_unrecognised_connections(params, block, matrix.transpose())

            self_connection = self[word_number]
            unrecognised_val = unrecognised_value(unrecognised, word_number)
            # sum the connections without the self-connection
            conn_sum_row = np.around(np.around(np.sum(matrix, 1)[block.word2index[word]],
                                               params['decimal_round']) - self_connection - unrecognised_val,
                                     params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = conn_sum_row
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # connection mean row
            connection_mean_val_row = conn_sum_row / (
                len(block.word2index) - self_unrecognised_num)  # without the same connection
            conn_mean_row = np.around(connection_mean_val_row, params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = conn_mean_row
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        i += 1
        j = 4

    # set column widths to size of longest entry in column, for every column in every sheet
    for sheet in wb.sheetnames:
        # for sheet in wb.sheetnames:

        if (sheet == "Parameters"):
            # for i in range(0,2):
            #     val = get_column_letter(i + 1)
            wb[sheet].column_dimensions['A'].width = 45
            wb[sheet].column_dimensions['B'].width = 65
        else:
            # wb[sheet].column_dimensions['A'].width = 20
            wb[sheet].freeze_panes = wb[sheet]['F2']
            for i in range(0, len(categories) + 1):
                val = get_column_letter(i + 1)
                wb[sheet].column_dimensions[val].width = 15

        for row in wb[sheet].iter_rows():
            for cell in row:
                cell.alignment = cell.alignment.copy(wrapText=True)
                # for column_cells in wb[sheet].columns:
                #     col_width = max(len(as_text(cell.value)) for cell in column_cells)
                #
                #     if col_width < min_col_width:
                #         col_width = min_col_width


                # hereeeeeeeeeeeeeeeeeee
                #
                # temp4 = round_width(col_width)
                # wb[sheet].column_dimensions[sheet].width = round_width(col_width)
                # temp5 = wb[sheet].column_dimensions[sheet].width

                # wb[sheet].column_dimensions['A'].width = round_width(col_width)
                # wb[sheet].column_dimensions[column_cells[0].column].width = round_width(col_width)

    if not 'output_name' in params.keys():
        params['output_name'] = ""
    path2 = path
    if params['output_name'] != "":
        path2 = path2 + params['output_name'] + "_";
    if params['append_datetime']:
        path2 = path2 + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M_");

    # save excel file in path
    fullPath = os.path.normpath(path2 + "Summary.xlsx")
    try:
        wb.save(fullPath)
    except Exception:
        params['error_flag'] = 1
        params['error_text'] = params['error_text'] + '\nERROR: Cannot create Summary.xlsx\n'
        params['error_text'] = params[
                                   'error_text'] + 'Check if the this file is currently open, and if so, close it before trying again\n'
    return params


def target_output(params, block_list, path):
    global parameter_explanations
    global parameter_hidden

    # excel file constants
    global conn_sum_string
    global act_sum_string_units
    global act_sum_string_cycles
    global min_col_width
    global indentation
    global darkgreen
    global darkred

    len_list = []
    # name2slash = {}
    for block in block_list:
        len_temp = len(block.sub_titles)
        len_list.append(len_temp)
    # name2slash[block.title] = len_temp

    # must_common_number = most_frequent(len_list)
    max_number = max(len_list)

    # params['irregular_text_name'] = name2slash

    categories = ['text index', 'text name']
    for i in range(1, max_number + 1):
        categories += ['condition ' + str(i)]

    # update the params['show_columns'] , update for multiple text names
    index_bool = 0
    temp_bool = []
    for bool in params['show_columns']:
        if index_bool == 2:
            for i in range(0, max_number + 1):
                temp_bool.append(bool)
        else:
            temp_bool.append(bool)
        index_bool = index_bool + 1

    params['show_columns'] = temp_bool

    categories += ['last seen in cycle', 'activation in one cycle before the last',
                   'activation in last cycle', 'activation sum', 'activation mean', 'initial connection sum',
                   'initial connection mean', 'self connection',
                   'connections sum column', 'connections mean column', 'connections sum row', 'connections mean row']

    categoryConditionTitle = "condition "
    categoryBeforeConditionTitle = "text name"
    categoryAfterConditionTitle = "last seen in cycle"

    # creating a dictionary of all units, pointing to their respective blocks
    joint_units = []

    for block in block_list:
        for unit in block.target_units:
            joint_units.append((unit, block))

    if not joint_units:
        return params

    # getting maximum condition length
    cond_num = 0
    for block in block_list:
        if block.conditions:
            if cond_num < len(block.conditions):
                cond_num = len(block.conditions)

    # inserting "condition 1, condition 2, ..." to the categories list
    if cond_num > 0:
        j = 0
        for i in range(cond_num):
            categories.insert(2 + j, 'condition ' + str(i + 1))
            j += 1

    wb = Workbook()

    # removes default sheet
    temp_array = []
    for item in wb.sheetnames:
        temp_array.append(item)
    wb.remove(wb[temp_array[0]])

    # creating parameters sheet
    ws_params = wb.create_sheet(title='Parameters')

    # populate parameters
    i = 1
    for key in params.keys():

        # parameters to be ignored for now
        if key in parameter_hidden:
            continue

        # writing parameter description
        ws_params['A' + str(i)] = parameter_explanations[key] + ":"
        ws_params['A' + str(i)].font = Font(bold=True)

        # writing parameter values
        ws_params['B' + str(i)] = params[key]
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center

        # styling parameter value in case of True/False
        if str(params[key]) == 'True':
            ws_params['B' + str(i)].font = Font(color=darkgreen)
        elif str(params[key]) == 'False':
            ws_params['B' + str(i)].font = Font(color=darkred)

        i += 1

    i += 1
    ws_params['A' + str(i)] = parameter_explanations['ignored_words'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)

    # if the ignored words list is case sensitive, use it as is
    if params['case_sensitive']:
        ignored_words_xls = params['ignored_words']

    # otherwise, create a list with only lower case words
    else:
        ignored_words_xls = list(OrderedSet([x.lower() for x in params['ignored_words']]))

    # writing the ignored words list
    for item in ignored_words_xls:
        ws_params['B' + str(i)] = str(item)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing ignored chars list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['ignored_chars'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)

    for item in params['ignored_chars']:
        ws_params['B' + str(i)] = str(item)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing replaced words list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['replaced_words'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)
    for itemKey in params['replaced_words'].keys():
        itemData = params['replaced_words'][itemKey]
        ws_params['B' + str(i)] = str(itemKey) + " -> " + str(itemData)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # writing replaced chars list
    i += 1
    ws_params['A' + str(i)] = parameter_explanations['replaced_chars'] + ":"
    ws_params['A' + str(i)].font = Font(bold=True)
    for itemKey in params['replaced_chars'].keys():
        itemData = params['replaced_chars'][itemKey]
        ws_params['B' + str(i)] = str(itemKey) + " -> " + str(itemData)
        ws_params['B' + str(i)].alignment = Alignment(horizontal='left')  # center
        i += 1

    # creating target sheet
    ws = wb.create_sheet(title='Target Units')

    # writing unit rows
    row = indentation
    ws['A' + str(row-1)] = "unit content"
    ws['A' + str(row-1)].font = Font(bold=True)
    ws['A' + str(row-1)].alignment = Alignment(horizontal='center')
    for item in joint_units:
        ws['A' + str(row)] = str(item[0])
        ws['A' + str(row)].font = Font(bold=True)
        ws['A' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    ws['B' + str(indentation - 1)] = "unit index"
    ws['B' + str(indentation - 1)].font = Font(bold=True)
    ws['B' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for item in joint_units:
        ws['B' + str(row)] = str(item[1].word2index[item[0]] + 1)
        ws['B' + str(row)].font = Font(bold=True)
        ws['B' + str(row)].alignment = Alignment(horizontal='center')

        row += 1
    #
    ws['C' + str(indentation - 1)] = "cycle index"
    ws['C' + str(indentation - 1)].font = Font(bold=True)
    ws['C' + str(indentation - 1)].alignment = Alignment(horizontal='center')
    row = indentation
    for item in joint_units:
        ws['C' + str(row)] = str(item[1].word2cycles[item[0]][0] + 1)
        ws['C' + str(row)].font = Font(bold=True)
        ws['C' + str(row)].alignment = Alignment(horizontal='center')

        row += 1

    col = indentation+2
    j2 = 0
    if params['show_all_columns']:
        for category in categories:
            ws[get_column_letter(col) + '1'] = category
            ws[get_column_letter(col) + '1'].font = Font(bold=True)
            ws[get_column_letter(col) + '1'].alignment = Alignment(horizontal='center')
            col += 1
    else:
        for category in categories:
            if category == categoryAfterConditionTitle and cond_num > 0:
                j2 = j2 + 1
            if params['show_columns'][j2]:
                ws[get_column_letter(col) + '1'] = category
                ws[get_column_letter(col) + '1'].font = Font(bold=True)
                ws[get_column_letter(col) + '1'].alignment = Alignment(horizontal='center')
                col += 1
            if category[0:len(categoryConditionTitle)] == categoryConditionTitle:
                j2 = j2 - 1
            if category == categoryBeforeConditionTitle and cond_num < 1:
                j2 = j2 + 1
            j2 += 1

    i = 2
    j = 4
    for item in joint_units:

        # comfortable constants
        block = item[1]
        word = item[0]
        act_matrix = np.around(block.activation_matrix, params['decimal_round'])
        lsa_matrix = np.around(block.lsa_matrix, params['decimal_round'])
        conn_matrix = np.around(block.connection_matrix, params['decimal_round'])
        j2 = 0

        if params['show_all_columns'] or params['show_columns'][j2]:
            # text index
            ws[get_column_letter(j) + str(i)] = block.id
            ws[get_column_letter(j) + str(i)].font = Font(color=darkblue)
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        if params['show_all_columns'] or params['show_columns'][j2]:
            # text name
            ws[get_column_letter(j) + str(i)] = block.title
            ws[get_column_letter(j) + str(i)].font = Font(color=darkblue)
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1;
        if params['show_all_columns'] or params['show_columns'][j2]:
            # text name conditions
            for num in range(0, max_number):

                if num <= len(block.sub_titles) - 1:
                    val = block.sub_titles[num]
                    ws[get_column_letter(j) + str(i)] = val
                else:
                    ws[get_column_letter(j) + str(i)] = ""
                ws[get_column_letter(j) + str(i)].font = Font(italic=True)
                ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
                j += 1
        j2 += max_number - 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # conditions
            k = 0
            while k < len(block.conditions):
                ws[get_column_letter(j + k) + str(i)] = block.conditions[k]
                ws[get_column_letter(j + k) + str(i)].font = Font(italic=True)
                ws[get_column_letter(j + k) + str(i)].alignment = Alignment(horizontal='center')
                k = k + 1
            j = j + cond_num

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # last seen in cycle # HERE
            if params['limit_cycles']:
                temp_list = block.word2cycles[word]

                for i3 in temp_list:
                    if i3 + 1 > int(params['iteration_limit']):
                        temp_list.remove(i3)

                if not block.word2cycles[word]:
                    ws[get_column_letter(j) + str(i)] = 0
                else:
                    ws[get_column_letter(j) + str(i)] = max(block.word2cycles[word]) + 1
            else:
                ws[get_column_letter(j) + str(i)] = max(block.word2cycles[word]) + 1

            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # activation in second to last cycle
            if params['blank_cycle']:
                len_block = len(block) + 1
            else:
                len_block = len(block)
            if params['limit_cycles'] == True:
                v1 = min(params['iteration_limit'] - 2, len_block - 2)
            else:
                v1 = (len_block - 2)
            v2 = block.word2index[word]
            v3 = act_matrix[(len_block - 2), block.word2index[word]]
            limit = params['iteration_limit']
            if (v1 < 0):
                ws[get_column_letter(j) + str(i)] = 0
            else:
                ws[get_column_letter(j) + str(i)] = act_matrix[v1, block.word2index[word]]
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # activation in last cycle
            if params['limit_cycles'] == True:
                v1 = min(params['iteration_limit'] - 1, len_block - 1)
            else:
                v1 = (len_block - 1)
            v2 = block.word2index[word]
            v3 = act_matrix[(len_block - 1), block.word2index[word]]
            ws[get_column_letter(j) + str(i)] = act_matrix[v1, block.word2index[word]]
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # activation sum
            if params['limit_cycles']:
                last_cycle = min(params['iteration_limit'], len(block))
                sum_val = 0
                for k in range(0, last_cycle):
                    sum_val += block.activation_matrix[k, block.word2index[word]]
                act_sum = np.around(sum_val, params['decimal_round'])

            else:
                act_sum = np.around(np.sum(block.activation_matrix, 0)[block.word2index[word]], params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = act_sum
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # activation mean
            if params['limit_cycles']:
                iteration_num = min(params['iteration_limit'], len(block))
                val_mean = act_sum / iteration_num
                act_mean = np.around(val_mean, params['decimal_round'])
            else:
                act_mean = np.around(np.mean(block.activation_matrix, 0)[block.word2index[word]],
                                     params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = act_mean
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1;
        if params['show_all_columns'] or params['show_columns'][j2]:
            # lsa sum
            word_num = block.word2index[word]
            matrix = np.around(block.lsa_matrix, params['decimal_round'])
            self, unrecognised = self_and_unrecognised_connections(params, block, matrix)

            self_conn = self[word_num]
            unrecognised_val = unrecognised_value(unrecognised, word_num)
            lsa_sum = np.around(np.around(np.sum(block.lsa_matrix, 0)[block.word2index[word]], params['decimal_round']),
                                params['decimal_round']) - self_conn - unrecognised_val
            ws[get_column_letter(j) + str(i)] = lsa_sum
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1;
        if params['show_all_columns'] or params['show_columns'][j2]:
            # lsa mean
            self_unrecognised_num = unrecognised_words_block(params, block.units, word)
            mean_val = lsa_sum / (len(block.word2index) - self_unrecognised_num)  # without the same connection
            lsa_mean = np.around(mean_val, params['decimal_round'])
            # lsa_mean = np.around(np.mean(block.lsa_matrix, 0)[block.word2index[word]], params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = lsa_mean
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # self connection
            word_number = block.word2index[word]
            temp_matrix = np.around(block.connection_matrix, params['decimal_round'])
            self_connection = temp_matrix[word_number][word_number]

            ws[get_column_letter(j) + str(i)] = self_connection
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # connection sum column
            word_number = block.word2index[word]
            temp_matrix = np.around(block.connection_matrix, params['decimal_round'])
            # matrix = np.around(block.connection_matrix, params['decimal_round']).transpose()
            # matrix after threshold
            matrix_temp = matrix_threshold(temp_matrix, len(block.units), params)
            matrix_after_threshold = matrix_temp.transpose()
            self, unrecognised = self_and_unrecognised_connections(params, block, matrix_after_threshold)
            self_connection = self[word_number]
            unrecognised_val = unrecognised_value(unrecognised, word_number)
            # sum the connections without the self-connection
            conn_sum_column = np.around(np.around(np.sum(matrix_after_threshold, 0)[block.word2index[word]],
                                                  params['decimal_round']) - self_connection - unrecognised_val,
                                        params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = conn_sum_column
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # connection mean column
            self_unrecognised_num = unrecognised_words_block(params, block.units, word)
            connection_mean_val_column = conn_sum_column / (
                (len(block.word2index)) - self_unrecognised_num)  # without the same connection
            conn_mean = np.around(connection_mean_val_column, params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = conn_mean
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # connection sum row
            word_number = block.word2index[word]
            # temp_matrix = np.around(block.connection_matrix, params['decimal_round'])
            matrix = matrix_after_threshold
            self, unrecognised = self_and_unrecognised_connections(params, block, matrix.transpose())

            self_connection = self[word_number]
            unrecognised_val = unrecognised_value(unrecognised, word_number)
            # sum the connections without the self-connection
            conn_sum_row = np.around(np.around(np.sum(matrix, 1)[block.word2index[word]],
                                               params['decimal_round']) - self_connection - unrecognised_val,
                                     params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = conn_sum_row
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        j2 += 1
        if params['show_all_columns'] or params['show_columns'][j2]:
            # connection mean row
            connection_mean_val_row = conn_sum_row / (
                len(block.word2index) - self_unrecognised_num)  # without the same connection
            conn_mean_row = np.around(connection_mean_val_row, params['decimal_round'])
            ws[get_column_letter(j) + str(i)] = conn_mean_row
            ws[get_column_letter(j) + str(i)].alignment = Alignment(horizontal='center')
            j += 1

        i += 1
        j = 4

    for sheet in wb.sheetnames:
        # for sheet in wb.sheetnames:

        if (sheet == "Parameters"):
            # for i in range(0,2):
            #     val = get_column_letter(i + 1)
            wb[sheet].column_dimensions['A'].width = 45
            wb[sheet].column_dimensions['B'].width = 65
        else:
            wb[sheet].freeze_panes = wb[sheet]['D2']
            for i in range(0, len(categories) + 1):
                val = get_column_letter(i + 1)
                wb[sheet].column_dimensions[val].width = 15

        for row in wb[sheet].iter_rows():
            for cell in row:
                cell.alignment = cell.alignment.copy(wrapText=True)

    if not 'output_name' in params.keys():
        params['output_name'] = ""
    path2 = path
    if params['output_name'] != "":
        path2 = path2 + params['output_name'] + "_"
    if params['append_datetime']:
        path2 = path2 + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M_")

    # save excel file in path
    fullPath = os.path.normpath(path2 + "Targets.xlsx")
    try:
        wb.save(fullPath)
    except Exception:
        params['error_flag'] = 1
        params['error_text'] = params['error_text'] + '\nERROR: Cannot create Targets.xlsx\n'
        params['error_text'] = params[
                                   'error_text'] + 'Check if the this file is currently open, and if so, close it before trying again\n'
    return params

def matrix_threshold(matrix,col,params):
    new_matrix = np.zeros((col,col),dtype=float)
    for i in range(col):
        for j in range(col):
            val = matrix[i][j]
            if val < params['connection_threshold']:
                continue
            new_matrix[i][j] = val
    return new_matrix


# returns a dic for every block with the edited text and the file title in it
def post_process(block):
    temp_cycles = block.cycles
    par_all = ''

    for cycle in temp_cycles:
        par = ""

        for unit in cycle.split(' '):
            if unit in block.inferred_units:
                if unit in block.target_units:
                    unit = '@' + unit
                unit = '0' + unit
            if unit in block.target_units:
                unit = '@' + unit
            par += unit + " "

        par_all += (par[:-1] + '\n')

    return {'title': "*"+block.title, 'content': par_all}


# creates a word document with the edited texts from each block
# params: a list containing dictionaries with the text content and title of each block
def create_post_doc(params, text_list, delimiter_blocks, path, dcpFileType="docx"):
    defaultFolder = 'icons'
    defaultName = 'default.docx'
    if not 'output_name' in params.keys():
        params['output_name'] = ""
    path2 = path
    if params['output_name'] != "":
        path2 = path2 + params['output_name'] + "_";
    if params['append_datetime']:
        path2 = path2 + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M_");
    path2 = path2 + "edited_texts." + dcpFileType
    savePath = os.path.normpath(path2)

    if dcpFileType == "docx" or dcpFileType == "doc":
        try:
            dc = Document(os.path.join(defaultFolder, defaultName))
        except:
            params['error_flag'] = 1
            params['error_text'] = params[
                                       'error_text'] + '\nERROR: default docx document not found at:\n' + os.path.join(
                defaultFolder, defaultName) + '\n'
        else:
            # p = dc.add_paragraph()
            iText = 0
            for text in text_list:
                p = dc.add_paragraph()
                runner = p.add_run('&' + '\n')
                runner.bold = True

                p = dc.add_paragraph()
                runner = p.add_run(text['title'] + '\n')
                runner.bold = True

                for cycleLines in text['content'].split(delimiter_blocks[iText]):
                    for line in cycleLines.splitlines():
                        p = dc.add_paragraph()
                        for word in line.split():
                            w = word
                            contB = True
                            if len(w) > 1:
                                if len(w) > 2:
                                    if w[0:2] == '0@' or w[0:2] == '@0':
                                        w = w[2:len(w)]
                                        contB = False
                                if contB and (w[0] == '@' or w[0] == '0'):
                                    w = w[1:len(w)]
                            if w in params['unrecognised_words']:
                                runner = p.add_run(word + ' ')
                                runner.bold = False
                                font = runner.font
                                font.color.rgb = RGBColor(255, 0, 0)
                            else:
                                runner = p.add_run(word + ' ')
                                runner.bold = False
                        runner = p.add_run('\n')
                        runner.bold = False
                    runner = p.add_run('\n\n')
                iText = iText + 1

            try:
                dc.save(savePath)
            except Exception:
                params['error_flag'] = 1
                params['error_text'] = params[
                                           'error_text'] + '\nERROR: Cannot create edited_texts.' + dcpFileType + '\n'
                params['error_text'] = params[
                                           'error_text'] + 'Check if the this file is currently open, and if so, close it before trying again\n'

    if dcpFileType != "docx" and dcpFileType != "doc":
        p = "";
        iText = 0
        for text in text_list:
            p = p + text['title'] + '\n'
            for cycleLines in text['content'].split(delimiter_blocks[iText]):
                p = p + cycleLines + '\n\n'
            iText = iText + 1

        # write file
        try:
            doc = open(savePath, "w")
            doc.write(p)
            doc.close()

        except Exception:
            params['error_flag'] = 1
            params['error_text'] = params['error_text'] + '\nERROR: Cannot create edited_texts.' + dcpFileType + '\n'
            params['error_text'] = params[
                                       'error_text'] + 'check if the this file is currently open, and if so, close it before trying again\n'
    return params


# returns the current output path
# params: a list containing dictionaries with the text content and title of each block
def find_true_output_path(params):
    fullPath = "";
    if params['same_folder']:
        inPath = os.path.normpath(params['input_path'])
        fullPath = inPath;
        i2 = len(inPath) - 1;
        while (i2 > -1 and inPath[i2] != ".") and (inPath[i2] != "/" and inPath[i2] != "\\"):
            i2 = i2 - 1;
        if i2 > -1:
            if inPath[i2] == ".":
                fullPath = inPath[0:i2]
        fullPath = fullPath + "_output"
    else:
        fullPath = os.path.normpath(params['output_path'])
    if fullPath.endswith("/"):
        fullPath = fullPath[0:len(fullPath) - 1]
    if not fullPath.endswith("\\"):
        fullPath = fullPath + "\\"
    return fullPath


def unrecognised_words_block(params, units,curr_word):
    counter = 0
    for word in params['unrecognised_words']:
        for unit in units:
            if word == unit:
                counter += 1
    if curr_word not in params['unrecognised_words']:
        counter+=1

    return counter
