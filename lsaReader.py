import numpy as np
import math
import os
from collections import OrderedDict
import lsaParser as lsaP
import lsaWeb as lsaW
import time
import copy


# globals
activation_matrix = np.zeros((1, 1))
connection_matrix = np.zeros((1, 1))
index2word = OrderedDict()
word2index = OrderedDict()
cycle2words = OrderedDict()
word2cycles = OrderedDict()
config_path = 'Default_Parameters1.txt'

# function for pretty printing matrices
def matprint(mat, fmt="g"):
    strResult = ""
    end = "  "
    col_maxes = [max([len(("{:"+fmt+"}").format(x)) for x in col]) for col in mat.T]
    for x in mat:
        for i, y in enumerate(x):
            strResult = strResult + ("{:"+str(col_maxes[i])+fmt+"}").format(y) + end + "\n"
        strResult = strResult + "\n"
    return strResult

# working memory update by cycle
def working_memory(cycle, params):
    global activation_matrix

    if params['is_mem_cap']:
        temp_sum = activation_matrix[cycle].sum()

        if temp_sum > params['working_mem_cap']:
            activation_matrix[cycle] *= (params['working_mem_cap']/temp_sum)

# the Sigmoid formula
def sigmoid(x, params):

    # if linear or using sigmoid connections, return original value
    if params['sigmoid_connections'] or params['linear_function']:
        return x

    # using the range difference between min and max values defined by user to distort the original function
    range_diff = params['sigmoid_max']-params['sigmoid_min']
    add = params['sigmoid_min'] + 1
    mult = range_diff/2

    return ((math.tanh(3 * (x - 1))) + add) * mult


# expectancy formula ("expectancy(i,cycle) = sum [...]")
def expectancy(word_index, cycle, params):
    global activation_matrix
    global connection_matrix
    global index2word

    formula_sum = 0

    for other_index in index2word.keys():
        if other_index != word_index:
            formula_sum += (activation_matrix[cycle, other_index] * connection_matrix[word_index, other_index])

    formula_sum *= (params['learning_rate'] / params['m'])

    # if reduced expectancy is true, divide the formula sum by the number of units
    if params['reduced_expectancy']:
        formula_sum /= len(index2word)

    return formula_sum


# updates a connection using the connection formula ("Sij = Sij + [...]")
def update_connection(index1, index2, cycle, params):
    global activation_matrix
    global connection_matrix

    # the formula itself
    Ai = activation_matrix[cycle, index1]/params['m']
    Aj = activation_matrix[cycle, index2]/params['m']
    current_update = ((1 - expectancy(index1, cycle, params)) * Ai * Aj)*params['episodic_weight']

    # connection threshold here
    # if(current_update < params['connection_threshold']):
    #     current_update = 0
    connection_matrix[index1, index2] += current_update
    if(connection_matrix[index1, index2] < params['connection_threshold']):
        connection_matrix[index1, index2] = 0

    # if (connection_matrix[index1, index2] < params['connection_threshold'] ):
    #     connection_matrix[index1, index2] = 0
    if params['sigmoid_connections']:
        connection_matrix[index1, index2] = sigmoid(connection_matrix[index1, index2], params)
        if (connection_matrix[index1, index2] < params['connection_threshold']):
            connection_matrix[index1, index2] = 0


# updates the connections between all words
def update_cycle_connections(cycle, params):
    global index2word
    global connection_matrix

    for index1 in list(index2word.keys()):
        for index2 in list(index2word.keys()):
            update_connection(index1, index2, cycle, params)



# actual algorithm for a single text
def single_block_alg(block, params):

    # globals
    global activation_matrix
    global connection_matrix
    global index2word
    global word2index
    global cycle2words
    global word2cycles

    #################
    # INITIALIZATION
    #################
    # creating two ordered dicts, one ("index2word") - keys=indices (ints) and vals=words (strings), and vice versa
    index2word = OrderedDict((zip(range(len(block.units)), block.units)))  # using ordered dict to preserve order
    word2index = OrderedDict((zip(block.units, range(len(block.units)))))

    # initializing  cycle2words - keys: cycle num (integer), vals: words in cycle (list of strings)
    cycle2words = OrderedDict()

    i = 0
    for sentence in block.cycles:

        # using ordered set to remove duplicates from list whilst preserve order
        words_in_sentence = list(lsaP.OrderedSet(sentence.split(' ')))
        cycle2words[i] = words_in_sentence
        i += 1

    # initializing word2cycles - keys: word (string), vals: cycles which the word is in (list of integers)
    word2cycles = OrderedDict()

    for word in block.units:
        word2cycles[word] = []

    i = 0
    for sentence in block.cycles:
        words_in_sentence = list(lsaP.OrderedSet(sentence.split(' ')))

        for word in words_in_sentence:
            if word:
                word2cycles[word].append(i)
        i += 1

    # initializing activation matrix (to zeroes)
    activation_matrix = np.zeros((len(block), len(index2word)), dtype=float)
    if params['blank_cycle']:
        activation_matrix = np.zeros((len(block) +1, len(index2word)), dtype=float)

    # populating activation matrix by mentions
    for word in index2word.values():
        for cycle in block.cycles:
            temp_cycle = cycle.split()
            if word in temp_cycle and word not in block.inferred_units:
                mention_val = params['m']
                activation_matrix[block.cycles.index(cycle), word2index[word]] = params['m']

            # if blank cycle is on, every word in the last cycle gets 0 instead of mention value
            # if params['blank_cycle']:
            #     if cycle == block.cycles[-1]:
            #         activation_matrix[block.cycles.index(cycle), word2index[word]] = 0
    # if params['blank_cycle']:
    #     for word in index2word.values():
    #         activation_matrix[len(block), word2index[word]] = 0

    # initializing connection matrix (to zeroes, lsa web extraction or file)
    wordList = list(index2word.values())


    wordNum = len(wordList)
    wordUnrecognisedFlag = False
    wordUnrecognisedLoadFlag = False
    wordUnrecognised = [False]*wordNum
    wordUnrecognisedLoad = [False]*wordNum  
    connection_matrix = np.zeros((wordNum,wordNum), dtype=float)
    for i in range(wordNum):
        connection_matrix[i,i] = 1.0
        i = i + 1
    if params['loaded_connection_matrix'] == False:
        naFlag = False
        downloadRes = lsaW.download(wordList, params['lsaChoice'])
        if len(downloadRes) < wordNum:
            params['error_flag'] = 2
            params['error_text'] = params['error_text'] + '\nERROR: LSA website connection failed.\n'
            return params
        if len(downloadRes[0]) < wordNum:
            params['error_flag'] = 2
            # unrecognisedWords =[]
            # for word in downloadRes[0]:
            #     unrecognisedWords.append(word)
            params['error_text'] += '\nThe following words were not recognized by LSA: ' +','.join(downloadRes[0])+' in '+ str(block.title)+' text\n'

            return params
        i1 = 0
        i2 = 0
        while i1 < wordNum:
            naFlag = (downloadRes[i1][i1] == 'N/A')
            if not naFlag:
                try:
                    cellVal = float(downloadRes[i1][i1])
                except ValueError:
                    naFlag = True
            if naFlag:
                wordUnrecognisedFlag = True
                wordUnrecognised[i1] = True
            else:
                i2 = 0
                while i2 < i1 + 1:
                    if not wordUnrecognised[i2]:
                        naFlag = (downloadRes[i1][i2] == 'N/A')
                        if not naFlag:
                            try:
                                cellVal = float(downloadRes[i1][i2])
                            except ValueError:
                                naFlag = True
                            if not naFlag:
                                connection_matrix[i1,i2] = cellVal * params['semantic_weight']
                                connection_matrix[i2,i1] = connection_matrix[i1,i2]
                    i2 = i2 + 1
            i1 = i1 + 1
        
    if params['loaded_connection_matrix']:
        loadRes = lsaW.loadMatrix(wordList, os.path.normpath(params['connection_matrix_path']), params['case_sensitive'])
        if len(loadRes) < wordNum:
            params['error_flag'] = 3
            params['error_text'] = params['error_text'] + '\nERROR: Initial connections file not found.\n'
            return params
        if len(loadRes[0]) < wordNum:
            params['error_flag'] = 3
            params['error_text'] = params['error_text'] + '\nERROR: Initial connections file not found.\n'
            return params
        i1 = 0
        i2 = 0
        while i1 < wordNum:
            naFlag = (loadRes[i1][i1] == 'N/A')
            if not naFlag:
                try:
                    cellVal = float(loadRes[i1][i1])
                except ValueError:
                    naFlag = True
            if naFlag:
                wordUnrecognisedLoadFlag = True
                wordUnrecognisedLoad[i1] = True
            else:
                i2 = 0
                while i2 < wordNum:
                    if not wordUnrecognisedLoad[i2]:
                        if not wordUnrecognisedLoad[i2]:
                            naFlag = (loadRes[i1][i2] == 'N/A')
                            if not naFlag:
                                try:
                                    cellVal = float(loadRes[i1][i2])
                                except ValueError:
                                    naFlag = True
                                if not naFlag:
                                    connection_matrix[i1,i2] = cellVal
                    i2 = i2 + 1
            i1 = i1 + 1
        if params['loaded_connection_matrix'] == False:
            if wordUnrecognisedFlag:
                wordUnrecognisedFlag = False
                i1 = 0
                while i1 < wordNum:
                    if wordUnrecognised[i1]:
                        if wordUnrecognisedLoad[i1]:
                            wordUnrecognisedFlag = True
                        else:
                            wordUnrecognised[i1] = False
                    i1 = i1 + 1
        else:
            wordUnrecognisedFlag = wordUnrecognisedLoadFlag
            wordUnrecognised = wordUnrecognisedLoad
    
    if not params['negative_connections']:
        for i in range(wordNum):
            for j in range(wordNum):
                if connection_matrix[i, j] < 0:
                    connection_matrix[i, j] = 0.0

    if wordUnrecognisedFlag:
        for i in range(wordNum):
            if wordUnrecognised[i]:
                w = wordList[i]
                if not w in params['unrecognised_words']:
                    params['unrecognised_words'].append(w)
    block.lsa_matrix = connection_matrix.copy()

    #################
    # MAIN
    #################

    # creating words to iterate over in the activation formula
    words_for_activation = []

    iterations = len(block)

    # if number of cycles was limited to a value smaller
    # than the default number of cycles, iterate up to it
    if params['limit_cycles'] and params['iteration_limit'] < len(block):
            iterations = params['iteration_limit']
    if params['blank_cycle']:
        iterations = len(block)+1
    ################################################################################
    # iterating over all the cycles
    for iteration in range(iterations):
        # if this is not the first cycle
        if iteration > 0:
            # checking forward activation
            if params['forward_activation']:
                # iterating over all the units
                words_for_activation = list(index2word.values())

            else:
                # iterating over all the words until (not including) current cycle
                words_for_activation.extend(cycle2words[iteration - 1])

            # actual formula
            for word in words_for_activation:

                # initialize every new summation
                activation_sum = 0

                for other_word in words_for_activation:
                    if (word != other_word):
                        activation_temp = activation_matrix[(iteration - 1), word2index[other_word]] \
                                          * connection_matrix[word2index[word], word2index[other_word]]
                        activation_sum += activation_temp

                activation_sum *= (params['cohort'])
                activation_unit = activation_sum + params['decay']*activation_matrix[(iteration-1),word2index[word]]
                if params['blank_cycle'] and iteration == iterations - 1:  # maybe: and word not in block.inferred_units
                    if activation_sum > params['m']:
                        activation_unit = params['m']
                else:
                    if activation_sum > params['m'] or (word in cycle2words[iteration] and word not in block.inferred_units):
                        activation_unit = params['m']
                if(activation_unit > params['m']):
                    activation_unit = params['m']
                if(activation_unit < params['activation_threshold']):
                    activation_unit = 0
                activation_matrix[iteration, word2index[word]] = activation_unit

        # update connection matrix and working memory
        working_memory(iteration, params)
        update_cycle_connections(iteration, params)

    activation_matrix[activation_matrix < params['activation_threshold'] ] = 0
    # check if the cycle activation sum is smaller than cap memory value and repair it
    if (params['is_keep_reading_max'] == True):
        keep_reading_max(iterations,words_for_activation,params)
    elif(params['is_keep_cap_max'] == True):
        keep_cap_max(iterations,words_for_activation,params)
    block.activation_matrix = activation_matrix
    block.connection_matrix = connection_matrix
    block.index2word = index2word
    block.word2index = word2index
    block.cycle2words = cycle2words
    block.word2cycles = word2cycles

    return params

def keep_reading_max(iterations ,words_for_activation, params):
    global activation_matrix
    for cycle in range(iterations):
        max_unit = max(activation_matrix[cycle])
        if (activation_matrix[cycle].sum() < params['working_mem_cap']):
            temp =copy.deepcopy(activation_matrix[cycle])
            temp *= (params['m'] / max_unit)

            if (temp.sum() > params['working_mem_cap']):
                activation_matrix[cycle] *= (params['working_mem_cap']/activation_matrix[cycle].sum())
            else:
                activation_matrix[cycle] = temp



def keep_cap_max(iterations,words_for_activation,params):
    global activation_matrix
    for cycle in range(iterations):
        maximum_list_positions = []
        count = 0
        while (count < len(activation_matrix[cycle])):
            num_max = 0
            cycle_temp = activation_matrix[cycle]
            cycle_temp = np.array([0 if index in maximum_list_positions else val for index,val in enumerate(cycle_temp)])

            max_unit = max(cycle_temp)

            if max_unit == 0:
                break
            if (activation_matrix[cycle].sum() == params['working_mem_cap']):
                break
            if (activation_matrix[cycle].sum() < params['working_mem_cap']):
                temp = copy.deepcopy(activation_matrix[cycle])
                for i, cell in enumerate(temp):
                    if i not in maximum_list_positions:
                        temp[i] *= (params['m'] / max_unit)
                    else:
                        num_max +=1

                # maximum_list.append(max_unit)
                for position, val in enumerate(activation_matrix[cycle]):
                    if val == max_unit:
                        maximum_list_positions.append(position)
                # find the number of occurrences of mention value
                occurrences_mention = np.count_nonzero(activation_matrix[cycle] == params['m'])

                if (temp.sum() > params['working_mem_cap']):
                    if occurrences_mention == 0:
                        for i, cell in enumerate(activation_matrix[cycle]):
                            activation_matrix[cycle][i] *= (params['working_mem_cap'] / activation_matrix[cycle].sum())
                    else:
                        for i, cell in enumerate(activation_matrix[cycle]):
                            if i not in maximum_list_positions:
                                # activation_matrix[cycle][i] *= (params['working_mem_cap'] - num_max*params['m'] / activation_matrix[cycle].sum() - num_max*params['m'])
                                activation_matrix[cycle][i] *= ((params['working_mem_cap'] - occurrences_mention*params['m'])/ (activation_matrix[cycle].sum()- occurrences_mention*params['m']))
                else:
                    activation_matrix[cycle] = temp
            else:
                for i, cell in enumerate(activation_matrix[cycle]):
                    if i not in maximum_list_positions:
                        activation_matrix[cycle][i] *= (params['working_mem_cap'] / activation_matrix[cycle].sum())


            count +=1



    # global activation_matrix
    #
    # addition ={}
    # for iteration in range(iterations):
    #     if (activation_matrix[iteration].sum() < params['working_mem_cap']):
    #         lack = params['working_mem_cap'] - activation_matrix[iteration].sum()
    #         for word in words_for_activation:
    #             if (activation_matrix[iteration].sum() > params['working_mem_cap']):
    #                 break
    #             if (activation_matrix[iteration, word2index[word]] < params['m']):
    #                 original_val = activation_matrix[iteration, word2index[word]]
    #                 addition[word] = (activation_matrix[iteration, word2index[word]]/ activation_matrix[iteration].sum()) * lack
    #                 activation_matrix[iteration, word2index[word]] += addition[word]
    #                 if (activation_matrix[iteration, word2index[word]] > params['m']):
    #                     lack -= (params['m'] - original_val)
    #                     lack += activation_matrix[iteration, word2index[word]] - params['m']
    #                     activation_matrix[iteration, word2index[word]] = params['m']








def read_text(params):
    global activation_matrix
    global connection_matrix
    global index2word
    global word2index
    global cycle2words
    global word2cycles
    params['unrecognised_words'] = []
    params['irregular_text_id'] = []
    activation_matrix = np.zeros((1, 1))
    connection_matrix = np.zeros((1, 1))
    index2word = OrderedDict()
    word2index = OrderedDict()
    cycle2words = OrderedDict()
    word2cycles = OrderedDict()
    delimiter = '%del%'
    bl = lsaP.parse(os.path.normpath(params['input_path']), params, delimiter)
    if len(bl) < 1:
        params['error_flag'] = 1
        params['error_text'] = params['error_text'] + '\nERROR: Input file not found,open, or no blocks to read in file.'
        return params
    if len(bl) > 1 and params['loaded_connection_matrix']:
        params['error_flag'] = 1
        params['error_text'] = params['error_text'] + '\nERROR: Input file contains more than one text, cannot use initial matrix from file.\n'
        return params
    

    trueOutputPath = lsaP.find_true_output_path(params)
    directoryCheck = lsaP.output_directory(trueOutputPath)
    if not directoryCheck:
        params['error_flag'] = 1
        params['error_text'] = params['error_text'] + '\nERROR: Cannot create directory:\n ' + trueOutputPath + '\n'
        return params
    
    edited_texts = []
    delimiter_blocks = []

    start_time = time.time()
    for block in bl:
        print("--- %s seconds ---" % (time.time() - start_time))
        start_time = time.time()
        print(block.title)
        params = single_block_alg(block, params)
        if params['error_flag'] == 2 or params['error_flag'] == 3:
            break
        params = lsaP.single_output(params, block, trueOutputPath)
        edited_texts.append(lsaP.post_process(block))
        delimiter_blocks.append(block.delimiter)

    if params['error_flag'] != 2 and params['error_flag'] != 3:
        params = lsaP.joint_output(params, bl, trueOutputPath)
        params = lsaP.target_output(params, bl, trueOutputPath)
        params = lsaP.create_post_doc(params, edited_texts, delimiter_blocks, trueOutputPath, params['edited_text_type'])
        params['summary_text'] = "\nLS-R simulations were conducted successfully\nOutput files were created and saved\n\n"
        if len(params['unrecognised_words']) > 0:
            params['summary_text'] = params['summary_text'] + 'The following words were not recognized by LSA.\nThe words are marked in red in the edited texts:\n' + params['unrecognised_words'][0]
            i = 1
            while i < len(params['unrecognised_words']):
                params['summary_text'] = params['summary_text'] + ' ; ' + params['unrecognised_words'][i]
                i = i + 1
        if len(params['irregular_text_id']) > 0:
            params['summary_text'] = params['summary_text']+"\n\nThe following texts contain irregular number of parsers (/) in their names:\nTexts number "
            for val in params['irregular_text_id']:
                params['summary_text'] = params['summary_text'] + str(val)+" "

    return params

def most_frequent(List):
    dict = {}
    count, itm = 0, ''
    for item in reversed(List):
        dict[item] = dict.get(item, 0) + 1
        if dict[item] >= count:
            count, itm = dict[item], item
    return (itm)
