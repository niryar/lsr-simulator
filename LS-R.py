print('\n\n... Loading, please wait ...\n\n')
# try:
#     from Tkinter import *
#     from Tkinter import _setit as setItCommand
# except ImportError:
from tkinter import *
from tkinter import _setit as setItCommand
import win32console
import win32gui

try:
    from tkinter import filedialog
except ImportError:

    import tkFileDialog as filedialog
try:
    from tkinter import messagebox
except ImportError:
    from Tkinter import PhotoImage
try:
    from tkinter import PhotoImage
except ImportError:
    from tkinter import messagebox
import numpy as np
import math
from collections import OrderedDict
import lsaParser as lsaP
import lsaWeb as lsaW
import lsaReader as lsaM
import lsaIconsDocx as lsaI
import os
import copy

# parameters dictionary
global params
params = {}


class LsaGUIForm:
    def __init__(self, myParent, params, iconsFlag):

        self.windowsInformation = ['MainWindow', 'ParametersWindow', 'ParametersAdvanced', 'OutputColumnsWindow',
                                   'IgnoreWordsWindow',
                                   'ReplaceWordsWindow', 'IgnoreCharsWindow', 'ReplaceCharsWindow']
        self.typesInformation = ['Button', 'CheckButton', 'TextBoxText', 'TextBoxInteger', 'TextBoxLSA', 'TextBoxFile',
                                 'TextBoxFolder',
                                 'FileBox', 'FolderBox', 'LabelBox']
        self.mainWindowInformation = ['Button', 'CheckButton', 'TextBoxText', 'TextBoxInteger', 'TextBoxLSA',
                                      'TextBoxFile', 'TextBoxFolder',
                                      'FileBox', 'FolderBox', 'LabelBox']
        self.categories = ['text index', 'text name', 'conditions', 'last seen in cycle',
                           'activation in\nsecond to last cycle',
                           'activation in last cycle', 'activation sum', 'activation mean', 'lsa sum', 'lsa mean',
                           'connections sum column', 'connections mean column','connections sum row','connections mean row']
        self.parametersDict = params
        self.defaultParametersDict = []
        self.advancedShown = False
        self.iconFolder = 'icons'
        self.iconRunName = 'runningMan.png'
        self.iconRedXName = 'redX.png'
        self.iconRun = PhotoImage(file=os.path.join(self.iconFolder, self.iconRunName))
        self.iconRedX = PhotoImage(file=os.path.join(self.iconFolder, self.iconRedXName))
        self.iconRun = self.iconRun.subsample(4)
        self.iconRedX = self.iconRedX.subsample(10)
        self.closeText = "Apply And Close"
        self.numberOfColumnsIgnore = 4
        self.numberOfColumnsReplace = 2
        self.fontType = "Courier"
        self.fontSize = 11
        self.fontSizeMarkers = 12
        self.first_entry = True

        self.varSaveInSamePath = BooleanVar();
        #        self.varCaseSensitive = BooleanVar();
        # self.varInitial_connection_matrix = BooleanVar();
        self.varLoaded_connection_matrix = BooleanVar();
        self.varIs_mem_cap = BooleanVar();
        self.var_keep_max = StringVar()
        # self.varIs_keep_reading_max = BooleanVar()
        # self.varIs_keep_cap_max = BooleanVar()
        # self.varSelf_activation = BooleanVar();
        self.varForward_activation = BooleanVar();
        self.varBlank_cycle = BooleanVar();
        self.varNegative_connections = BooleanVar();
        self.varReduced_expectancy = BooleanVar();
        self.varLinear_function = IntVar();

        self.varSigmoid_connections = IntVar();
        self.varNumbers_replace = BooleanVar();
        self.varOrdinals_replace = BooleanVar();
        self.varLimit_cycles = BooleanVar();
        self.varWord_is_cycle = BooleanVar();
        self.varContractions_replace = BooleanVar();
        #        self.varAppend_DateTime_To_Output = BooleanVar();
        self.varShow_All_Columns = BooleanVar();
        self.varShowColumns = [];
        for i in range(len(self.categories)):
            self.varShowColumns.append(BooleanVar());

        self.varDecimal_round = IntVar();
        self.varDecimal_roundStr = StringVar();
        self.varLsaChoice = IntVar();
        self.varLsaChoiceStr = StringVar();
        self.varIteration_limit = IntVar();
        self.varIteration_limitStr = StringVar();
        self.varM = DoubleVar();
        self.varMStr = StringVar();
        self.varActivation_threshold = DoubleVar();
        self.varActivation_thresholdStr = StringVar();

        self.varConnection_threshold = DoubleVar();
        self.varConnection_thresholdStr = StringVar();

        self.varWorking_mem_cap = DoubleVar();
        self.varWorking_mem_capStr = StringVar();
        self.varLearning_rate = DoubleVar();
        self.varLearning_rateStr = StringVar();
        self.varCohort = DoubleVar();
        self.varCohortStr = StringVar();
        self.varDecay = DoubleVar();
        self.varDecayStr = StringVar();
        self.varSemantic_weight = DoubleVar();
        self.varSemantic_weightStr = StringVar();
        self.varEpisodic_weight = DoubleVar();
        self.varEpisodic_weightStr = StringVar();
        self.varSigmoid_min = DoubleVar();
        self.varSigmoid_minStr = StringVar();
        self.varSigmoid_max = DoubleVar();
        self.varSigmoid_maxStr = StringVar();

        self.varInput_Path = StringVar();
        self.varOutput_Path = StringVar();
        #        self.varOutput_Name = StringVar();
        #        self.varReset_Configuration_Path = StringVar();
        self.varSave_Configuration_Path = StringVar();
        self.varLoad_Configuration_Path = StringVar();
        self.varLoad_Connections_Path = StringVar();
        self.varSelect_Ignored_Words = BooleanVar();
        self.varSelect_Ignored_Chars = BooleanVar();
        self.varSelect_Replace_Words = BooleanVar();
        self.varSelect_Replace_Chars = BooleanVar();
        self.varIgnored_Words_num = 1;
        self.varIgnored_Chars_num = 1;
        self.varReplace_Words_num = 1;
        self.varReplace_Chars_num = 1;
        self.varIgnored_Words_Boxes = [];
        self.varIgnored_Chars_Boxes = [];
        self.varReplace_Words_1_Boxes = [];
        self.varReplace_Words_2_Boxes = [];
        self.varReplace_Chars_1_Boxes = [];
        self.varReplace_Chars_2_Boxes = [];
        self.varIgnored_Words_Check = [];
        self.varIgnored_Chars_Check = [];
        self.varReplace_Words_Check = [];
        self.varReplace_Chars_Check = [];
        self.lsaChoicesArr = ['1st Year College', '12th Grade      ', '9th Grade       ', '6th Grade       ',
                              '3rd Grade       ']
        self.lsaChoices = ['1st Year College', '12th Grade      ', '9th Grade       ', '6th Grade       ',
                           '3rd Grade       ']
        self.lsaDict = {'1st Year College': 1, '12th Grade      ': 12, '9th Grade       ': 9, '6th Grade       ': 6,
                        '3rd Grade       ': 3}
        self.lsaDictReverse = {1: '1st Year College', 12: '12th Grade      ', 9: '9th Grade       ',
                               6: '6th Grade       ', 3: '3rd Grade       '}

        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.containerMain = Frame(myParent)
        self.containerMain1 = Frame(self.containerMain)
        self.containerMain2 = Frame(self.containerMain)
        self.containerMain3 = Frame(self.containerMain)
        self.containerMain4 = Frame(self.containerMain)
        self.containerMain5 = Frame(self.containerMain)

        self.buttonEntryInputPath = Button(self.containerMain1, command=self.buttonEntryInputPathClick)
        self.buttonEntryInputPath.config(font=(self.fontType, self.fontSize))
        self.buttonEntryInputPath["text"] = "Input File"
        self.buttonEntryInputPath["background"] = buttonColour
        self.entryInputPath = Entry(self.containerMain1, textvariable=self.varInput_Path)
        self.entryInputPath.config(font=(self.fontType, self.fontSize))
        self.buttonSameFolder = Checkbutton(self.containerMain1, variable=self.varSaveInSamePath,
                                            command=self.buttonSameFolderClick)
        self.buttonSameFolder.config(font=(self.fontType, self.fontSize))
        self.buttonSameFolder["text"] = "Save Output In Input Folder"
        self.buttonSameFolder["background"] = buttonColour
        self.buttonEntryOutputPath = Button(self.containerMain1, command=self.buttonEntryOutputPathClick)
        self.buttonEntryOutputPath.config(font=(self.fontType, self.fontSize))
        self.buttonEntryOutputPath["text"] = "Output Folder"
        self.buttonEntryOutputPath["background"] = buttonColour
        self.entryOutputPath = Entry(self.containerMain1, textvariable=self.varOutput_Path)
        self.entryOutputPath.config(font=(self.fontType, self.fontSize))
        ##        self.labelOutputName = Label(self.containerMain1, text="Output Name:")
        ##        self.entryOutputName = Entry(self.containerMain1, textvariable=self.varOutput_Name)
        ##        self.buttonAppendDateTime = Checkbutton(self.containerMain1 , variable=self.varAppend_DateTime_To_Output)
        ##        self.buttonAppendDateTime["text"]= "Append date and time to name"
        ##        self.buttonAppendDateTime["background"] = buttonColour

        self.buttonStart = Button(self.containerMain2, command=self.buttonStartClick)
        self.buttonStart.config(font=(self.fontType, self.fontSize))
        self.buttonStart["background"] = buttonColour
        self.buttonStart.configure(image=self.iconRun,
                                   width=100,
                                   padx=button_padx,
                                   pady=button_pady
                                   )
        self.labelStart = Label(self.containerMain2, text="Run Simulation")
        self.labelStart.config(font=(self.fontType, self.fontSize))
        #        self.labelStart["background"] = buttonColour

        self.markers = Label(self.containerMain2, text="\nText markers:\n& : new text\n* : text name\n/ or \ : conditions parser\n- : multiple words unit\n@ : target unit\n0 : inferred unit")
        self.markers.config(font=(self.fontType, self.fontSizeMarkers))

        self.notes = Label(self.containerMain2,
                             text="\nImportant notes:\nSimulator accepts only English\nClose input and output files to run simulation\n‘save as’ an output file to open it in SPSS")
        self.notes.config(font=(self.fontType, self.fontSizeMarkers))

        self.buttonConfigSavePath = Button(self.containerMain5, command=self.buttonConfigSavePathClick)
        self.buttonConfigSavePath.config(font=(self.fontType, self.fontSize))
        self.buttonConfigSavePath.config(font=(self.fontType, self.fontSize))
        self.buttonConfigSavePath["text"] = "Save Parameters File"
        self.buttonConfigSavePath["background"] = buttonColour
        self.entryConfigSavePath = Entry(self.containerMain5, textvariable=self.varSave_Configuration_Path)
        self.entryConfigSavePath.config(font=(self.fontType, self.fontSize))
        self.buttonConfigLoadPath = Button(self.containerMain5, command=self.buttonConfigLoadPathClick)
        self.buttonConfigLoadPath.config(font=(self.fontType, self.fontSize))
        self.buttonConfigLoadPath["text"] = "Load Parameters File"
        self.buttonConfigLoadPath["background"] = buttonColour
        self.entryConfigLoadPath = Entry(self.containerMain5, textvariable=self.varLoad_Configuration_Path)
        self.entryConfigLoadPath.config(font=(self.fontType, self.fontSize))
        self.buttonResetConfiguration = Button(self.containerMain5, command=self.buttonResetConfigurationClick)
        self.buttonResetConfiguration.config(font=(self.fontType, self.fontSize))
        self.buttonResetConfiguration["text"] = "Reset Parameters\nTo Default"
        self.buttonResetConfiguration["background"] = buttonColour

        self.buttonParametersShow = Button(self.containerMain3, command=self.buttonParametersShowClick)
        self.buttonParametersShow.config(font=(self.fontType, self.fontSize))
        self.buttonParametersShow["text"] = "Parameters"
        self.buttonParametersShow["background"] = buttonColour
        self.buttonParametersShow.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.buttonOutputColumnsShow = Button(self.containerMain3, command=self.buttonOutputColumnsShowClick)
        self.buttonOutputColumnsShow.config(font=(self.fontType, self.fontSize))
        self.buttonOutputColumnsShow["text"] = "Output Columns"
        self.buttonOutputColumnsShow["background"] = buttonColour
        self.buttonOutputColumnsShow.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.buttonIgnoreWordsShow = Button(self.containerMain4, command=self.buttonIgnoreWordsShowClick)
        self.buttonIgnoreWordsShow.config(font=(self.fontType, self.fontSize))
        self.buttonIgnoreWordsShow["text"] = "Ignored Words"
        self.buttonIgnoreWordsShow["background"] = buttonColour
        self.buttonIgnoreWordsShow.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.buttonIgnoreCharactersShow = Button(self.containerMain4, command=self.buttonIgnoreCharactersShowClick)
        self.buttonIgnoreCharactersShow.config(font=(self.fontType, self.fontSize))
        self.buttonIgnoreCharactersShow["text"] = "Ignored Characters"
        self.buttonIgnoreCharactersShow["background"] = buttonColour
        self.buttonIgnoreCharactersShow.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.buttonReplaceWordsShow = Button(self.containerMain4, command=self.buttonReplaceWordsShowClick)
        self.buttonReplaceWordsShow.config(font=(self.fontType, self.fontSize))
        self.buttonReplaceWordsShow["text"] = "Replace Words"
        self.buttonReplaceWordsShow["background"] = buttonColour
        self.buttonReplaceWordsShow.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.buttonReplaceCharactersShow = Button(self.containerMain4, command=self.buttonReplaceCharactersShowClick)
        self.buttonReplaceCharactersShow.config(font=(self.fontType, self.fontSize))
        self.buttonReplaceCharactersShow["text"] = "Replace Characters"
        self.buttonReplaceCharactersShow["background"] = buttonColour
        self.buttonReplaceCharactersShow.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.containerParameters = Frame(myParent)
        self.containerParametersRowExit = Frame(self.containerParameters)
        self.containerParametersNormal = Frame(self.containerParameters)
        self.containerGradeLevel = Frame(self.containerParametersNormal)
        self.containerParametersMemCaprow = Frame(self.containerParametersNormal)
        # self.containerParametersOptionsMemCapRow = Frame(self.containerParametersNormal)
        self.containerParametersMentionrow = Frame(self.containerParametersNormal)
        self.containerParametersLearningRaterow = Frame(self.containerParametersNormal)
        self.containerParametersCohortrow = Frame(self.containerParametersNormal)
        self.containerParametersDecayrow = Frame(self.containerParametersNormal)
        self.containerParametersActiveThresholdrow = Frame(self.containerParametersNormal)
        self.containerParametersConnectionThresholdrow = Frame(self.containerParametersNormal)
        self.containerParametersSemanticWeightrow = Frame(self.containerParametersNormal)
        self.containerParametersEpisodicWeightrow = Frame(self.containerParametersNormal)
        self.containerParametersShowAdvancedrow = Frame(self.containerParametersNormal)
        self.containerParametersSaveAsDefaultBasic = Frame(self.containerParametersNormal)

        self.containerParametersAdvanced = Frame(self.containerParameters)
        # self.containerParametersLSArow = Frame(self.containerParametersAdvanced)
        self.containerParametersLoadMatrixrow = Frame(self.containerParametersAdvanced)
        self.containerParametersDecimalrow = Frame(self.containerParametersAdvanced)
        self.containerParametersOptionsMemCapRow = Frame(self.containerParametersAdvanced)
        self.containerParametersSelfActiverow = Frame(self.containerParametersAdvanced)
        self.containerParametersForewardActiverow = Frame(self.containerParametersAdvanced)
        self.containerParametersNegativeConnectionsrow = Frame(self.containerParametersAdvanced)
        self.containerParametersSigmoidrow = Frame(self.containerParametersAdvanced)
        self.containerParametersLimitCyclesrow = Frame(self.containerParametersAdvanced)
        self.containerParametersWordIsCyclerow = Frame(self.containerParametersAdvanced)
        self.containerParametersBlankCyclerow = Frame(self.containerParametersAdvanced)
        self.containerParametersReducedExpectancyrow = Frame(self.containerParametersAdvanced)
        self.containerParametersDefaultConfigrow = Frame(self.containerParametersAdvanced)
        self.containerParametersShowNormalrow = Frame(self.containerParametersAdvanced)

        #        self.buttonCaseSensitive = Checkbutton(self.containerParameters2 )
        #        self.buttonCaseSensitive["text"]= "Case Sensitive"
        #        self.buttonCaseSensitive["variable"] = self.varCaseSensitive;
        self.buttonIs_mem_cap = Checkbutton(self.containerParametersMemCaprow, command=self.buttonIs_mem_capClick)
        self.buttonIs_mem_cap.config(font=(self.fontType, self.fontSize))
        self.buttonIs_mem_cap["text"] = "Working Memory Capacity: "
        self.buttonIs_mem_cap["variable"] = self.varIs_mem_cap

        # self.labelWorking_mem_cap = Label(self.containerParametersMemCaprow, text="Working Memory Cap")
        # self.labelWorking_mem_cap.config(font=(self.fontType, self.fontSize))
        self.entryWorking_mem_cap = Entry(self.containerParametersMemCaprow, textvariable=self.varWorking_mem_capStr,
                                          validate="focusout")
        self.entryWorking_mem_cap.config(font=(self.fontType, self.fontSize), width=10)

        self.R1 = Radiobutton(self.containerParametersOptionsMemCapRow,text= "Keep Reading Max", value="keep reading max", var= self.var_keep_max)
        self.R1.config(font=(self.fontType, self.fontSize))
        self.R2 = Radiobutton(self.containerParametersOptionsMemCapRow,text= "Keep CAP Max", value="keep CAP max", var= self.var_keep_max)
        self.R2.config(font=(self.fontType, self.fontSize))
        self.R3 = Radiobutton(self.containerParametersOptionsMemCapRow,text="No Correction", value="Without correction", var= self.var_keep_max)
        self.R3.config(font=(self.fontType, self.fontSize))

        # self.keepA = Radiobutton(self.containerParametersOptionsMemCapRow)
        # self.keepA.config(font=(self.fontType,self.fontSize))
        # self.keepA["text"] = "Keep reading max"
        # self.keepA["variable"] = self.var_kepp_max
        #
        # self.keepB = Radiobutton(self.containerParametersOptionsMemCapRow)
        # self.keepB.config(font=(self.fontType,self.fontSize))
        # self.keepB["text"] = "Keep CAP max"
        # self.keepB["variable"] = self.varIs_keep_cap_max

        self.labelM = Label(self.containerParametersMentionrow, text="Reading (max) Value")
        self.labelM.config(font=(self.fontType, self.fontSize))
        self.entryM = Entry(self.containerParametersMentionrow, textvariable=self.varMStr, validate="focusout")
        self.entryM.config(font=(self.fontType, self.fontSize), width=10)

        self.labelLearning_rate = Label(self.containerParametersLearningRaterow, text="Learning Rate")
        self.labelLearning_rate.config(font=(self.fontType, self.fontSize))
        self.entryLearning_rate = Entry(self.containerParametersLearningRaterow, textvariable=self.varLearning_rateStr,
                                        validate="focusout")
        self.entryLearning_rate.config(font=(self.fontType, self.fontSize), width=10)

        self.labelCohort = Label(self.containerParametersCohortrow, text="Cohort")
        self.labelCohort.config(font=(self.fontType, self.fontSize))
        self.entryCohort = Entry(self.containerParametersCohortrow, textvariable=self.varCohortStr, validate="focusout")
        self.entryCohort.config(font=(self.fontType, self.fontSize), width=10)
        self.labelDecay = Label(self.containerParametersDecayrow, text="Activation Decay Rate")
        self.labelDecay.config(font=(self.fontType, self.fontSize))
        self.entryDecay = Entry(self.containerParametersDecayrow, textvariable=self.varDecayStr, validate="focusout")
        self.entryDecay.config(font=(self.fontType, self.fontSize), width=10)

        self.labelActivation_threshold = Label(self.containerParametersActiveThresholdrow, text="Activation Threshold")
        self.labelActivation_threshold.config(font=(self.fontType, self.fontSize))
        self.entryActivation_threshold = Entry(self.containerParametersActiveThresholdrow,
                                               textvariable=self.varActivation_thresholdStr, validate="focusout")
        self.entryActivation_threshold.config(font=(self.fontType, self.fontSize), width=10)

        self.labelConnection_threshold = Label(self.containerParametersConnectionThresholdrow,
                                               text="Connection Threshold")
        self.labelConnection_threshold.config(font=(self.fontType, self.fontSize))
        self.entryConnection_threshold = Entry(self.containerParametersConnectionThresholdrow,
                                               textvariable=self.varConnection_thresholdStr, validate="focusout")
        self.entryConnection_threshold.config(font=(self.fontType, self.fontSize), width=10)

        self.labelSemantic_weight = Label(self.containerParametersSemanticWeightrow, text="Semantic Connection Weight")
        self.labelSemantic_weight.config(font=(self.fontType, self.fontSize))
        self.entrySemantic_weight = Entry(self.containerParametersSemanticWeightrow,
                                          textvariable=self.varSemantic_weightStr, validate="focusout")
        self.entrySemantic_weight.config(font=(self.fontType, self.fontSize), width=10)

        self.labelEpisodic_weight = Label(self.containerParametersEpisodicWeightrow, text="Episodic Connection Weight")
        self.labelEpisodic_weight.config(font=(self.fontType, self.fontSize))
        self.entryEpisodic_weight = Entry(self.containerParametersEpisodicWeightrow,
                                          textvariable=self.varEpisodic_weightStr, validate="focusout")
        self.entryEpisodic_weight.config(font=(self.fontType, self.fontSize), width=10)

        self.labelLsaChoice = Label(self.containerGradeLevel, text="Semantic Knowledge (LSA) Grade Level")
        self.labelLsaChoice.config(font=(self.fontType, self.fontSize))

        self.entryLsaChoice = OptionMenu(self.containerGradeLevel, self.varLsaChoiceStr, *self.lsaChoices)
        self.entryLsaChoice['menu'].delete(0, 'end')
        for lsaC in self.lsaChoicesArr:
            self.entryLsaChoice['menu'].add_command(label=lsaC, command=setItCommand(self.varLsaChoiceStr, lsaC))
        self.entryLsaChoice.config(font=(self.fontType, self.fontSize), bg="WHITE")
        self.entryLsaChoice["menu"].config(bg="WHITE")


        # self.buttonSelf_activation = Checkbutton(self.containerParametersForewardActiverow)
        # self.buttonSelf_activation.config(font=(self.fontType, self.fontSize))
        # self.buttonSelf_activation["text"] = "Self Activation"
        # self.buttonSelf_activation["variable"] = self.varSelf_activation;

        self.buttonShowAdvanced = Button(self.containerParametersShowAdvancedrow)
        self.buttonShowAdvanced.config(font=(self.fontType, self.fontSize))
        self.buttonShowAdvanced["text"] = "Show Advanced Parameters"
        self.buttonShowAdvanced["command"] = self.buttonShowAdvancedClick;
        self.buttonShowAdvanced["background"] = buttonColour
        self.buttonShowAdvanced.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.buttonSaveAsDef = Button(self.containerParametersSaveAsDefaultBasic)
        self.buttonSaveAsDef.config(font=(self.fontType, self.fontSize))
        self.buttonSaveAsDef["text"] = "Save Parameters As\nDefault Parameters"
        self.buttonSaveAsDef["command"] = self.buttonCreateDefaultConfigurationFileClick
        self.buttonSaveAsDef["background"] = buttonColour
        # self.buttonSaveAsDef.configure(
        #     width=button_width,
        #     padx=button_padx,
        #     pady=button_pady
        # )

        # self.basic = Button(self.containerParametersSaveAsDefaultBasic,
        #                                              command=self.buttonCreateDefaultConfigurationFileClick)
        # self.basic.config(font=(self.fontType, self.fontSize))
        # self.basic["text"] = "Save Parameters As\nDefault Parameters"
        # self.basic["background"] = buttonColour

        self.labelDecimal_round_part1 = Label(self.containerParametersDecimalrow, text="Round Values To")
        self.labelDecimal_round_part1.config(font=(self.fontType, self.fontSize))
        self.entryDecimal_round = Entry(self.containerParametersDecimalrow, textvariable=self.varDecimal_roundStr,
                                        validate="focusout")
        self.entryDecimal_round.config(font=(self.fontType, self.fontSize), width=10)
        self.labelDecimal_round_part2 = Label(self.containerParametersDecimalrow, text="Decimal Digits")
        self.labelDecimal_round_part2.config(font=(self.fontType, self.fontSize))

        self.buttonForward_activation = Checkbutton(self.containerParametersForewardActiverow)
        self.buttonForward_activation.config(font=(self.fontType, self.fontSize))
        self.buttonForward_activation["text"] = "Forward Activation"
        self.buttonForward_activation["variable"] = self.varForward_activation;
        self.buttonNegative_connections = Checkbutton(self.containerParametersNegativeConnectionsrow)
        self.buttonNegative_connections.config(font=(self.fontType, self.fontSize))
        self.buttonNegative_connections["text"] = "Negative Connections"
        self.buttonNegative_connections["variable"] = self.varNegative_connections;


        # self.buttonIs_keep_reading_max = Radiobutton(self.containerParametersOptionsMemCapRow,
        #                                              command=self.button_Is_keep_reading_max_Click)
        # self.buttonIs_keep_reading_max.config(font=(self.fontType, self.fontSize))
        # self.buttonIs_keep_reading_max["text"] = "Keep reading max"
        # self.buttonIs_keep_reading_max["variable"] = self.varIs_keep_reading_max



        self.buttonLinear_function = Radiobutton(self.containerParametersSigmoidrow, value=1,
                                                 command=self.buttonLinear_functionClick)
        self.buttonLinear_function.config(font=(self.fontType, self.fontSize))
        self.buttonLinear_function["text"] = "Linear Function"
        self.buttonLinear_function["variable"] = self.varLinear_function

        self.buttonSigmoid_connections = Radiobutton(self.containerParametersSigmoidrow, value=1,
                                                     command=self.buttonSigmoid_connectionsClick)
        self.buttonSigmoid_connections.config(font=(self.fontType, self.fontSize))
        self.buttonSigmoid_connections["text"] = "Sigmoid Function: "
        self.buttonSigmoid_connections["variable"] = self.varSigmoid_connections

        self.labelSigmoid_min = Label(self.containerParametersSigmoidrow, text="Min")
        self.labelSigmoid_min.config(font=(self.fontType, self.fontSize))
        self.entrySigmoid_min = Entry(self.containerParametersSigmoidrow, textvariable=self.varSigmoid_minStr,
                                      validate="focusout")
        self.entrySigmoid_min.config(font=(self.fontType, self.fontSize), width=10)
        self.labelSigmoid_max = Label(self.containerParametersSigmoidrow, text="Max")
        self.labelSigmoid_max.config(font=(self.fontType, self.fontSize))
        self.entrySigmoid_max = Entry(self.containerParametersSigmoidrow, textvariable=self.varSigmoid_maxStr,
                                      validate="focusout")
        self.entrySigmoid_max.config(font=(self.fontType, self.fontSize), width=10)
        self.buttonLimit_cycles = Checkbutton(self.containerParametersLimitCyclesrow,
                                              command=self.buttonLimit_cyclesClick)
        self.buttonLimit_cycles.config(font=(self.fontType, self.fontSize))
        # self.buttonLimit_cycles["text"] = "Limit Number Of Cycles:"
        self.buttonLimit_cycles["variable"] = self.varLimit_cycles;
        self.labelIteration_limit = Label(self.containerParametersLimitCyclesrow, text="Limit Number Of Cycles")
        self.labelIteration_limit.config(font=(self.fontType, self.fontSize))
        self.entryIteration_limit = Entry(self.containerParametersLimitCyclesrow,
                                          textvariable=self.varIteration_limitStr, validate="focusout")
        self.entryIteration_limit.config(font=(self.fontType, self.fontSize), width=10)
        self.buttonWord_is_cycle = Checkbutton(self.containerParametersWordIsCyclerow)
        self.buttonWord_is_cycle.config(font=(self.fontType, self.fontSize))
        self.buttonWord_is_cycle["text"] = "Each Unit Is A Different Cycle"
        self.buttonWord_is_cycle["variable"] = self.varWord_is_cycle
        self.buttonBlank_cycle = Checkbutton(self.containerParametersBlankCyclerow)
        self.buttonBlank_cycle.config(font=(self.fontType, self.fontSize))
        self.buttonBlank_cycle["text"] = "Blank Cycle"
        self.buttonBlank_cycle["variable"] = self.varBlank_cycle;
        self.buttonReduced_expectancy = Checkbutton(self.containerParametersReducedExpectancyrow)
        self.buttonReduced_expectancy.config(font=(self.fontType, self.fontSize))
        self.buttonReduced_expectancy["text"] = "Reduced Expectancy"
        self.buttonReduced_expectancy["variable"] = self.varReduced_expectancy

        # self.buttonInitial_connection_matrix = Checkbutton(self.containerParametersLSArow,
        #                                                    command=self.buttonMatrixLSAClick)
        # self.buttonInitial_connection_matrix.config(font=(self.fontType, self.fontSize))
        # self.buttonInitial_connection_matrix["text"] = "Import Initial Connection Matrix From LSA:"
        # self.buttonInitial_connection_matrix["variable"] = self.varInitial_connection_matrix

        # self.labelLsaChoice = Label(self.containerParametersLSArow, text="Semantic Knowledge (LSA) Grade Level")
        # self.labelLsaChoice.config(font=(self.fontType, self.fontSize))
        #
        # self.entryLsaChoice = OptionMenu(self.containerParametersLSArow, self.varLsaChoiceStr, *self.lsaChoices)
        # self.entryLsaChoice['menu'].delete(0, 'end')
        # for lsaC in self.lsaChoicesArr:
        #     self.entryLsaChoice['menu'].add_command(label=lsaC, command=setItCommand(self.varLsaChoiceStr, lsaC))
        # self.entryLsaChoice.config(font=(self.fontType, self.fontSize), bg="WHITE")
        # self.entryLsaChoice["menu"].config(bg="WHITE")

        self.buttonLoad_connection_matrix = Checkbutton(self.containerParametersLoadMatrixrow,
                                                        command=self.buttonMatrixLoadClick)
        self.buttonLoad_connection_matrix.config(font=(self.fontType, self.fontSize))
        # self.buttonLoad_connection_matrix["text"] = "Load Initial Matrix From File"
        self.buttonLoad_connection_matrix["variable"] = self.varLoaded_connection_matrix
        self.buttonMatrixLoadPath = Button(self.containerParametersLoadMatrixrow,
                                           command=self.buttonMatrixLoadPathClick)
        self.buttonMatrixLoadPath.config(font=(self.fontType, self.fontSize))
        self.buttonMatrixLoadPath["text"] = "Load Initial Connections Matrix File"
        self.entryMatrixLoadPath = Entry(self.containerParametersLoadMatrixrow,
                                         textvariable=self.varLoad_Connections_Path, validate="focusout")
        self.entryMatrixLoadPath.config(font=(self.fontType, self.fontSize))

        self.buttonSaveDefaultConfiguration = Button(self.containerParametersDefaultConfigrow,
                                                     command=self.buttonCreateDefaultConfigurationFileClick)
        self.buttonSaveDefaultConfiguration.config(font=(self.fontType, self.fontSize))
        self.buttonSaveDefaultConfiguration["text"] = "Save Parameters As\nDefault Parameters"
        self.buttonSaveDefaultConfiguration["background"] = buttonColour

        self.buttonShowNormal = Button(self.containerParametersShowNormalrow)
        self.buttonShowNormal.config(font=(self.fontType, self.fontSize))
        self.buttonShowNormal["text"] = "Show Regular Parameters"
        self.buttonShowNormal["command"] = self.buttonShowNormalClick;
        self.buttonShowNormal["background"] = buttonColour
        self.buttonShowNormal.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.buttonSaveParameters = Button(self.containerParametersRowExit, command=self.buttonSaveParametersClick)
        self.buttonSaveParameters["background"] = buttonColour
        self.buttonSaveParameters.configure(image=self.iconRedX,
                                            width=button_width,
                                            padx=button_padx,
                                            pady=button_pady
                                            )
        self.labelCloseParameters = Label(self.containerParametersRowExit, text=self.closeText)
        self.labelCloseParameters.config(font=(self.fontType, self.fontSize))
        self.labelCloseParameters["background"] = buttonColour

        self.containerOutputColumns = Frame(myParent)
        self.containerOutputColumns1 = Frame(self.containerOutputColumns)
        self.containerOutputColumns2 = Frame(self.containerOutputColumns)
        self.containerOutputColumns3 = Frame(self.containerOutputColumns)

        ##        self.buttonShowAllColumns = Checkbutton(self.containerOutputColumns2 , command=self.buttonShowAllColumnsClick)
        ##        self.buttonShowAllColumns["variable"] = self.varShow_All_Columns
        ##        self.buttonShowAllColumns.config(font=(self.fontType, self.fontSize))
        ##        self.buttonShowAllColumns["text"]= "Show All Columns"
        ##        self.buttonShowAllColumns["background"] = buttonColour
        ##        self.buttonShowAllColumns.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonShowColumns = [];
        for i in range(len(self.categories)):
            self.buttonShowColumns.append(Checkbutton(self.containerOutputColumns3));
            self.buttonShowColumns[i]["variable"] = self.varShowColumns[i]
            self.buttonShowColumns[i].config(font=(self.fontType, self.fontSize))
            self.buttonShowColumns[i]["text"] = self.categories[i]
            self.buttonShowColumns[i]["background"] = buttonColour
            self.buttonShowColumns[i].configure(
                width=button_width,
                padx=button_padx,
                pady=button_pady
            )
        self.buttonCloseColumns = Button(self.containerOutputColumns1, command=self.buttonCloseColumnsClick)
        self.buttonCloseColumns["background"] = buttonColour
        self.buttonCloseColumns.configure(image=self.iconRedX,
                                          width=button_width,
                                          padx=button_padx,
                                          pady=button_pady
                                          )
        self.labelCloseColumns = Label(self.containerOutputColumns1, text=self.closeText)
        self.labelCloseColumns.config(font=(self.fontType, self.fontSize))
        self.labelCloseColumns["background"] = buttonColour

        self.containerIgnoreWords = Frame(myParent)
        self.containerIgnoreWords1 = Frame(self.containerIgnoreWords)
        self.containerIgnoreWordsOptionsRow = Frame(self.containerIgnoreWords)
        self.containerIgnoreWordsLabelRow = Frame(self.containerIgnoreWords)
        self.containerIgnoreWords2 = VerticalScrolledFrame(self.containerIgnoreWords)
        self.containerIgnoreWordsRows = []
        self.containerIgnoreWordsCheckBoxList = []
        self.containerIgnoreWordsTextBoxList = []
        self.containerIgnoreWordsAddRow = Frame(self.containerIgnoreWords)
        self.labelIgnoreWords = Label(self.containerIgnoreWordsLabelRow, text="List of words to remove from text:")
        self.labelIgnoreWords.config(font=(self.fontType, self.fontSize))

        self.buttonSelectIgnoreWords = Checkbutton(self.containerIgnoreWordsOptionsRow,
                                                   command=self.buttonSelectIgnoreWordsClick)
        self.buttonSelectIgnoreWords["variable"] = self.varSelect_Ignored_Words
        self.buttonSelectIgnoreWords.config(font=(self.fontType, self.fontSize))
        self.buttonSelectIgnoreWords["text"] = "Deselect All Words"
        ##        self.buttonSelectIgnoreWords.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonSortIgnoreWords = Button(self.containerIgnoreWordsOptionsRow,
                                            command=self.buttonSortIgnoreWordsClick)
        self.buttonSortIgnoreWords.config(font=(self.fontType, self.fontSize))
        self.buttonSortIgnoreWords["text"] = "Sort Words Alphabetically"
        self.buttonSortIgnoreWords["background"] = buttonColour
        ##        self.buttonSortIgnoreWords.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonRemoveIgnoreWords = Button(self.containerIgnoreWordsOptionsRow,
                                              command=self.buttonRemoveIgnoreWordsClick)
        self.buttonRemoveIgnoreWords.config(font=(self.fontType, self.fontSize))
        self.buttonRemoveIgnoreWords["text"] = "Remove Deselected Words"
        self.buttonRemoveIgnoreWords["background"] = buttonColour
        ##        self.buttonRemoveIgnoreWords.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)

        self.buttonAddIgnoreWords = Button(self.containerIgnoreWordsAddRow, command=self.buttonAddIgnoreWordsClick)
        self.buttonAddIgnoreWords.config(font=(self.fontType, self.fontSize))
        if self.numberOfColumnsIgnore < 2:
            self.buttonAddIgnoreWords["text"] = "Add Word"
        else:
            self.buttonAddIgnoreWords["text"] = "Add Row Of Words"
        self.buttonAddIgnoreWords["background"] = buttonColour
        self.buttonAddIgnoreWords.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )

        self.buttonCloseIgnoreWords = Button(self.containerIgnoreWords1, command=self.buttonCloseIgnoreWordsClick)
        self.buttonCloseIgnoreWords.config(font=(self.fontType, self.fontSize))
        self.buttonCloseIgnoreWords["background"] = buttonColour
        self.buttonCloseIgnoreWords.configure(image=self.iconRedX,
                                              width=button_width,
                                              padx=button_padx,
                                              pady=button_pady
                                              )
        self.labelCloseIgnoreWords = Label(self.containerIgnoreWords1, text=self.closeText)
        self.labelCloseIgnoreWords.config(font=(self.fontType, self.fontSize))
        self.labelCloseIgnoreWords["background"] = buttonColour

        self.containerIgnoreCharacters = Frame(myParent)
        self.containerIgnoreCharacters1 = Frame(self.containerIgnoreCharacters)
        self.containerIgnoreCharactersOptionsRow = Frame(self.containerIgnoreCharacters)
        self.containerIgnoreCharactersLabelRow = Frame(self.containerIgnoreCharacters)
        self.containerIgnoreCharacters2 = VerticalScrolledFrame(self.containerIgnoreCharacters)
        self.containerIgnoreCharactersRows = []
        self.containerIgnoreCharactersCheckBoxList = []
        self.containerIgnoreCharactersTextBoxList = []
        self.containerIgnoreCharactersAddRow = Frame(self.containerIgnoreCharacters)

        self.labelIgnoreCharacters = Label(self.containerIgnoreCharactersLabelRow,
                                           text="List of characters to remove from text:")
        self.labelIgnoreCharacters.config(font=(self.fontType, self.fontSize))

        self.buttonSelectIgnoreCharacters = Checkbutton(self.containerIgnoreCharactersOptionsRow,
                                                        command=self.buttonSelectIgnoreCharactersClick)
        self.buttonSelectIgnoreCharacters["variable"] = self.varSelect_Ignored_Chars
        self.buttonSelectIgnoreCharacters.config(font=(self.fontType, self.fontSize))
        self.buttonSelectIgnoreCharacters["text"] = "Deselect All Characters"
        ##        self.buttonSelectIgnoreCharacters.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonSortIgnoreCharacters = Button(self.containerIgnoreCharactersOptionsRow,
                                                 command=self.buttonSortIgnoreCharactersClick)
        self.buttonSortIgnoreCharacters.config(font=(self.fontType, self.fontSize))
        self.buttonSortIgnoreCharacters["text"] = "Sort Characters Alphabetically"
        self.buttonSortIgnoreCharacters["background"] = buttonColour
        ##        self.buttonSortIgnoreCharacters.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonRemoveIgnoreCharacters = Button(self.containerIgnoreCharactersOptionsRow,
                                                   command=self.buttonRemoveIgnoreCharactersClick)
        self.buttonRemoveIgnoreCharacters.config(font=(self.fontType, self.fontSize))
        self.buttonRemoveIgnoreCharacters["text"] = "Remove Deselected Characters"
        self.buttonRemoveIgnoreCharacters["background"] = buttonColour
        ##        self.buttonRemoveIgnoreCharacters.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)

        self.buttonAddIgnoreCharacters = Button(self.containerIgnoreCharactersAddRow,
                                                command=self.buttonAddIgnoreCharactersClick)
        self.buttonAddIgnoreCharacters.config(font=(self.fontType, self.fontSize))
        if self.numberOfColumnsIgnore < 2:
            self.buttonAddIgnoreCharacters["text"] = "Add Character"
        else:
            self.buttonAddIgnoreCharacters["text"] = "Add Row Of Characters"
        self.buttonAddIgnoreCharacters["background"] = buttonColour
        self.buttonAddIgnoreCharacters.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )
        self.buttonCloseIgnoreCharacters = Button(self.containerIgnoreCharacters1,
                                                  command=self.buttonCloseIgnoreCharactersClick)
        self.buttonCloseIgnoreCharacters["background"] = buttonColour
        self.buttonCloseIgnoreCharacters.configure(image=self.iconRedX,
                                                   width=button_width,
                                                   padx=button_padx,
                                                   pady=button_pady
                                                   )
        self.labelCloseIgnoreCharacters = Label(self.containerIgnoreCharacters1, text=self.closeText)
        self.labelCloseIgnoreCharacters.config(font=(self.fontType, self.fontSize))
        self.labelCloseIgnoreCharacters["background"] = buttonColour

        self.containerReplaceWords = Frame(myParent)
        self.containerReplaceWords1 = Frame(self.containerReplaceWords)
        self.containerReplaceWordsContractionsRow = Frame(self.containerReplaceWords)
        self.containerReplaceWordsOptionsRow = Frame(self.containerReplaceWords)
        self.containerReplaceWordsLabelRow = Frame(self.containerReplaceWords)
        self.containerReplaceWords2 = VerticalScrolledFrame(self.containerReplaceWords)
        self.containerReplaceWordsRows = []
        self.containerReplaceWordsCheckBoxList = []
        self.containerReplaceWordsTextBoxList1 = []
        self.containerReplaceWordsTextBoxList2 = []
        self.containerReplaceWordsAddRow = Frame(self.containerReplaceWords)

        self.labelReplaceWords = Label(self.containerReplaceWordsLabelRow,
                                       text="List of words to replace in text, and their replacements:")
        self.labelReplaceWords.config(font=(self.fontType, self.fontSize))

        self.buttonSelectReplaceWords = Checkbutton(self.containerReplaceWordsOptionsRow,
                                                    command=self.buttonSelectReplaceWordsClick)
        self.buttonSelectReplaceWords["variable"] = self.varSelect_Replace_Words
        self.buttonSelectReplaceWords.config(font=(self.fontType, self.fontSize))
        self.buttonSelectReplaceWords["text"] = "Deselect All Words"
        ##        self.buttonSelectReplaceWords.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonSortReplaceWords = Button(self.containerReplaceWordsOptionsRow,
                                             command=self.buttonSortReplaceWordsClick)
        self.buttonSortReplaceWords.config(font=(self.fontType, self.fontSize))
        self.buttonSortReplaceWords["text"] = "Sort Words Alphabetically"
        self.buttonSortReplaceWords["background"] = buttonColour
        ##        self.buttonSortReplaceWords.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonRemoveReplaceWords = Button(self.containerReplaceWordsOptionsRow,
                                               command=self.buttonRemoveReplaceWordsClick)
        self.buttonRemoveReplaceWords.config(font=(self.fontType, self.fontSize))
        self.buttonRemoveReplaceWords["text"] = "Remove Deselected Words"
        self.buttonRemoveReplaceWords["background"] = buttonColour
        ##        self.buttonRemoveReplaceWords.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)

        self.buttonContractionsReplace = Checkbutton(self.containerReplaceWordsContractionsRow)
        self.buttonContractionsReplace["variable"] = self.varContractions_replace
        self.buttonContractionsReplace.config(font=(self.fontType, self.fontSize))
        self.buttonContractionsReplace["text"] = "Separate Contractions (words with apostrophes)"
        self.buttonContractionsReplace["background"] = buttonColour

        self.buttonNumbersReplace = Checkbutton(self.containerReplaceWordsContractionsRow)
        self.buttonNumbersReplace["variable"] = self.varNumbers_replace
        self.buttonNumbersReplace.config(font=(self.fontType, self.fontSize))
        self.buttonNumbersReplace["text"] = "Replace Numbers"
        self.buttonNumbersReplace["background"] = buttonColour

        self.buttonOrdinalsReplace = Checkbutton(self.containerReplaceWordsContractionsRow)
        self.buttonOrdinalsReplace["variable"] = self.varOrdinals_replace
        self.buttonOrdinalsReplace.config(font=(self.fontType, self.fontSize))
        self.buttonOrdinalsReplace["text"] = "Replace Ordinals"
        self.buttonOrdinalsReplace["background"] = buttonColour

        self.buttonAddReplaceWords = Button(self.containerReplaceWordsAddRow, command=self.buttonAddReplaceWordsClick)
        self.buttonAddReplaceWords.config(font=(self.fontType, self.fontSize))
        if self.numberOfColumnsReplace < 2:
            self.buttonAddReplaceWords["text"] = "Add Word"
        else:
            self.buttonAddReplaceWords["text"] = "Add Row of Words"
        self.buttonAddReplaceWords["background"] = buttonColour
        self.buttonAddReplaceWords.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )
        self.buttonCloseReplaceWords = Button(self.containerReplaceWords1, command=self.buttonCloseReplaceWordsClick)
        self.buttonCloseReplaceWords["background"] = buttonColour
        self.buttonCloseReplaceWords.configure(image=self.iconRedX,
                                               width=button_width,
                                               padx=button_padx,
                                               pady=button_pady
                                               )
        self.labelCloseReplaceWords = Label(self.containerReplaceWords1, text=self.closeText)
        self.labelCloseReplaceWords.config(font=(self.fontType, self.fontSize))
        self.labelCloseReplaceWords["background"] = buttonColour

        self.containerReplaceCharacters = Frame(myParent)
        self.containerReplaceCharacters1 = Frame(self.containerReplaceCharacters)
        self.containerReplaceCharactersOptionsRow = Frame(self.containerReplaceCharacters)
        self.containerReplaceCharactersLabelRow = Frame(self.containerReplaceCharacters)
        self.containerReplaceCharacters2 = VerticalScrolledFrame(self.containerReplaceCharacters)
        self.containerReplaceCharactersRows = []
        self.containerReplaceCharactersCheckBoxList = []
        self.containerReplaceCharactersTextBoxList1 = []
        self.containerReplaceCharactersTextBoxList2 = []
        self.containerReplaceCharactersAddRow = Frame(self.containerReplaceCharacters)

        self.labelReplaceCharacters = Label(self.containerReplaceCharactersLabelRow,
                                            text="List of characters to replace in text, and their replacements:")
        self.labelReplaceCharacters.config(font=(self.fontType, self.fontSize))

        self.buttonSelectReplaceCharacters = Checkbutton(self.containerReplaceCharactersOptionsRow,
                                                         command=self.buttonSelectReplaceCharactersClick)
        self.buttonSelectReplaceCharacters["variable"] = self.varSelect_Replace_Chars
        self.buttonSelectReplaceCharacters.config(font=(self.fontType, self.fontSize))
        self.buttonSelectReplaceCharacters["text"] = "Deselect All Characters"
        ##        self.buttonSelectReplaceCharacters.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonSortReplaceCharacters = Button(self.containerReplaceCharactersOptionsRow,
                                                  command=self.buttonSortReplaceCharactersClick)
        self.buttonSortReplaceCharacters.config(font=(self.fontType, self.fontSize))
        self.buttonSortReplaceCharacters["text"] = "Sort Characters Alphabetically"
        self.buttonSortReplaceCharacters["background"] = buttonColour
        ##        self.buttonSortReplaceCharacters.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)
        self.buttonRemoveReplaceCharacters = Button(self.containerReplaceCharactersOptionsRow,
                                                    command=self.buttonRemoveReplaceCharactersClick)
        self.buttonRemoveReplaceCharacters.config(font=(self.fontType, self.fontSize))
        self.buttonRemoveReplaceCharacters["text"] = "Remove Deselected Characters"
        self.buttonRemoveReplaceCharacters["background"] = buttonColour
        ##        self.buttonRemoveReplaceCharacters.configure(
        ##			width=button_width,
        ##			padx=button_padx,
        ##			pady=button_pady
        ##			)

        self.buttonAddReplaceCharacters = Button(self.containerReplaceCharactersAddRow,
                                                 command=self.buttonAddReplaceCharactersClick)
        self.buttonAddReplaceCharacters.config(font=(self.fontType, self.fontSize))
        if self.numberOfColumnsIgnore < 2:
            self.buttonAddReplaceCharacters["text"] = "Add Character"
        else:
            self.buttonAddReplaceCharacters["text"] = "Add Row Of Characters"
        self.buttonAddReplaceCharacters["background"] = buttonColour
        self.buttonAddReplaceCharacters.configure(
            width=button_width,
            padx=button_padx,
            pady=button_pady
        )
        self.buttonCloseReplaceCharacters = Button(self.containerReplaceCharacters1,
                                                   command=self.buttonCloseReplaceCharactersClick)
        self.buttonCloseReplaceCharacters["background"] = buttonColour
        self.buttonCloseReplaceCharacters.configure(image=self.iconRedX,
                                                    width=button_width,
                                                    padx=button_padx,
                                                    pady=button_pady
                                                    )
        self.labelCloseReplaceCharacters = Label(self.containerReplaceCharacters1, text=self.closeText)
        self.labelCloseReplaceCharacters.config(font=(self.fontType, self.fontSize))
        self.labelCloseReplaceCharacters["background"] = buttonColour
        self.varSigmoid_connections.set(1)
        self.varLinear_function.set(0)

        self.varSelect_Ignored_Words.set(True);
        self.varSelect_Ignored_Chars.set(True);
        self.varSelect_Replace_Words.set(True);
        self.varSelect_Replace_Chars.set(True);

        self.updateGUIFromParam(params)

        self.varLsaChoiceStr.trace("w", self.checkLSAChoice)
        self.varWorking_mem_capStr.trace("w", self.checkWorking_mem_cap)
        self.varMStr.trace("w", self.checkM)
        self.varLearning_rateStr.trace("w", self.checkLearning_rate)
        self.varCohortStr.trace("w", self.checkCohort)
        self.varDecayStr.trace("w", self.checkDecay)
        self.varActivation_thresholdStr.trace("w", self.checkActivation_threshold)
        self.varConnection_thresholdStr.trace("w", self.checkConnection_threshold)
        self.varSemantic_weightStr.trace("w", self.checkSemantic_weight)
        self.varEpisodic_weightStr.trace("w", self.checkEpisodic_weight)
        self.varDecimal_roundStr.trace("w", self.checkDecimal_round)
        self.varSigmoid_minStr.trace("w", self.checkSigmoid_min)
        self.varSigmoid_maxStr.trace("w", self.checkSigmoid_max)
        self.varIteration_limitStr.trace("w", self.checkIteration_limit)
        self.entryMatrixLoadPath.config(state="disabled")
        self.entryConfigLoadPath.config(state="disabled")
        self.entryConfigSavePath.config(state="disabled")
        self.entryOutputPath.config(state="disabled")
        self.entryInputPath.config(state="disabled")
        self.packFrameMain()

    def handleParamErrorsExternal(self, params):
        if params["error_flag"] != 0:
            messagebox.showerror("Error", params["error_text"])
        params["error_flag"] = 0
        params["error_text"] = ''
        params["summary_text"] = ''
        return params

    def handleParamErrors(self):
        if self.parametersDict["error_flag"] != 0:
            messagebox.showerror("Error", self.parametersDict["error_text"])
        self.parametersDict["error_flag"] = 0
        self.parametersDict["error_text"] = ''
        self.parametersDict["summary_text"] = ''

    def handleParamSummary(self):
        if self.parametersDict["error_flag"] != 0:
            messagebox.showerror("Error", self.parametersDict["error_text"])
        else:
            messagebox.showinfo("Summary", self.parametersDict["summary_text"])
        self.parametersDict["error_flag"] = 0
        self.parametersDict["error_text"] = ''
        self.parametersDict["summary_text"] = ''

    def handleParameterSaveError(self, path):
        messagebox.showerror("Error", "File could not be saved at " + str(path))

    def buttonStartClick(self):
        self.updateParamFromGUIMain()
        # errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        # if errorSaveFlag:
        #     self.handleParameterSaveError("Default_Parameters1.txt")
        # else:
        self.parametersDict = lsaM.read_text(self.parametersDict)
        self.handleParamSummary()

    def buttonOutputColumnsShowClick(self):
        self.updateParamFromGUIMain()
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerMain.pack_forget()
        self.packFrameOutputOptions()

    def buttonCreateDefaultConfigurationFileClick(self):
        # self.buttonSaveParametersClick() #new
        self.saveParametersToFile()
        self.updateParamFromGUIMain()
        self.defaultParametersDict = copy.deepcopy(self.parametersDict) #new

        # errorSaveFlag = lsaP.saveDefaultConfigurationFile()
        # if errorSaveFlag:
        #     self.handleParameterSaveError(params['default_config_path'])

    def buttonResetConfigurationClick(self):
        self.updateParamFromGUIMain()
        # params = lsaP.resetParams()
        params = self.defaultParametersDict
        params['input_path'] = self.parametersDict['input_path']
        params['output_path'] = self.parametersDict['output_path']
        params['same_folder'] = self.parametersDict['same_folder']
        params = self.handleParamErrorsExternal(params)
        self.updateGUIFromParam(params)
        self.entryInputPath.xview_moveto(1)
        self.entryOutputPath.xview_moveto(1)
        self.entryConfigSavePath.xview_moveto(1)
        self.entryConfigLoadPath.xview_moveto(1)

    def buttonParametersShowClick(self):
        self.updateParamFromGUIMain()
        if (self.first_entry == True):
            errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
            if errorSaveFlag:
                self.handleParameterSaveError("Default_Parameters1.txt")
            self.defaultParametersDict = copy.deepcopy(self.parametersDict)
            self.first_entry = False
        self.defaultParametersDict = copy.deepcopy(self.defaultParametersDict)
        self.parametersDict['connection_threshold'] = 0.5
        self.containerMain.pack_forget()
        self.packFrameParameters()


    def buttonIgnoreWordsShowClick(self):
        self.updateParamFromGUIMain()
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerMain.pack_forget()
        self.packFrameIgnoreWords()

    def buttonIgnoreCharactersShowClick(self):
        self.updateParamFromGUIMain();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerMain.pack_forget()
        self.packFrameIgnoreCharacters()

    def buttonReplaceWordsShowClick(self):
        self.updateParamFromGUIMain();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerMain.pack_forget()
        self.packFrameReplaceWords()

    def buttonReplaceCharactersShowClick(self):
        self.updateParamFromGUIMain();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerMain.pack_forget()
        self.packFrameReplaceCharacters()

    def buttonShowAdvancedClick(self):
        self.containerParametersNormal.pack_forget()
        self.packFrameAdvancedParameters()

    def buttonShowNormalClick(self):
        self.containerParametersAdvanced.pack_forget()
        self.packFrameNormalParameters()

    def buttonMatrixLoadClick(self):
        if self.varLoaded_connection_matrix.get():
            self.buttonMatrixLoadPath.config(state="normal")
        else:
            self.buttonMatrixLoadPath.config(state="disabled")

    # def buttonMatrixLSAClick(self):
    #     if self.varInitial_connection_matrix.get():
    #         self.entryLsaChoice.config(state="normal")
    #     else:
    #         self.entryLsaChoice.config(state="disabled")

    def buttonIs_mem_capClick(self):
        if self.varIs_mem_cap.get():
            self.entryWorking_mem_cap.config(state="normal")
            self.R1.config(state="normal")
            self.R2.config(state="normal")
            self.R3.config(state="normal")
        else:
            self.entryWorking_mem_cap.config(state="disabled")
            self.R1.config(state="disabled")
            self.R2.config(state="disabled")
            self.R3.config(state="disabled")
    # def button_Is_keep_reading_max_Click(self):
        # disable keep cap max
        # self.keepB.config(state= "disabled")


        # #normal keep cap max
        # self.buttonIs_keep_cap_max.config(state= "normal")

    #
    # def button_Is_keep_cap_max_Click(self):
    #     if self.varIs_keep_cap_max.get():
    #         # self.buttonIs_keep_reading_max.config(state="disabled")
    #         x = 5
    #     else:
    #         # self.buttonIs_keep_reading_max.config(state= "normal")
    #         x = 5


    def buttonLinear_functionClick(self):
        self.varSigmoid_connections.set(int(1 - self.varLinear_function.get()))
        if self.varSigmoid_connections.get() == 1:
            self.entrySigmoid_min.config(state="normal")
            self.entrySigmoid_max.config(state="normal")
        else:
            self.entrySigmoid_min.config(state="disabled")
            self.entrySigmoid_max.config(state="disabled")

    def buttonSigmoid_connectionsClick(self):
        self.varLinear_function.set(int(1 - self.varSigmoid_connections.get()))
        if self.varSigmoid_connections.get() == 1:
            self.entrySigmoid_min.config(state="normal")
            self.entrySigmoid_max.config(state="normal")
        else:
            self.entrySigmoid_min.config(state="disabled")
            self.entrySigmoid_max.config(state="disabled")

    def buttonLimit_cyclesClick(self):
        if self.varLimit_cycles.get():
            self.entryIteration_limit.config(state="normal")
        else:
            self.entryIteration_limit.config(state="disabled")

    def buttonSameFolderClick(self):
        if self.varSaveInSamePath.get():
            self.buttonEntryOutputPath.config(state="disabled")
        else:
            self.buttonEntryOutputPath.config(state="normal")

            ##    def buttonShowAllColumnsClick(self):
            ##        if self.varShow_All_Columns.get():
            ##            for i in range(len(self.categories)):
            ##                self.buttonShowColumns[i].config(state="disabled")
            ##        else:
            ##            for i in range(len(self.categories)):
            ##                self.buttonShowColumns[i].config(state="normal")

    def buttonSaveParametersClick(self):
        self.updateParamFromGUIParameters();
        # errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        # if errorSaveFlag:
        #     self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerParametersNormal.pack_forget()
        self.containerParametersAdvanced.pack_forget()
        self.containerParameters.pack_forget()
        self.packFrameMain()

    def saveParametersToFile(self):
        self.updateParamFromGUIParameters();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerParametersNormal.pack_forget()
        self.containerParametersAdvanced.pack_forget()
        self.containerParameters.pack_forget()
        self.packFrameMain()

    def buttonCloseColumnsClick(self):
        self.updateParamFromGUIOutputColumns();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerOutputColumns.pack_forget()
        self.packFrameMain()

    def buttonCloseIgnoreWordsClick(self):
        self.updateParamFromGUIIgnoredWords();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerIgnoreWords.pack_forget()
        self.packFrameMain()

    def buttonCloseIgnoreCharactersClick(self):
        self.updateParamFromGUIIgnoredChars();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerIgnoreCharacters.pack_forget()
        self.packFrameMain()

    def buttonCloseReplaceWordsClick(self):
        self.updateParamFromGUIReplacedWords();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerReplaceWords.pack_forget()
        self.packFrameMain()

    def buttonCloseReplaceCharactersClick(self):
        self.updateParamFromGUIReplaceCharacters();
        errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
        if errorSaveFlag:
            self.handleParameterSaveError("Default_Parameters1.txt")
        self.containerReplaceCharacters.pack_forget()
        self.packFrameMain()

    def buttonSelectIgnoreWordsClick(self):
        if self.varSelect_Ignored_Words.get():
            self.buttonSelectIgnoreWords["text"] = "Deselect all words"
            i = 0
            while i < len(self.varIgnored_Words_Check):
                self.varIgnored_Words_Check[i].set(True)
                i = i + 1
        else:
            self.buttonSelectIgnoreWords["text"] = "  Select all words"
            i = 0
            while i < len(self.varIgnored_Words_Check):
                self.varIgnored_Words_Check[i].set(False)
                i = i + 1
        self.updateParamFromGUIIgnoredWords();

    def buttonSortIgnoreWordsClick(self):
        orderNew = sortListOfWords(self.varIgnored_Words_Boxes)
        new_1_Boxes = []
        new_Check = []
        for i in orderNew:
            new_1_Boxes.append(self.varIgnored_Words_Boxes[i].get())
            new_Check.append(self.varIgnored_Words_Check[i].get())
        for i in range(len(orderNew)):
            self.varIgnored_Words_Boxes[i].set(new_1_Boxes[i])
            self.varIgnored_Words_Check[i].set(new_Check[i])
        self.updateParamFromGUIIgnoredWords();

    def buttonRemoveIgnoreWordsClick(self):
        i = 0
        while i < len(self.varIgnored_Words_Check):
            if not self.varIgnored_Words_Check[i].get():
                self.varIgnored_Words_Boxes[i].set("")
            i = i + 1
        self.updateParamFromGUIIgnoredWords();

    def buttonSelectIgnoreCharactersClick(self):
        if self.varSelect_Ignored_Chars.get():
            self.buttonSelectIgnoreCharacters["text"] = "Deselect all characters"
            i = 0
            while i < len(self.varIgnored_Chars_Check):
                self.varIgnored_Chars_Check[i].set(True)
                i = i + 1
        else:
            self.buttonSelectIgnoreCharacters["text"] = "  Select all characters"
            i = 0
            while i < len(self.varIgnored_Chars_Check):
                self.varIgnored_Chars_Check[i].set(False)
                i = i + 1
        self.updateParamFromGUIIgnoredChars();

    def buttonSortIgnoreCharactersClick(self):
        orderNew = sortListOfWords(self.varIgnored_Chars_Boxes)
        new_1_Boxes = []
        new_Check = []
        for i in orderNew:
            new_1_Boxes.append(self.varIgnored_Chars_Boxes[i].get())
            new_Check.append(self.varIgnored_Chars_Check[i].get())
        for i in range(len(orderNew)):
            self.varIgnored_Chars_Boxes[i].set(new_1_Boxes[i])
            self.varIgnored_Chars_Check[i].set(new_Check[i])
        self.updateParamFromGUIIgnoredChars();

    def buttonRemoveIgnoreCharactersClick(self):
        i = 0
        while i < len(self.varIgnored_Chars_Check):
            if not self.varIgnored_Chars_Check[i].get():
                self.varIgnored_Chars_Boxes[i].set("")
            i = i + 1
        self.updateParamFromGUIIgnoredChars();

    def buttonSelectReplaceWordsClick(self):
        if self.varSelect_Replace_Words.get():
            self.buttonSelectReplaceWords["text"] = "Deselect all words"
            i = 0
            while i < len(self.varReplace_Words_Check):
                self.varReplace_Words_Check[i].set(True)
                i = i + 1
        else:
            self.buttonSelectReplaceWords["text"] = "  Select all words"
            i = 0
            while i < len(self.varReplace_Words_Check):
                self.varReplace_Words_Check[i].set(False)
                i = i + 1
        self.updateParamFromGUIReplacedWords();

    def buttonSortReplaceWordsClick(self):
        orderNew = sortListOfWords(self.varReplace_Words_1_Boxes, self.varReplace_Words_2_Boxes)
        new_1_Boxes = []
        new_2_Boxes = []
        new_Check = []
        for i in orderNew:
            new_1_Boxes.append(self.varReplace_Words_1_Boxes[i].get())
            new_2_Boxes.append(self.varReplace_Words_2_Boxes[i].get())
            new_Check.append(self.varReplace_Words_Check[i].get())
        for i in range(len(orderNew)):
            self.varReplace_Words_1_Boxes[i].set(new_1_Boxes[i])
            self.varReplace_Words_2_Boxes[i].set(new_2_Boxes[i])
            self.varReplace_Words_Check[i].set(new_Check[i])
        self.updateParamFromGUIReplacedWords();

    def buttonRemoveReplaceWordsClick(self):
        i = 0
        while i < len(self.varReplace_Words_Check):
            if not self.varReplace_Words_Check[i].get():
                self.varReplace_Words_1_Boxes[i].set("")
                self.varReplace_Words_2_Boxes[i].set("")
            i = i + 1
        self.updateParamFromGUIReplacedWords();

    def buttonSelectReplaceCharactersClick(self):
        if self.varSelect_Replace_Chars.get():
            self.buttonSelectReplaceCharacters["text"] = "Deselect all characters"
            i = 0
            while i < len(self.varReplace_Chars_Check):
                self.varReplace_Chars_Check[i].set(True)
                i = i + 1
        else:
            self.buttonSelectReplaceCharacters["text"] = "  Select all characters"
            i = 0
            while i < len(self.varReplace_Chars_Check):
                self.varReplace_Chars_Check[i].set(False)
                i = i + 1
        self.updateParamFromGUIReplaceCharacters();

    def buttonSortReplaceCharactersClick(self):
        orderNew = sortListOfWords(self.varReplace_Chars_1_Boxes, self.varReplace_Chars_2_Boxes)
        new_1_Boxes = []
        new_2_Boxes = []
        new_Check = []
        for i in orderNew:
            new_1_Boxes.append(self.varReplace_Chars_1_Boxes[i].get())
            new_2_Boxes.append(self.varReplace_Chars_2_Boxes[i].get())
            new_Check.append(self.varReplace_Chars_Check[i].get())
        for i in range(len(orderNew)):
            self.varReplace_Chars_1_Boxes[i].set(new_1_Boxes[i])
            self.varReplace_Chars_2_Boxes[i].set(new_2_Boxes[i])
            self.varReplace_Chars_Check[i].set(new_Check[i])
        self.updateParamFromGUIReplaceCharacters();

    def buttonRemoveReplaceCharactersClick(self):
        i = 0
        while i < len(self.varReplace_Chars_Check):
            if not self.varReplace_Chars_Check[i].get():
                self.varReplace_Chars_1_Boxes[i].set("")
                self.varReplace_Chars_2_Boxes[i].set("")
            i = i + 1
        self.updateParamFromGUIReplaceCharacters();

    def buttonMatrixLoadPathClick(self):
        newPath = filedialog.askopenfilename(defaultextension=".xlsx", title="Select connections matrix")
        if newPath != None:
            if len(newPath) > 0:
                self.varLoad_Connections_Path.set(newPath)
                self.entryMatrixLoadPath.xview_moveto(1)

    def buttonEntryInputPathClick(self):
        newPath = filedialog.askopenfilename(defaultextension=".docx", title="Select input file")
        if newPath != None:
            if len(newPath) > 0:
                self.varInput_Path.set(newPath)
                self.entryInputPath.xview_moveto(1)

    def buttonEntryOutputPathClick(self):
        newPath = filedialog.askdirectory(title="Select output directory")
        if newPath != None:
            if len(newPath) > 0:
                self.varOutput_Path.set(newPath)
                self.entryOutputPath.xview_moveto(1)

    def buttonConfigSavePathClick(self):
        newPath = filedialog.asksaveasfilename(defaultextension=".txt", title="Save parameters file as...")
        if newPath != None:
            if len(newPath) > 0:
                self.varSave_Configuration_Path.set(newPath)
                self.entryConfigSavePath.xview_moveto(1)
                self.updateParamFromGUIMain();
                errorSaveFlag = lsaP.saveNewConfigurationFile(self.parametersDict)
                if errorSaveFlag:
                    self.handleParameterSaveError(params['save_config_path'])
                errorSaveFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", self.parametersDict)
                if errorSaveFlag:
                    self.handleParameterSaveError("Default_Parameters1.txt")

    def buttonConfigLoadPathClick(self):
        newPath = filedialog.askopenfilename(defaultextension=".txt", title="Load parameters file")
        if newPath != None:
            if len(newPath) > 0:
                params = lsaP.configure(newPath);
                params = self.handleParamErrorsExternal(params)
                params['load_config_path'] = newPath
                check = self.varInput_Path.get()
                params['input_path'] = self.varInput_Path.get()
                params['output_path'] = self.varOutput_Path.get()
                params['same_folder'] = self.varSaveInSamePath.get()
                self.updateGUIFromParam(params);
                self.entryInputPath.xview_moveto(1)
                self.entryOutputPath.xview_moveto(1)
                self.entryConfigSavePath.xview_moveto(1)
                self.entryConfigLoadPath.xview_moveto(1)

    def changeDecimal_round(self):
        if RepresentsInt(self.varDecimal_roundStr.get()):
            self.varDecimal_round.set(int(self.varDecimal_roundStr.get()));
        else:
            self.varDecimal_roundStr.set(str(self.varDecimal_round.get()));

    def changeLsaChoice(self):
        self.varLsaChoice.set(self.lsaDict[self.varLsaChoiceStr.get()]);

    def changeWorking_mem_cap(self):
        if RepresentsFloat(self.varWorking_mem_capStr.get()):
            self.varWorking_mem_cap.set(float(self.varWorking_mem_capStr.get()));
        if self.varWorking_mem_cap.get() % 1 == 0:
            self.varWorking_mem_capStr.set(str(int(self.varWorking_mem_cap.get())));
        else:
            self.varWorking_mem_capStr.set(str(self.varWorking_mem_cap.get()));

    def changeM(self):
        if RepresentsFloat(self.varMStr.get()):
            self.varM.set(float(self.varMStr.get()));
        if self.varM.get() % 1 == 0:
            self.varMStr.set(str(int(self.varM.get())));
        else:
            self.varMStr.set(str(self.varM.get()));

    def changeLearning_rate(self):
        if RepresentsFloat(self.varLearning_rateStr.get()):
            self.varLearning_rate.set(float(self.varLearning_rateStr.get()));
        if self.varLearning_rate.get() % 1 == 0:
            self.varLearning_rateStr.set(str(int(self.varLearning_rate.get())));
        else:
            self.varLearning_rateStr.set(str(self.varLearning_rate.get()));

    def changeCohort(self):
        if RepresentsFloat(self.varCohortStr.get()):
            self.varCohort.set(float(self.varCohortStr.get()));
        if self.varCohort.get() % 1 == 0:
            self.varCohortStr.set(str(int(self.varCohort.get())));
        else:
            self.varCohortStr.set(str(self.varCohort.get()));

    def changeDecay(self):
        if RepresentsFloat(self.varDecayStr.get()):
            self.varDecay.set(float(self.varDecayStr.get()));
        if self.varDecay.get() % 1 == 0:
            self.varDecayStr.set(str(int(self.varDecay.get())));
        else:
            self.varDecayStr.set(str(self.varDecay.get()));

    def changeActivation_threshold(self):
        if RepresentsFloat(self.varActivation_thresholdStr.get()):
            self.varActivation_threshold.set(float(self.varActivation_thresholdStr.get()));
        if self.varActivation_threshold.get() % 1 == 0:
            self.varActivation_thresholdStr.set(str(int(self.varActivation_threshold.get())));
        else:
            self.varActivation_thresholdStr.set(str(self.varActivation_threshold.get()));

    def changeConnection_threshold(self):
        if RepresentsFloat(self.varConnection_thresholdStr.get()):
            self.varConnection_threshold.set(float(self.varConnection_thresholdStr.get()));
        if self.varConnection_threshold.get() % 1 == 0:
            self.varConnection_thresholdStr.set(str(int(self.varConnection_threshold.get())));
        else:
            self.varConnection_thresholdStr.set(str(self.varConnection_threshold.get()));

    def changeSemantic_weight(self):
        if RepresentsFloat(self.varSemantic_weightStr.get()):
            self.varSemantic_weight.set(float(self.varSemantic_weightStr.get()));
        if self.varSemantic_weight.get() % 1 == 0:
            self.varSemantic_weightStr.set(str(int(self.varSemantic_weight.get())));
        else:
            self.varSemantic_weightStr.set(str(self.varSemantic_weight.get()));

    def changeEpisodic_weight(self):
        if RepresentsFloat(self.varEpisodic_weightStr.get()):
            self.varEpisodic_weight.set(float(self.varEpisodic_weightStr.get()));
        if self.varEpisodic_weight.get() % 1 == 0:
            self.varEpisodic_weightStr.set(str(int(self.varEpisodic_weight.get())));
        else:
            self.varEpisodic_weightStr.set(str(self.varEpisodic_weight.get()));

    def changeSigmoid_min(self):
        if RepresentsFloat(self.varSigmoid_minStr.get()):
            self.varSigmoid_min.set(float(self.varSigmoid_minStr.get()));
        if self.varSigmoid_min.get() % 1 == 0:
            self.varSigmoid_minStr.set(str(int(self.varSigmoid_min.get())));
        else:
            self.varSigmoid_minStr.set(str(self.varSigmoid_min.get()));

    def changeSigmoid_max(self):
        if RepresentsFloat(self.varSigmoid_maxStr.get()):
            self.varSigmoid_max.set(float(self.varSigmoid_maxStr.get()));
        if self.varSigmoid_max.get() % 1 == 0:
            self.varSigmoid_maxStr.set(str(int(self.varSigmoid_max.get())));
        else:
            self.varSigmoid_maxStr.set(str(self.varSigmoid_max.get()));

    def changeIteration_limit(self):
        if RepresentsInt(self.varIteration_limitStr.get()):
            self.varIteration_limit.set(int(self.varIteration_limitStr.get()));
        else:
            self.varIteration_limitStr.set(str(self.varIteration_limit.get()));

    def checkLSAChoice(self, n, m, x):
        return

    def checkDecimal_round(self, n, m, x):
        if not NumberCheck(self.varDecimal_roundStr.get(), False):
            self.varDecimal_roundStr.set(str(self.varDecimal_round.get()));
            messagebox.showerror("Invalid Decimal Round", "Number of digits to round must be an integer.")

    def checkIteration_limit(self, n, m, x):
        if not NumberCheck(self.varIteration_limitStr.get(), False):
            self.varIteration_limitStr.set(str(self.varIteration_limit.get()));
            messagebox.showerror("Invalid Iteration Limit", "Iteration limit must be an integer.")

    def checkWorking_mem_cap(self, n, m, x):
        if not NumberCheck(self.varWorking_mem_capStr.get(), True):
            if self.varWorking_mem_cap.get() % 1 == 0:
                self.varWorking_mem_capStr.set(str(int(self.varWorking_mem_cap.get())));
            else:
                self.varWorking_mem_capStr.set(str(self.varWorking_mem_cap.get()));
            messagebox.showerror("Invalid Working Memory Cap", "Working memory cap must be a number.")

    def checkM(self, n, m, x):
        if not NumberCheck(self.varMStr.get(), True):
            if self.varM.get() % 1 == 0:
                self.varMStr.set(str(int(self.varM.get())));
            else:
                self.varMStr.set(str(self.varM.get()));
            messagebox.showerror("Invalid Mention", "Mention must be a number.")

    def checkLearning_rate(self, n, m, x):
        if not NumberCheck(self.varLearning_rateStr.get(), True):
            if self.varLearning_rate.get() % 1 == 0:
                self.varLearning_rateStr.set(str(int(self.varLearning_rate.get())));
            else:
                self.varLearning_rateStr.set(str(self.varLearning_rate.get()));
            messagebox.showerror("Invalid ILearning Rate", "Learning rate must be a number.")

    def checkCohort(self, n, m, x):
        if not NumberCheck(self.varCohortStr.get(), True):
            if self.varCohort.get() % 1 == 0:
                self.varCohortStr.set(str(int(self.varCohort.get())));
            else:
                self.varCohortStr.set(str(self.varCohort.get()));
            messagebox.showerror("Invalid Cohort", "Cohort must be a number.")

    def checkDecay(self, n, m, x):
        if not NumberCheck(self.varDecayStr.get(), True):
            if self.varDecay.get() % 1 == 0:
                self.varDecayStr.set(str(int(self.varDecay.get())));
            else:
                self.varDecayStr.set(str(self.varDecay.get()));
            messagebox.showerror("Invalid Decay", "Decay must be a number.")
        elif (self.varDecayStr.get() != ""):
            if(float(self.varDecayStr.get()) > 1):
                if self.varDecay.get() % 1 == 0:
                    self.varDecayStr.set(str(int(self.varDecay.get())));
                else:
                    self.varDecayStr.set(str(self.varDecay.get()));
                messagebox.showerror("Invalid Decay", "Activation Decay Rate cannot be larger than 1")

    def checkActivation_threshold(self, n, m, x):
        if not NumberCheck(self.varActivation_thresholdStr.get(), True):
            if self.varActivation_threshold.get() % 1 == 0:
                self.varActivation_thresholdStr.set(str(int(self.varActivation_threshold.get())));
            else:
                self.varActivation_thresholdStr.set(str(self.varActivation_threshold.get()));
            messagebox.showerror("Invalid Activation Threshold", "Activation threshold must be a number.")

    def checkConnection_threshold(self, n, m, x):
        if not NumberCheck(self.varConnection_thresholdStr.get(), True):
            if self.varConnection_threshold.get() % 1 == 0:
                self.varActivation_thresholdStr.set(str(int(self.varConnection_threshold.get())));
            else:
                self.varConnection_thresholdStr.set(str(self.varConnection_threshold.get()));
            messagebox.showerror("Invalid Connection Threshold", "Connection threshold must be a number.")

    def checkSemantic_weight(self, n, m, x):
        if not NumberCheck(self.varSemantic_weightStr.get(), True):
            if self.varSemantic_weight.get() % 1 == 0:
                self.varSemantic_weightStr.set(str(int(self.varSemantic_weight.get())));
            else:
                self.varSemantic_weightStr.set(str(self.varSemantic_weight.get()));
            messagebox.showerror("Invalid Semantic Connection Weight", "Semantic Connection Weight must be a number.")

    def checkEpisodic_weight(self, n, m, x):
        if not NumberCheck(self.varEpisodic_weightStr.get(), True):
            if self.varEpisodic_weight.get() % 1 == 0:
                self.varEpisodic_weightStr.set(str(int(self.varEpisodic_weight.get())));
            else:
                self.varEpisodic_weightStr.set(str(self.varEpisodic_weight.get()))
            messagebox.showerror("Invalid Episodic Connection Weight", "Episodic Connection Weight must be a number.")

    def checkSigmoid_min(self, n, m, x):
        if not NumberCheck(self.varSigmoid_minStr.get(), True):
            if self.varSigmoid_min.get() % 1 == 0:
                self.varSigmoid_minStr.set(str(int(self.varSigmoid_min.get())));
            else:
                self.varSigmoid_minStr.set(str(self.varSigmoid_min.get()));
            messagebox.showerror("Invalid Sigmoid Min", "Sigmoid min must be a number.")

    def checkSigmoid_max(self, n, m, x):
        if not NumberCheck(self.varSigmoid_maxStr.get(), True):
            if self.varSigmoid_max.get() % 1 == 0:
                self.varSigmoid_maxStr.set(str(int(self.varSigmoid_max.get())));
            else:
                self.varSigmoid_maxStr.set(str(self.varSigmoid_max.get()));
            messagebox.showerror("Invalid Sigmoid Max", "Sigmoid max must be a number.")

    def packFrameMain(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.containerMain.pack(
            ipadx=buttons_frame_ipadx,
            ipady=buttons_frame_ipady,
            padx=buttons_frame_padx,
            pady=buttons_frame_pady,
        )
        self.containerMain1.pack(side=TOP,
                                 ipadx=buttons_frame_ipadx,
                                 ipady=buttons_frame_ipady,
                                 padx=buttons_frame_padx,
                                 pady=buttons_frame_pady,
                                 )
        self.containerMain3.pack(side=TOP,
                                 ipadx=buttons_frame_ipadx,
                                 ipady=buttons_frame_ipady,
                                 padx=buttons_frame_padx,
                                 pady=buttons_frame_pady,
                                 )
        self.containerMain4.pack(side=TOP,
                                 ipadx=buttons_frame_ipadx,
                                 ipady=buttons_frame_ipady,
                                 padx=buttons_frame_padx,
                                 pady=buttons_frame_pady,
                                 )
        self.containerMain4.pack(side=TOP,
                                 ipadx=buttons_frame_ipadx,
                                 ipady=buttons_frame_ipady,
                                 padx=buttons_frame_padx,
                                 pady=buttons_frame_pady,
                                 )
        self.containerMain5.pack(side=TOP,
                                 ipadx=buttons_frame_ipadx,
                                 ipady=buttons_frame_ipady,
                                 padx=buttons_frame_padx,
                                 pady=buttons_frame_pady,
                                 )
        self.containerMain2.pack(side=BOTTOM,
                                 ipadx=buttons_frame_ipadx,
                                 ipady=buttons_frame_ipady,
                                 padx=buttons_frame_padx,
                                 pady=buttons_frame_pady,
                                 )


        self.labelStart.pack(side=TOP)
        self.notes.pack(side=BOTTOM)
        self.markers.pack(side=BOTTOM)

        self.buttonStart.pack(side=BOTTOM)
        self.buttonStart.focus_force()
        self.buttonParametersShow.pack(side=LEFT)
        self.buttonOutputColumnsShow.pack(side=LEFT)
        self.buttonIgnoreWordsShow.pack(side=LEFT)
        self.buttonIgnoreCharactersShow.pack(side=LEFT)
        self.buttonReplaceWordsShow.pack(side=LEFT)
        self.buttonReplaceCharactersShow.pack(side=LEFT)

        self.buttonEntryInputPath.pack(side=LEFT)
        self.entryInputPath.pack(side=LEFT)
        self.buttonSameFolder.pack(side=LEFT)
        self.buttonEntryOutputPath.pack(side=LEFT)
        self.entryOutputPath.pack(side=LEFT)
        #        self.labelOutputName.pack(side=LEFT)
        #        self.entryOutputName.pack(side=LEFT)
        #        self.buttonAppendDateTime.pack(side=LEFT)

        self.buttonConfigSavePath.pack(side=LEFT)
        self.entryConfigSavePath.pack(side=LEFT)
        self.buttonConfigLoadPath.pack(side=LEFT)
        self.entryConfigLoadPath.pack(side=LEFT)
        self.buttonResetConfiguration.pack(side=RIGHT)

        self.entryInputPath.xview_moveto(1)
        self.entryOutputPath.xview_moveto(1)
        self.entryConfigSavePath.xview_moveto(1)
        self.entryConfigLoadPath.xview_moveto(1)
        if self.varSaveInSamePath.get():
            self.buttonEntryOutputPath.config(state="disabled")
        else:
            self.buttonEntryOutputPath.config(state="normal")

    def packFrameParameters(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.containerParameters.pack(
            ipadx=buttons_frame_ipadx,
            ipady=buttons_frame_ipady,
            padx=buttons_frame_padx,
            pady=buttons_frame_pady,
        )

        self.containerParametersRowExit.pack(side=TOP,
                                             ipadx=buttons_frame_ipadx,
                                             ipady=buttons_frame_ipady,
                                             padx=buttons_frame_padx,
                                             pady=buttons_frame_pady,
                                             )
        self.buttonSaveParameters.pack(side=RIGHT)
        self.labelCloseParameters.pack(side=RIGHT)
        self.advancedShown = False;
        self.packFrameNormalParameters()

    def packFrameNormalParameters(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        # Normal parameters are packed here        
        self.containerParametersNormal.pack(side=TOP,
                                            ipadx=buttons_frame_ipadx,
                                            ipady=buttons_frame_ipady,
                                            padx=buttons_frame_padx,
                                            pady=buttons_frame_pady,
                                            )
        self.containerGradeLevel.pack(side=TOP,
                                      ipadx=buttons_frame_ipadx,
                                      ipady=buttons_frame_ipady,
                                      padx=buttons_frame_padx,
                                      pady=buttons_frame_pady,
                                      )
        self.containerParametersMemCaprow.pack(side=TOP,
                                               ipadx=buttons_frame_ipadx,
                                               ipady=buttons_frame_ipady,
                                               padx=buttons_frame_padx,
                                               pady=buttons_frame_pady,
                                               )
        # self.containerParametersOptionsMemCapRow.pack(side=TOP,
        #                                        ipadx=buttons_frame_ipadx,
        #                                        ipady=buttons_frame_ipady,
        #                                        padx=buttons_frame_padx,
        #                                        pady=buttons_frame_pady,
        #                                        )
        self.containerParametersMentionrow.pack(side=TOP,
                                                ipadx=buttons_frame_ipadx,
                                                ipady=buttons_frame_ipady,
                                                padx=buttons_frame_padx,
                                                pady=buttons_frame_pady,
                                                )
        self.containerParametersLearningRaterow.pack(side=TOP,
                                                     ipadx=buttons_frame_ipadx,
                                                     ipady=buttons_frame_ipady,
                                                     padx=buttons_frame_padx,
                                                     pady=buttons_frame_pady,
                                                     )
        self.containerParametersCohortrow.pack(side=TOP,
                                               ipadx=buttons_frame_ipadx,
                                               ipady=buttons_frame_ipady,
                                               padx=buttons_frame_padx,
                                               pady=buttons_frame_pady,
                                               )
        self.containerParametersDecayrow.pack(side=TOP,
                                              ipadx=buttons_frame_ipadx,
                                              ipady=buttons_frame_ipady,
                                              padx=buttons_frame_padx,
                                              pady=buttons_frame_pady,
                                              )
        self.containerParametersActiveThresholdrow.pack(side=TOP,
                                                        ipadx=buttons_frame_ipadx,
                                                        ipady=buttons_frame_ipady,
                                                        padx=buttons_frame_padx,
                                                        pady=buttons_frame_pady,
                                                        )

        self.containerParametersConnectionThresholdrow.pack(side=TOP,
                                                            ipadx=buttons_frame_ipadx,
                                                            ipady=buttons_frame_ipady,
                                                            padx=buttons_frame_padx,
                                                            pady=buttons_frame_pady,
                                                            )

        self.containerParametersSemanticWeightrow.pack(side=TOP,
                                                       ipadx=buttons_frame_ipadx,
                                                       ipady=buttons_frame_ipady,
                                                       padx=buttons_frame_padx,
                                                       pady=buttons_frame_pady,
                                                       )
        self.containerParametersEpisodicWeightrow.pack(side=TOP,
                                                       ipadx=buttons_frame_ipadx,
                                                       ipady=buttons_frame_ipady,
                                                       padx=buttons_frame_padx,
                                                       pady=buttons_frame_pady,
                                                       )
        # self.containerGradeLevel.pack(side=TOP,
        #                                                ipadx=buttons_frame_ipadx,
        #                                                ipady=buttons_frame_ipady,
        #                                                padx=buttons_frame_padx,
        #                                                pady=buttons_frame_pady,
        #                                                )
        self.containerParametersShowAdvancedrow.pack(side=BOTTOM,
                                                     ipadx=buttons_frame_ipadx,
                                                     ipady=buttons_frame_ipady,
                                                     padx=buttons_frame_padx,
                                                     pady=buttons_frame_pady,
                                                     )
        self.containerParametersSaveAsDefaultBasic.pack(side=TOP,
                                                     ipadx=buttons_frame_ipadx,
                                                     ipady=buttons_frame_ipady,
                                                     padx=buttons_frame_padx,
                                                     pady=buttons_frame_pady,
                                                     )

        self.buttonIs_mem_cap.pack(side=LEFT)
        # self.labelWorking_mem_cap.pack(side=LEFT)
        # self.keepA.pack(side= LEFT)
        # self.keepB.pack(side=RIGHT)
        self.R3.pack(side=LEFT)
        self.R1.pack(side=RIGHT)
        self.R2.pack(side=RIGHT)
        # self.R3.pack(side=LEFT)
        self.entryWorking_mem_cap.pack(side=LEFT)
        self.labelM.pack(side=LEFT)
        self.entryM.pack(side=LEFT)
        self.labelLearning_rate.pack(side=LEFT)
        self.entryLearning_rate.pack(side=LEFT)
        self.labelCohort.pack(side=LEFT)
        self.entryCohort.pack(side=LEFT)
        self.labelDecay.pack(side=LEFT)
        self.entryDecay.pack(side=LEFT)
        self.labelActivation_threshold.pack(side=LEFT)
        self.entryActivation_threshold.pack(side=LEFT)

        self.labelConnection_threshold.pack(side=LEFT)
        self.entryConnection_threshold.pack(side=LEFT)

        self.labelSemantic_weight.pack(side=LEFT)
        self.entrySemantic_weight.pack(side=LEFT)
        self.labelEpisodic_weight.pack(side=LEFT)
        self.entryEpisodic_weight.pack(side=LEFT)
        self.labelLsaChoice.pack(side=LEFT)
        self.entryLsaChoice.pack(side=LEFT)
        self.buttonShowAdvanced.pack(side=BOTTOM)
        self.buttonSaveAsDef.pack(side=BOTTOM)
        self.advancedShown = False

    def packFrameAdvancedParameters(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        # Advanced parameters are packed here
        self.containerParametersAdvanced.pack(side=TOP,
                                              ipadx=buttons_frame_ipadx,
                                              ipady=buttons_frame_ipady,
                                              padx=buttons_frame_padx,
                                              pady=buttons_frame_pady,
                                              )
        # self.containerParametersLSArow.pack(side=TOP,
        #                                     ipadx=buttons_frame_ipadx,
        #                                     ipady=buttons_frame_ipady,
        #                                     padx=buttons_frame_padx,
        #                                     pady=buttons_frame_pady,
        #                                     )
        self.containerParametersLoadMatrixrow.pack(side=TOP,
                                                   ipadx=buttons_frame_ipadx,
                                                   ipady=buttons_frame_ipady,
                                                   padx=buttons_frame_padx,
                                                   pady=buttons_frame_pady,
                                                   )
        self.containerParametersDecimalrow.pack(side=TOP,
                                                ipadx=buttons_frame_ipadx,
                                                ipady=buttons_frame_ipady,
                                                padx=buttons_frame_padx,
                                                pady=buttons_frame_pady,
                                                )
        self.containerParametersOptionsMemCapRow.pack(side=TOP,
                                                      ipadx=buttons_frame_ipadx,
                                                      ipady=buttons_frame_ipady,
                                                      padx=buttons_frame_padx,
                                                      pady=buttons_frame_pady,
                                                      )
        # self.containerParametersSelfActiverow.pack(side=TOP,
        #                                            ipadx=buttons_frame_ipadx,
        #                                            ipady=buttons_frame_ipady,
        #                                            padx=buttons_frame_padx,
        #                                            pady=buttons_frame_pady,
        #                                            )
        self.containerParametersForewardActiverow.pack(side=TOP,
                                                       ipadx=buttons_frame_ipadx,
                                                       ipady=buttons_frame_ipady,
                                                       padx=buttons_frame_padx,
                                                       pady=buttons_frame_pady,
                                                       )
        self.containerParametersNegativeConnectionsrow.pack(side=TOP,
                                                            ipadx=buttons_frame_ipadx,
                                                            ipady=buttons_frame_ipady,
                                                            padx=buttons_frame_padx,
                                                            pady=buttons_frame_pady,
                                                            )
        self.containerParametersSigmoidrow.pack(side=TOP,
                                                ipadx=buttons_frame_ipadx,
                                                ipady=buttons_frame_ipady,
                                                padx=buttons_frame_padx,
                                                pady=buttons_frame_pady,
                                                )
        self.containerParametersLimitCyclesrow.pack(side=TOP,
                                                    ipadx=buttons_frame_ipadx,
                                                    ipady=buttons_frame_ipady,
                                                    padx=buttons_frame_padx,
                                                    pady=buttons_frame_pady,
                                                    )
        self.containerParametersWordIsCyclerow.pack(side=TOP,
                                                    ipadx=buttons_frame_ipadx,
                                                    ipady=buttons_frame_ipady,
                                                    padx=buttons_frame_padx,
                                                    pady=buttons_frame_pady,
                                                    )
        self.containerParametersBlankCyclerow.pack(side=TOP,
                                                   ipadx=buttons_frame_ipadx,
                                                   ipady=buttons_frame_ipady,
                                                   padx=buttons_frame_padx,
                                                   pady=buttons_frame_pady,
                                                   )
        self.containerParametersReducedExpectancyrow.pack(side=TOP,
                                                          ipadx=buttons_frame_ipadx,
                                                          ipady=buttons_frame_ipady,
                                                          padx=buttons_frame_padx,
                                                          pady=buttons_frame_pady,
                                                          )
        self.containerParametersDefaultConfigrow.pack(side=TOP,
                                                      ipadx=buttons_frame_ipadx,
                                                      ipady=buttons_frame_ipady,
                                                      padx=buttons_frame_padx,
                                                      pady=buttons_frame_pady,
                                                      )
        self.containerParametersShowNormalrow.pack(side=BOTTOM,
                                                   ipadx=buttons_frame_ipadx,
                                                   ipady=buttons_frame_ipady,
                                                   padx=buttons_frame_padx,
                                                   pady=buttons_frame_pady,
                                                   )

        self.buttonSaveDefaultConfiguration.pack(side=LEFT)
        # self.buttonInitial_connection_matrix.pack(side=LEFT)
        # self.labelLsaChoice.pack(side=LEFT)
        # self.entryLsaChoice.pack(side=LEFT)
        self.buttonLoad_connection_matrix.pack(side=LEFT)
        self.buttonMatrixLoadPath.pack(side=LEFT)
        self.entryMatrixLoadPath.pack(side=LEFT)
        self.labelDecimal_round_part1.pack(side=LEFT)
        self.entryDecimal_round.pack(side=LEFT)
        self.labelDecimal_round_part2.pack(side=LEFT)
        # self.buttonSelf_activation.pack(side=LEFT)
        self.buttonForward_activation.pack(side=LEFT)
        self.buttonNegative_connections.pack(side=LEFT)
        self.buttonLinear_function.pack(side=LEFT)
        self.buttonSigmoid_connections.pack(side=LEFT)
        self.labelSigmoid_min.pack(side=LEFT)
        self.entrySigmoid_min.pack(side=LEFT)
        self.labelSigmoid_max.pack(side=LEFT)
        self.entrySigmoid_max.pack(side=LEFT)
        self.buttonLimit_cycles.pack(side=LEFT)
        self.labelIteration_limit.pack(side=LEFT)
        self.entryIteration_limit.pack(side=LEFT)
        self.buttonWord_is_cycle.pack(side=LEFT)
        self.buttonBlank_cycle.pack(side=LEFT)
        self.buttonReduced_expectancy.pack(side=LEFT)
        self.buttonSaveDefaultConfiguration.pack(side=LEFT)
        self.buttonShowNormal.pack(side=BOTTOM)

        self.varSigmoid_connections.set(1 - self.varLinear_function.get())
        if self.varSigmoid_connections.get() == 1:
            self.entrySigmoid_min.config(state="normal")
            self.entrySigmoid_max.config(state="normal")
        else:
            self.entrySigmoid_min.config(state="disabled")
            self.entrySigmoid_max.config(state="disabled")
        if self.varLimit_cycles.get():
            self.entryIteration_limit.config(state="normal")
        else:
            self.entryIteration_limit.config(state="disabled")

        if self.varLoaded_connection_matrix.get():
            self.buttonMatrixLoadPath.config(state="normal")
        else:
            self.buttonMatrixLoadPath.config(state="disabled")
        # if self.varInitial_connection_matrix.get():
        #     self.entryLsaChoice.config(state="normal")
        # else:
        #     self.entryLsaChoice.config(state="disabled")
        if self.varIs_mem_cap.get():
            self.entryWorking_mem_cap.config(state="normal")
            self.R1.config(state="normal")
            self.R2.config(state="normal")
        else:
            self.entryWorking_mem_cap.config(state="disabled")
            self.R1.config(state="disabled")
            self.R2.config(state="disabled")
        self.entryMatrixLoadPath.xview_moveto(1)
        self.advancedShown = True;

    def packFrameOutputOptions(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"
        self.containerOutputColumns.pack(
            ipadx=buttons_frame_ipadx,
            ipady=buttons_frame_ipady,
            padx=buttons_frame_padx,
            pady=buttons_frame_pady,
        )
        self.containerOutputColumns1.pack(side=TOP,
                                          ipadx=buttons_frame_ipadx,
                                          ipady=buttons_frame_ipady,
                                          padx=buttons_frame_padx,
                                          pady=buttons_frame_pady,
                                          )
        self.containerOutputColumns2.pack(side=TOP,
                                          ipadx=buttons_frame_ipadx,
                                          ipady=buttons_frame_ipady,
                                          padx=buttons_frame_padx,
                                          pady=buttons_frame_pady,
                                          )
        self.containerOutputColumns3.pack(side=BOTTOM,
                                          ipadx=buttons_frame_ipadx,
                                          ipady=buttons_frame_ipady,
                                          padx=buttons_frame_padx,
                                          pady=buttons_frame_pady,
                                          )

        ##        self.buttonShowAllColumns.pack(side=TOP)
        for i in range(len(self.categories)):
            self.buttonShowColumns[i].pack(side=TOP)
        self.buttonCloseColumns.pack(side=RIGHT)
        self.labelCloseColumns.pack(side=RIGHT)
        if self.varShow_All_Columns.get():
            for i in range(len(self.categories)):
                self.buttonShowColumns[i].config(state="disabled")
        else:
            for i in range(len(self.categories)):
                self.buttonShowColumns[i].config(state="normal")

    def packFrameIgnoreWords(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.containerIgnoreWords.pack(
            ipadx=buttons_frame_ipadx,
            ipady=buttons_frame_ipady,
            padx=buttons_frame_padx,
            pady=buttons_frame_pady,
        )
        self.containerIgnoreWords1.pack(side=TOP,
                                        ipadx=buttons_frame_ipadx,
                                        ipady=buttons_frame_ipady,
                                        padx=buttons_frame_padx,
                                        pady=buttons_frame_pady,
                                        )
        self.containerIgnoreWordsOptionsRow.pack(side=TOP,
                                                 ipadx=buttons_frame_ipadx,
                                                 ipady=buttons_frame_ipady,
                                                 padx=buttons_frame_padx,
                                                 pady=buttons_frame_pady,
                                                 )
        self.containerIgnoreWordsLabelRow.pack(side=TOP,
                                               ipadx=buttons_frame_ipadx,
                                               ipady=buttons_frame_ipady,
                                               padx=buttons_frame_padx,
                                               pady=buttons_frame_pady,
                                               )
        self.containerIgnoreWords2.pack(side=TOP,
                                        ipadx=buttons_frame_ipadx,
                                        ipady=buttons_frame_ipady,
                                        padx=buttons_frame_padx,
                                        pady=buttons_frame_pady,
                                        )

        self.buttonCloseIgnoreWords.pack(side=RIGHT)
        self.labelCloseIgnoreWords.pack(side=RIGHT)
        self.labelIgnoreWords.pack(side=TOP)
        self.buttonSortIgnoreWords.pack(side=LEFT)
        self.buttonSelectIgnoreWords.pack(side=LEFT)
        self.buttonRemoveIgnoreWords.pack(side=LEFT)

        for i in range(len(self.containerIgnoreWordsRows)):
            self.containerIgnoreWordsRows[i].pack(side=TOP,
                                                  ipadx=buttons_frame_ipadx,
                                                  ipady=buttons_frame_ipady,
                                                  padx=buttons_frame_padx,
                                                  pady=buttons_frame_pady,
                                                  )

        for i in range(self.varIgnored_Words_num):
            self.containerIgnoreWordsCheckBoxList[i].pack(side=LEFT)
            self.containerIgnoreWordsTextBoxList[i].pack(side=LEFT)

        self.containerIgnoreWordsAddRow.pack(side=BOTTOM,
                                             ipadx=buttons_frame_ipadx,
                                             ipady=buttons_frame_ipady,
                                             padx=buttons_frame_padx,
                                             pady=buttons_frame_pady,
                                             )
        self.buttonAddIgnoreWords.pack(side=BOTTOM)

    def packFrameIgnoreCharacters(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.containerIgnoreCharacters.pack(
            ipadx=buttons_frame_ipadx,
            ipady=buttons_frame_ipady,
            padx=buttons_frame_padx,
            pady=buttons_frame_pady,
        )
        self.containerIgnoreCharacters1.pack(side=TOP,
                                             ipadx=buttons_frame_ipadx,
                                             ipady=buttons_frame_ipady,
                                             padx=buttons_frame_padx,
                                             pady=buttons_frame_pady,
                                             )
        self.containerIgnoreCharactersOptionsRow.pack(side=TOP,
                                                      ipadx=buttons_frame_ipadx,
                                                      ipady=buttons_frame_ipady,
                                                      padx=buttons_frame_padx,
                                                      pady=buttons_frame_pady,
                                                      )
        self.containerIgnoreCharactersLabelRow.pack(side=TOP,
                                                    ipadx=buttons_frame_ipadx,
                                                    ipady=buttons_frame_ipady,
                                                    padx=buttons_frame_padx,
                                                    pady=buttons_frame_pady,
                                                    )
        self.containerIgnoreCharacters2.pack(side=TOP,
                                             ipadx=buttons_frame_ipadx,
                                             ipady=buttons_frame_ipady,
                                             padx=buttons_frame_padx,
                                             pady=buttons_frame_pady,
                                             )

        self.buttonCloseIgnoreCharacters.pack(side=RIGHT)
        self.labelCloseIgnoreCharacters.pack(side=RIGHT)
        self.labelIgnoreCharacters.pack(side=TOP)
        self.buttonSortIgnoreCharacters.pack(side=LEFT)
        self.buttonSelectIgnoreCharacters.pack(side=LEFT)
        self.buttonRemoveIgnoreCharacters.pack(side=LEFT)

        for i in range(len(self.containerIgnoreCharactersRows)):
            self.containerIgnoreCharactersRows[i].pack(side=TOP,
                                                       ipadx=buttons_frame_ipadx,
                                                       ipady=buttons_frame_ipady,
                                                       padx=buttons_frame_padx,
                                                       pady=buttons_frame_pady,
                                                       )

        for i in range(self.varIgnored_Chars_num):
            self.containerIgnoreCharactersCheckBoxList[i].pack(side=LEFT)
            self.containerIgnoreCharactersTextBoxList[i].pack(side=LEFT)

        self.containerIgnoreCharactersAddRow.pack(side=BOTTOM,
                                                  ipadx=buttons_frame_ipadx,
                                                  ipady=buttons_frame_ipady,
                                                  padx=buttons_frame_padx,
                                                  pady=buttons_frame_pady,
                                                  )
        self.buttonAddIgnoreCharacters.pack(side=BOTTOM)

    def packFrameReplaceWords(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.containerReplaceWords.pack(
            ipadx=buttons_frame_ipadx,
            ipady=buttons_frame_ipady,
            padx=buttons_frame_padx,
            pady=buttons_frame_pady,
        )
        self.containerReplaceWords1.pack(side=TOP,
                                         ipadx=buttons_frame_ipadx,
                                         ipady=buttons_frame_ipady,
                                         padx=buttons_frame_padx,
                                         pady=buttons_frame_pady,
                                         )
        self.containerReplaceWordsContractionsRow.pack(side=TOP,
                                                       ipadx=buttons_frame_ipadx,
                                                       ipady=buttons_frame_ipady,
                                                       padx=buttons_frame_padx,
                                                       pady=buttons_frame_pady,
                                                       )
        self.containerReplaceWordsOptionsRow.pack(side=TOP,
                                                  ipadx=buttons_frame_ipadx,
                                                  ipady=buttons_frame_ipady,
                                                  padx=buttons_frame_padx,
                                                  pady=buttons_frame_pady,
                                                  )
        self.containerReplaceWordsLabelRow.pack(side=TOP,
                                                ipadx=buttons_frame_ipadx,
                                                ipady=buttons_frame_ipady,
                                                padx=buttons_frame_padx,
                                                pady=buttons_frame_pady,
                                                )
        self.containerReplaceWords2.pack(side=TOP,
                                         ipadx=buttons_frame_ipadx,
                                         ipady=buttons_frame_ipady,
                                         padx=buttons_frame_padx,
                                         pady=buttons_frame_pady,
                                         )

        self.buttonCloseReplaceWords.pack(side=RIGHT)
        self.labelCloseReplaceWords.pack(side=RIGHT)
        self.buttonSortReplaceWords.pack(side=LEFT)
        self.buttonSelectReplaceWords.pack(side=LEFT)
        self.buttonRemoveReplaceWords.pack(side=LEFT)
        self.buttonContractionsReplace.pack(side=LEFT)
        self.buttonNumbersReplace.pack(side=LEFT)
        self.buttonOrdinalsReplace.pack(side=LEFT)
        self.labelReplaceWords.pack(side=TOP)

        for i in range(len(self.containerReplaceWordsRows)):
            self.containerReplaceWordsRows[i].pack(side=TOP,
                                                   ipadx=buttons_frame_ipadx,
                                                   ipady=buttons_frame_ipady,
                                                   padx=buttons_frame_padx,
                                                   pady=buttons_frame_pady,
                                                   )
        for i in range(self.varReplace_Words_num):
            self.containerReplaceWordsCheckBoxList[i].pack(side=LEFT)
            self.containerReplaceWordsTextBoxList1[i].pack(side=LEFT)
            self.containerReplaceWordsTextBoxList2[i].pack(side=LEFT)

        self.containerReplaceWordsAddRow.pack(side=BOTTOM,
                                              ipadx=buttons_frame_ipadx,
                                              ipady=buttons_frame_ipady,
                                              padx=buttons_frame_padx,
                                              pady=buttons_frame_pady,
                                              )
        self.buttonAddReplaceWords.pack(side=BOTTOM)

    def packFrameReplaceCharacters(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.containerReplaceCharacters.pack(
            ipadx=buttons_frame_ipadx,
            ipady=buttons_frame_ipady,
            padx=buttons_frame_padx,
            pady=buttons_frame_pady,
        )
        self.containerReplaceCharacters1.pack(side=TOP,
                                              ipadx=buttons_frame_ipadx,
                                              ipady=buttons_frame_ipady,
                                              padx=buttons_frame_padx,
                                              pady=buttons_frame_pady,
                                              )
        self.containerReplaceCharactersOptionsRow.pack(side=TOP,
                                                       ipadx=buttons_frame_ipadx,
                                                       ipady=buttons_frame_ipady,
                                                       padx=buttons_frame_padx,
                                                       pady=buttons_frame_pady,
                                                       )
        self.containerReplaceCharactersLabelRow.pack(side=TOP,
                                                     ipadx=buttons_frame_ipadx,
                                                     ipady=buttons_frame_ipady,
                                                     padx=buttons_frame_padx,
                                                     pady=buttons_frame_pady,
                                                     )
        self.containerReplaceCharacters2.pack(side=TOP,
                                              ipadx=buttons_frame_ipadx,
                                              ipady=buttons_frame_ipady,
                                              padx=buttons_frame_padx,
                                              pady=buttons_frame_pady,
                                              )

        self.buttonCloseReplaceCharacters.pack(side=RIGHT)
        self.labelCloseReplaceCharacters.pack(side=RIGHT)
        self.labelReplaceCharacters.pack(side=TOP)
        self.buttonSortReplaceCharacters.pack(side=LEFT)
        self.buttonSelectReplaceCharacters.pack(side=LEFT)
        self.buttonRemoveReplaceCharacters.pack(side=LEFT)

        for i in range(len(self.containerReplaceCharactersRows)):
            self.containerReplaceCharactersRows[i].pack(side=TOP,
                                                        ipadx=buttons_frame_ipadx,
                                                        ipady=buttons_frame_ipady,
                                                        padx=buttons_frame_padx,
                                                        pady=buttons_frame_pady,
                                                        )
        for i in range(self.varReplace_Chars_num):
            self.containerReplaceCharactersCheckBoxList[i].pack(side=LEFT)
            self.containerReplaceCharactersTextBoxList1[i].pack(side=LEFT)
            self.containerReplaceCharactersTextBoxList2[i].pack(side=LEFT)

        self.containerReplaceCharactersAddRow.pack(side=BOTTOM,
                                                   ipadx=buttons_frame_ipadx,
                                                   ipady=buttons_frame_ipady,
                                                   padx=buttons_frame_padx,
                                                   pady=buttons_frame_pady,
                                                   )
        self.buttonAddReplaceCharacters.pack(side=BOTTOM)

    def addIgnoreWordsClick(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        rowNum = len(self.containerIgnoreWordsRows)
        self.containerIgnoreWordsRows.append(Frame(self.containerIgnoreWords2.interior))
        j = 0
        i = self.varIgnored_Words_num
        while j < self.numberOfColumnsIgnore:
            self.varIgnored_Words_Boxes.append(StringVar())
            self.varIgnored_Words_Check.append(BooleanVar())
            self.varIgnored_Words_Boxes[i].set("")
            self.varIgnored_Words_Check[i].set(True)
            self.containerIgnoreWordsCheckBoxList.append(Checkbutton(self.containerIgnoreWordsRows[rowNum]))
            self.containerIgnoreWordsCheckBoxList[i]["variable"] = self.varIgnored_Words_Check[i]
            self.containerIgnoreWordsCheckBoxList[i]["background"] = buttonColour
            self.containerIgnoreWordsTextBoxList.append(
                Entry(self.containerIgnoreWordsRows[rowNum], textvariable=self.varIgnored_Words_Boxes[i],
                      validate="focusout"))
            self.containerIgnoreWordsTextBoxList[i].config(font=(self.fontType, self.fontSize))
            j = j + 1
            i = i + 1

        self.varIgnored_Words_num = i

    def addIgnoreCharactersClick(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        rowNum = len(self.containerIgnoreCharactersRows)
        self.containerIgnoreCharactersRows.append(Frame(self.containerIgnoreCharacters2.interior))
        j = 0
        i = self.varIgnored_Chars_num
        while j < self.numberOfColumnsIgnore:
            self.varIgnored_Chars_Boxes.append(StringVar())
            self.varIgnored_Chars_Check.append(BooleanVar())
            self.varIgnored_Chars_Boxes[i].set("")
            self.varIgnored_Chars_Check[i].set(True)
            self.containerIgnoreCharactersCheckBoxList.append(Checkbutton(self.containerIgnoreCharactersRows[rowNum]))
            self.containerIgnoreCharactersCheckBoxList[i]["variable"] = self.varIgnored_Chars_Check[i]
            self.containerIgnoreCharactersCheckBoxList[i]["background"] = buttonColour
            self.containerIgnoreCharactersTextBoxList.append(
                Entry(self.containerIgnoreCharactersRows[rowNum], textvariable=self.varIgnored_Chars_Boxes[i],
                      validate="focusout"))
            self.containerIgnoreCharactersTextBoxList[i].config(font=(self.fontType, self.fontSize), width=10)
            j = j + 1
            i = i + 1

        self.varIgnored_Chars_num = i

    def addReplaceWordsClick(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        rowNum = len(self.containerReplaceWordsRows)
        self.containerReplaceWordsRows.append(Frame(self.containerReplaceWords2.interior))
        j = 0
        i = self.varReplace_Words_num
        while j < self.numberOfColumnsReplace:
            self.varReplace_Words_1_Boxes.append(StringVar())
            self.varReplace_Words_2_Boxes.append(StringVar())
            self.varReplace_Words_Check.append(BooleanVar())
            self.varReplace_Words_1_Boxes[i].set("")
            self.varReplace_Words_2_Boxes[i].set("")
            self.varReplace_Words_Check[i].set(True)
            self.containerReplaceWordsCheckBoxList.append(Checkbutton(self.containerReplaceWordsRows[rowNum]))
            self.containerReplaceWordsCheckBoxList[i]["variable"] = self.varReplace_Words_Check[i]
            self.containerReplaceWordsCheckBoxList[i]["background"] = buttonColour
            self.containerReplaceWordsTextBoxList1.append(
                Entry(self.containerReplaceWordsRows[rowNum], textvariable=self.varReplace_Words_1_Boxes[i],
                      validate="focusout"))
            self.containerReplaceWordsTextBoxList2.append(
                Entry(self.containerReplaceWordsRows[rowNum], textvariable=self.varReplace_Words_2_Boxes[i],
                      validate="focusout"))
            self.containerReplaceWordsTextBoxList1[i].config(font=(self.fontType, self.fontSize))
            self.containerReplaceWordsTextBoxList2[i].config(font=(self.fontType, self.fontSize))
            j = j + 1
            i = i + 1

        self.varReplace_Words_num = i

    def addReplaceCharactersClick(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        rowNum = len(self.containerReplaceCharactersRows)
        self.containerReplaceCharactersRows.append(Frame(self.containerReplaceCharacters2.interior))
        j = 0
        i = self.varReplace_Chars_num
        while j < self.numberOfColumnsReplace:
            self.varReplace_Chars_1_Boxes.append(StringVar())
            self.varReplace_Chars_2_Boxes.append(StringVar())
            self.varReplace_Chars_Check.append(BooleanVar())
            self.varReplace_Chars_1_Boxes[i].set("")
            self.varReplace_Chars_2_Boxes[i].set("")
            self.varReplace_Chars_Check[i].set(True)
            self.containerReplaceCharactersCheckBoxList.append(Checkbutton(self.containerReplaceCharactersRows[rowNum]))
            self.containerReplaceCharactersCheckBoxList[i]["variable"] = self.varReplace_Chars_Check[i]
            self.containerReplaceCharactersCheckBoxList[i]["background"] = buttonColour
            self.containerReplaceCharactersTextBoxList1.append(
                Entry(self.containerReplaceCharactersRows[rowNum], textvariable=self.varReplace_Chars_1_Boxes[i],
                      validate="focusout"))
            self.containerReplaceCharactersTextBoxList2.append(
                Entry(self.containerReplaceCharactersRows[rowNum], textvariable=self.varReplace_Chars_2_Boxes[i],
                      validate="focusout"))
            self.containerReplaceCharactersTextBoxList1[i].config(font=(self.fontType, self.fontSize), width=10)
            self.containerReplaceCharactersTextBoxList2[i].config(font=(self.fontType, self.fontSize), width=10)
            j = j + 1
            i = i + 1

        self.varReplace_Chars_num = i

    def buttonAddIgnoreWordsClick(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.addIgnoreWordsClick()
        self.containerIgnoreWordsRows[len(self.containerIgnoreWordsRows) - 1].pack(side=TOP,
                                                                                   ipadx=buttons_frame_ipadx,
                                                                                   ipady=buttons_frame_ipady,
                                                                                   padx=buttons_frame_padx,
                                                                                   pady=buttons_frame_pady,
                                                                                   )
        i = self.varIgnored_Words_num - self.numberOfColumnsIgnore
        while i < self.varIgnored_Words_num:
            self.containerIgnoreWordsCheckBoxList[i].pack(side=LEFT)
            self.containerIgnoreWordsTextBoxList[i].pack(side=LEFT)
            i = i + 1

    def buttonAddIgnoreCharactersClick(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.addIgnoreCharactersClick()
        self.containerIgnoreCharactersRows[len(self.containerIgnoreCharactersRows) - 1].pack(side=TOP,
                                                                                             ipadx=buttons_frame_ipadx,
                                                                                             ipady=buttons_frame_ipady,
                                                                                             padx=buttons_frame_padx,
                                                                                             pady=buttons_frame_pady,
                                                                                             )
        i = self.varIgnored_Chars_num - self.numberOfColumnsIgnore
        while i < self.varIgnored_Chars_num:
            self.containerIgnoreCharactersCheckBoxList[i].pack(side=LEFT)
            self.containerIgnoreCharactersTextBoxList[i].pack(side=LEFT)
            i = i + 1

    def buttonAddReplaceWordsClick(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.addReplaceWordsClick()
        self.containerReplaceWordsRows[len(self.containerReplaceWordsRows) - 1].pack(side=TOP,
                                                                                     ipadx=buttons_frame_ipadx,
                                                                                     ipady=buttons_frame_ipady,
                                                                                     padx=buttons_frame_padx,
                                                                                     pady=buttons_frame_pady,
                                                                                     )
        i = self.varReplace_Words_num - self.numberOfColumnsReplace
        while i < self.varReplace_Words_num:
            self.containerReplaceWordsCheckBoxList[i].pack(side=LEFT)
            self.containerReplaceWordsTextBoxList1[i].pack(side=LEFT)
            self.containerReplaceWordsTextBoxList2[i].pack(side=LEFT)
            i = i + 1

    def buttonAddReplaceCharactersClick(self):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"

        self.addReplaceCharactersClick()
        self.containerReplaceCharactersRows[len(self.containerReplaceCharactersRows) - 1].pack(side=TOP,
                                                                                               ipadx=buttons_frame_ipadx,
                                                                                               ipady=buttons_frame_ipady,
                                                                                               padx=buttons_frame_padx,
                                                                                               pady=buttons_frame_pady,
                                                                                               )
        i = self.varReplace_Chars_num - self.numberOfColumnsReplace
        while i < self.varReplace_Chars_num:
            self.containerReplaceCharactersCheckBoxList[i].pack(side=LEFT)
            self.containerReplaceCharactersTextBoxList1[i].pack(side=LEFT)
            self.containerReplaceCharactersTextBoxList2[i].pack(side=LEFT)
            i = i + 1

    def updateGUIWordsListsFromParam(self, params):
        buttonColour = "white";
        button_width = 31
        button_padx = "1m"
        button_pady = "1m"
        buttons_frame_padx = "1m"
        buttons_frame_pady = "1m"
        buttons_frame_ipadx = "1m"
        buttons_frame_ipady = "1m"
        ignored_words_list = params['ignored_words']
        ignored_chars_list = params['ignored_chars']
        replaced_words_dict = params['replaced_words']
        replaced_chars_dict = params['replaced_chars']
        ignored_words_inactive_list = params['ignored_words_inactive']
        ignored_chars_inactive_list = params['ignored_chars_inactive']
        replaced_words_inactive_dict = params['replaced_words_inactive']
        replaced_chars_inactive_dict = params['replaced_chars_inactive']
        if params['remove_apostrophe']:
            ignored_chars_list.append("\'");
        for i in range(len(self.containerIgnoreWordsCheckBoxList)):
            self.containerIgnoreWordsCheckBoxList[i].destroy()
        for i in range(len(self.containerIgnoreWordsTextBoxList)):
            self.containerIgnoreWordsTextBoxList[i].destroy()
        for i in range(len(self.containerIgnoreCharactersCheckBoxList)):
            self.containerIgnoreCharactersCheckBoxList[i].destroy()
        for i in range(len(self.containerIgnoreCharactersTextBoxList)):
            self.containerIgnoreCharactersTextBoxList[i].destroy()

        for i in range(len(self.containerReplaceWordsCheckBoxList)):
            self.containerReplaceWordsCheckBoxList[i].destroy()
        for i in range(len(self.containerReplaceWordsTextBoxList1)):
            self.containerReplaceWordsTextBoxList1[i].destroy()
        for i in range(len(self.containerReplaceWordsTextBoxList2)):
            self.containerReplaceWordsTextBoxList2[i].destroy()
        for i in range(len(self.containerReplaceCharactersCheckBoxList)):
            self.containerReplaceCharactersCheckBoxList[i].destroy()
        for i in range(len(self.containerReplaceCharactersTextBoxList1)):
            self.containerReplaceCharactersTextBoxList1[i].destroy()
        for i in range(len(self.containerReplaceCharactersTextBoxList2)):
            self.containerReplaceCharactersTextBoxList2[i].destroy()

        for i in range(len(self.containerIgnoreWordsRows)):
            self.containerIgnoreWordsRows[i].destroy()
        for i in range(len(self.containerIgnoreCharactersRows)):
            self.containerIgnoreCharactersRows[i].destroy()
        for i in range(len(self.containerReplaceWordsRows)):
            self.containerReplaceWordsRows[i].destroy()
        for i in range(len(self.containerReplaceCharactersRows)):
            self.containerReplaceCharactersRows[i].destroy()
        self.varIgnored_Words_num = 0
        self.varIgnored_Words_Boxes = [];
        self.varIgnored_Words_Check = [];
        self.containerIgnoreWordsRows = []
        self.containerIgnoreWordsCheckBoxList = []
        self.containerIgnoreWordsTextBoxList = []
        self.varIgnored_Chars_num = 0
        self.varIgnored_Chars_Boxes = [];
        self.varIgnored_Chars_Check = [];
        self.containerIgnoreCharactersRows = []
        self.containerIgnoreCharactersCheckBoxList = []
        self.containerIgnoreCharactersTextBoxList = []
        self.varReplace_Words_num = 0
        self.varReplace_Words_1_Boxes = [];
        self.varReplace_Words_2_Boxes = [];
        self.varReplace_Words_Check = [];
        self.containerReplaceWordsRows = []
        self.containerReplaceWordsCheckBoxList = []
        self.containerReplaceWordsTextBoxList1 = []
        self.containerReplaceWordsTextBoxList2 = []
        self.varReplace_Chars_num = 0
        self.varReplace_Chars_1_Boxes = [];
        self.varReplace_Chars_2_Boxes = [];
        self.varReplace_Chars_Check = [];
        self.containerReplaceCharactersRows = []
        self.containerReplaceCharactersCheckBoxList = []
        self.containerReplaceCharactersTextBoxList1 = []
        self.containerReplaceCharactersTextBoxList2 = []

        numOfRows = int(
            math.ceil((len(ignored_words_list) + len(ignored_words_inactive_list)) / self.numberOfColumnsIgnore) + 1)
        i = 0
        self.varIgnored_Words_num = 0
        while i < numOfRows:
            self.addIgnoreWordsClick()
            i = i + 1
        i = 0
        for w in ignored_words_list:
            self.varIgnored_Words_Boxes[i].set(w)
            self.varIgnored_Words_Check[i].set(True)
            i = i + 1
        for w in ignored_words_inactive_list:
            self.varIgnored_Words_Boxes[i].set(w)
            self.varIgnored_Words_Check[i].set(False)
            i = i + 1

        numOfRows = int(
            math.ceil((len(ignored_chars_list) + len(ignored_chars_inactive_list)) / self.numberOfColumnsIgnore) + 1)
        i = 0
        self.varIgnored_Chars_num = 0
        while i < numOfRows:
            self.addIgnoreCharactersClick()
            i = i + 1
        i = 0
        for w in ignored_chars_list:
            self.varIgnored_Chars_Boxes[i].set(w)
            self.varIgnored_Chars_Check[i].set(True)
            i = i + 1
        for w in ignored_chars_inactive_list:
            self.varIgnored_Chars_Boxes[i].set(w)
            self.varIgnored_Chars_Check[i].set(False)
            i = i + 1

        numOfRows = int(math.ceil((len(replaced_words_dict.keys()) + len(
            replaced_words_inactive_dict.keys())) / self.numberOfColumnsReplace) + 1)
        i = 0
        self.varReplace_Words_num = 0
        while i < numOfRows:
            self.addReplaceWordsClick()
            i = i + 1
        i = 0
        for w in replaced_words_dict.keys():
            d = replaced_words_dict[w]
            self.varReplace_Words_1_Boxes[i].set(w)
            self.varReplace_Words_2_Boxes[i].set(d)
            self.varReplace_Words_Check[i].set(True)
            i = i + 1
        for w in replaced_words_inactive_dict.keys():
            d = replaced_words_inactive_dict[w]
            self.varReplace_Words_1_Boxes[i].set(w)
            self.varReplace_Words_2_Boxes[i].set(d)
            self.varReplace_Words_Check[i].set(False)
            i = i + 1

        numOfRows = int(math.ceil((len(replaced_chars_dict.keys()) + len(
            replaced_chars_inactive_dict.keys())) / self.numberOfColumnsReplace) + 1)
        i = 0
        self.varReplace_Chars_num = 0
        while i < numOfRows:
            self.addReplaceCharactersClick()
            i = i + 1
        i = 0
        for w in replaced_chars_dict.keys():
            d = replaced_chars_dict[w]
            self.varReplace_Chars_1_Boxes[i].set(w)
            self.varReplace_Chars_2_Boxes[i].set(d)
            self.varReplace_Chars_Check[i].set(True)
            i = i + 1
        for w in replaced_chars_inactive_dict.keys():
            d = replaced_chars_inactive_dict[w]
            self.varReplace_Chars_1_Boxes[i].set(w)
            self.varReplace_Chars_2_Boxes[i].set(d)
            self.varReplace_Chars_Check[i].set(False)
            i = i + 1
        self.varSelect_Ignored_Words.set(True);
        self.varSelect_Ignored_Chars.set(True);
        self.varSelect_Replace_Words.set(True);
        self.varSelect_Replace_Chars.set(True);

    def updateGUIFromParam(self, params, updateLists=True):
        prevSame = self.varSaveInSamePath.get()
        self.parametersDict = params;
        self.varSaveInSamePath.set(self.parametersDict['same_folder']);
        if prevSame != self.parametersDict['same_folder']:
            if self.parametersDict['same_folder']:
                self.buttonEntryOutputPath.config(state="disabled")
            else:
                self.buttonEntryOutputPath.config(state="normal")
                #        self.varCaseSensitive.set(self.parametersDict['case_sensitive']);
        # self.varInitial_connection_matrix.set(self.parametersDict['initial_connection_matrix']);
        self.varLoaded_connection_matrix.set(self.parametersDict['loaded_connection_matrix']);
        self.varIs_mem_cap.set(self.parametersDict['is_mem_cap'])

        if (self.parametersDict['is_keep_reading_max'] == True):
            self.var_keep_max.set("keep reading max")
        elif(self.parametersDict['is_keep_cap_max'] == True):
            self.var_keep_max.set("keep CAP max")
        elif(self.parametersDict['is_without_correction'] == True):
            self.var_keep_max.set("Without correction")


        # self.varIs_keep_reading_max.set(self.parametersDict['is_keep_reading_max'])
        # self.varIs_keep_cap_max.set(self.parametersDict['is_keep_cap_max'])
        # self.varSelf_activation.set(self.parametersDict['self_activation']);
        self.varForward_activation.set(self.parametersDict['forward_activation']);
        self.varBlank_cycle.set(self.parametersDict['blank_cycle']);
        self.varNegative_connections.set(self.parametersDict['negative_connections']);
        self.varReduced_expectancy.set(self.parametersDict['reduced_expectancy']);
        if self.parametersDict['sigmoid_connections']:
            self.varSigmoid_connections.set(1);
            self.varLinear_function.set(0);
        else:
            self.varSigmoid_connections.set(0);
            self.varLinear_function.set(1);
        self.varNumbers_replace.set(self.parametersDict['numbers_replace']);
        self.varOrdinals_replace.set(self.parametersDict['ordinals_replace']);
        self.varLimit_cycles.set(self.parametersDict['limit_cycles']);
        self.varWord_is_cycle.set(self.parametersDict['word_is_cycle']);
        self.varContractions_replace.set(self.parametersDict['contractions_replace']);
        #        self.varAppend_DateTime_To_Output.set(self.parametersDict['append_datetime']);
        self.varShow_All_Columns.set(self.parametersDict['show_all_columns']);
        i2 = 0;
        for i in self.varShowColumns:
            i.set(self.parametersDict['show_columns'][i2]);
            i2 = i2 + 1;

        self.varDecimal_round.set(self.parametersDict['decimal_round']);
        self.varDecimal_roundStr.set(str(self.varDecimal_round.get()));
        self.varLsaChoice.set(self.parametersDict['lsaChoice']);
        self.varLsaChoiceStr.set(self.lsaDictReverse[self.varLsaChoice.get()]);
        self.varIteration_limit.set(self.parametersDict['iteration_limit']);
        self.varIteration_limitStr.set(str(self.varIteration_limit.get()));
        self.varM.set(self.parametersDict['m']);
        if self.varM.get() % 1 == 0:
            self.varMStr.set(str(int(self.varM.get())));
        else:
            self.varMStr.set(str(self.varM.get()));

        self.varActivation_threshold.set(self.parametersDict['activation_threshold']);
        if self.varActivation_threshold.get() % 1 == 0:
            self.varActivation_thresholdStr.set(str(int(self.varActivation_threshold.get())));
        else:
            self.varActivation_thresholdStr.set(str(self.varActivation_threshold.get()));

        self.varConnection_threshold.set(self.parametersDict['connection_threshold']);
        if self.varConnection_threshold.get() % 1 == 0:
            self.varConnection_thresholdStr.set(str(int(self.varConnection_threshold.get())));
        else:
            self.varConnection_thresholdStr.set(str(self.varConnection_threshold.get()));

        self.varWorking_mem_cap.set(self.parametersDict['working_mem_cap']);
        if self.varWorking_mem_cap.get() % 1 == 0:
            self.varWorking_mem_capStr.set(str(int(self.varWorking_mem_cap.get())));
        else:
            self.varWorking_mem_capStr.set(str(self.varWorking_mem_cap.get()));
        self.varLearning_rate.set(self.parametersDict['learning_rate']);
        if self.varLearning_rate.get() % 1 == 0:
            self.varLearning_rateStr.set(str(int(self.varLearning_rate.get())));
        else:
            self.varLearning_rateStr.set(str(self.varLearning_rate.get()));
        self.varCohort.set(self.parametersDict['cohort']);
        if self.varCohort.get() % 1 == 0:
            self.varCohortStr.set(str(int(self.varCohort.get())));
        else:
            self.varCohortStr.set(str(self.varCohort.get()));
        self.varDecay.set(self.parametersDict['decay']);
        if self.varDecay.get() % 1 == 0:
            self.varDecayStr.set(str(int(self.varDecay.get())));
        else:
            self.varDecayStr.set(str(self.varDecay.get()));
        self.varSemantic_weight.set(self.parametersDict['semantic_weight']);
        if self.varSemantic_weight.get() % 1 == 0:
            self.varSemantic_weightStr.set(str(int(self.varSemantic_weight.get())));
        else:
            self.varSemantic_weightStr.set(str(self.varSemantic_weight.get()));
        self.varEpisodic_weight.set(self.parametersDict['episodic_weight']);
        if self.varEpisodic_weight.get() % 1 == 0:
            self.varEpisodic_weightStr.set(str(int(self.varEpisodic_weight.get())));
        else:
            self.varEpisodic_weightStr.set(str(self.varEpisodic_weight.get()));
        self.varSigmoid_min.set(self.parametersDict['sigmoid_min']);
        if self.varSigmoid_min.get() % 1 == 0:
            self.varSigmoid_minStr.set(str(int(self.varSigmoid_min.get())));
        else:
            self.varSigmoid_minStr.set(str(self.varSigmoid_min.get()));
        self.varSigmoid_max.set(self.parametersDict['sigmoid_max']);
        if self.varSigmoid_max.get() % 1 == 0:
            self.varSigmoid_maxStr.set(str(int(self.varSigmoid_max.get())));
        else:
            self.varSigmoid_maxStr.set(str(self.varSigmoid_max.get()));

        self.varInput_Path.set(self.parametersDict['input_path']);
        self.varOutput_Path.set(self.parametersDict['output_path']);
        #        self.varOutput_Name.set(self.parametersDict['output_name']);
        #        self.varReset_Configuration_Path.set(self.parametersDict['reset_config_path']);
        self.varSave_Configuration_Path.set(self.parametersDict['save_config_path']);
        self.varLoad_Configuration_Path.set(self.parametersDict['load_config_path']);
        self.varLoad_Connections_Path.set(self.parametersDict['connection_matrix_path']);

        if updateLists:
            self.updateGUIWordsListsFromParam(params)

    def updateParamFromGUI(self):
        self.changeDecimal_round();
        self.changeLsaChoice();
        self.changeWorking_mem_cap();
        self.changeM();
        self.changeLearning_rate();
        self.changeCohort();
        self.changeDecay();
        self.changeActivation_threshold();
        self.changeConnection_threshold()
        self.changeSemantic_weight();
        self.changeEpisodic_weight();
        self.changeSigmoid_min();
        self.changeSigmoid_max();
        self.changeIteration_limit();
        self.parametersDict['same_folder'] = self.varSaveInSamePath.get();
        #        self.parametersDict['case_sensitive'] = self.varCaseSensitive.get();
        # self.parametersDict['initial_connection_matrix'] = self.varInitial_connection_matrix.get()
        self.parametersDict['loaded_connection_matrix'] = self.varLoaded_connection_matrix.get()
        self.parametersDict['connection_matrix_path'] = self.varLoad_Connections_Path.get()
        self.parametersDict['is_mem_cap'] = self.varIs_mem_cap.get()
        value = self.var_keep_max.get()
        if (value == "keep reading max"):
            self.parametersDict['is_keep_reading_max'] = True
            self.parametersDict['is_keep_cap_max'] = False
            self.parametersDict['is_without_correction'] = False
        elif (value == "keep CAP max"):
            self.parametersDict['is_keep_cap_max'] = True
            self.parametersDict['is_keep_reading_max'] = False
            self.parametersDict['is_without_correction'] = False
        elif (value == "without correction"):
            self.parametersDict['is_keep_cap_max'] = False
            self.parametersDict['is_keep_reading_max'] = False
            self.parametersDict['is_without_correction'] = True
        # self.parametersDict['is_keep_reading_max'] = self.varIs_keep_reading_max.get()
        # self.parametersDict['is_keep_cap_max'] = self.varIs_keep_cap_max.get()
        # self.parametersDict['self_activation'] = self.varSelf_activation.get();
        self.parametersDict['forward_activation'] = self.varForward_activation.get();
        self.parametersDict['blank_cycle'] = self.varBlank_cycle.get();
        self.parametersDict['negative_connections'] = self.varNegative_connections.get();
        self.parametersDict['reduced_expectancy'] = self.varReduced_expectancy.get();
        if self.varSigmoid_connections.get() == 1:
            self.parametersDict['linear_function'] = False;
            self.parametersDict['sigmoid_connections'] = True;
        else:
            self.parametersDict['linear_function'] = True;
            self.parametersDict['sigmoid_connections'] = False;
        self.parametersDict['numbers_replace'] = self.varNumbers_replace.get();
        self.parametersDict['ordinals_replace'] = self.varOrdinals_replace.get();
        self.parametersDict['limit_cycles'] = self.varLimit_cycles.get();
        self.parametersDict['word_is_cycle'] = self.varWord_is_cycle.get();
        self.parametersDict['contractions_replace'] = self.varContractions_replace.get();
        #        self.parametersDict['append_datetime'] = self.varAppend_DateTime_To_Output.get();
        self.parametersDict['show_all_columns'] = self.varShow_All_Columns.get();
        i2 = 0;
        for i in self.varShowColumns:
            self.parametersDict['show_columns'][i2] = i.get();
            i2 = i2 + 1;

        self.parametersDict['decimal_round'] = self.varDecimal_round.get();
        self.parametersDict['lsaChoice'] = self.varLsaChoice.get();
        self.parametersDict['iteration_limit'] = self.varIteration_limit.get();
        self.parametersDict['m'] = self.varM.get();
        self.parametersDict['activation_threshold'] = self.varActivation_threshold.get();
        self.parametersDict['connection_threshold'] = self.varConnection_threshold.get()
        self.parametersDict['working_mem_cap'] = self.varWorking_mem_cap.get();
        self.parametersDict['learning_rate'] = self.varLearning_rate.get();
        self.parametersDict['cohort'] = self.varCohort.get();
        self.parametersDict['decay'] = self.varDecay.get();
        self.parametersDict['semantic_weight'] = self.varSemantic_weight.get();
        self.parametersDict['episodic_weight'] = self.varEpisodic_weight.get();
        self.parametersDict['sigmoid_min'] = self.varSigmoid_min.get();
        self.parametersDict['sigmoid_max'] = self.varSigmoid_max.get();
        self.parametersDict['error_flag'] = 0;
        self.parametersDict['error_text'] = "";
        self.parametersDict["summary_text"] = ''

        self.parametersDict['input_path'] = self.varInput_Path.get();
        self.parametersDict['output_path'] = self.varOutput_Path.get();
        #        self.parametersDict['output_name'] = self.varOutput_Name.get();
        #        self.parametersDict['reset_config_path'] = self.varReset_Configuration_Path.get();
        self.parametersDict['save_config_path'] = self.varSave_Configuration_Path.get();
        self.parametersDict['load_config_path'] = self.varLoad_Configuration_Path.get();

        self.parametersDict['ignored_words'] = []
        self.parametersDict['ignored_words_inactive'] = []
        i = 0
        for w in self.varIgnored_Words_Boxes:
            wSTR = str(w.get())
            if len(wSTR) > 0:
                if self.varIgnored_Words_Check[i].get():
                    self.parametersDict['ignored_words'].append(wSTR)
                else:
                    self.parametersDict['ignored_words_inactive'].append(wSTR)
            i = i + 1

        self.parametersDict['remove_apostrophe'] = False;
        self.parametersDict['ignored_chars'] = []
        self.parametersDict['ignored_chars_inactive'] = []
        i = 0
        for w in self.varIgnored_Chars_Boxes:
            wSTR = str(w.get())
            if wSTR == "\'" or (wSTR == "`" or wSTR == "’"):
                wSTR = "\'"
            if len(wSTR) > 0:
                if self.varIgnored_Chars_Check[i].get():
                    if wSTR == "\'":
                        self.parametersDict['remove_apostrophe'] = True;
                    else:
                        self.parametersDict['ignored_chars'].append(wSTR)
                else:
                    self.parametersDict['ignored_chars_inactive'].append(wSTR)
            i = i + 1

        self.parametersDict['replaced_words'] = {}
        self.parametersDict['replaced_words_inactive'] = {}
        i = 0
        for w in self.varReplace_Words_1_Boxes:
            wSTR = str(w.get())
            if len(wSTR) > 0:
                if self.varReplace_Words_Check[i].get():
                    self.parametersDict['replaced_words'][wSTR] = str(self.varReplace_Words_2_Boxes[i].get())
                else:
                    self.parametersDict['replaced_words_inactive'][wSTR] = str(self.varReplace_Words_2_Boxes[i].get())
            i = i + 1

        self.parametersDict['replaced_chars'] = {}
        self.parametersDict['replaced_chars_inactive'] = {}
        i = 0
        for w in self.varReplace_Chars_1_Boxes:
            wSTR = str(w.get())
            if len(wSTR) > 0:
                if self.varReplace_Chars_Check[i].get():
                    self.parametersDict['replaced_chars'][wSTR] = str(self.varReplace_Chars_2_Boxes[i].get())
                else:
                    self.parametersDict['replaced_chars_inactive'][wSTR] = str(self.varReplace_Chars_2_Boxes[i].get())
            i = i + 1
        return self.parametersDict;

    def updateParamFromGUIMain(self):
        self.parametersDict['same_folder'] = self.varSaveInSamePath.get();
        #        self.parametersDict['append_datetime'] = self.varAppend_DateTime_To_Output.get();

        self.parametersDict['input_path'] = self.varInput_Path.get();
        self.parametersDict['output_path'] = self.varOutput_Path.get();
        #        self.parametersDict['output_name'] = self.varOutput_Name.get();
        #        self.parametersDict['reset_config_path'] = self.varReset_Configuration_Path.get();
        self.parametersDict['save_config_path'] = self.varSave_Configuration_Path.get();
        self.parametersDict['load_config_path'] = self.varLoad_Configuration_Path.get();

        return self.parametersDict;

    def updateParamFromGUIParameters(self):
        self.changeDecimal_round();
        self.changeLsaChoice();
        self.changeWorking_mem_cap();
        self.changeM();
        self.changeLearning_rate();
        self.changeCohort();
        self.changeDecay();
        self.changeActivation_threshold();
        self.changeConnection_threshold()
        self.changeSemantic_weight();
        self.changeEpisodic_weight();
        self.changeSigmoid_min();
        self.changeSigmoid_max();
        self.changeIteration_limit();
        #        self.parametersDict['case_sensitive'] = self.varCaseSensitive.get();
        # self.parametersDict['initial_connection_matrix'] = self.varInitial_connection_matrix.get();
        self.parametersDict['loaded_connection_matrix'] = self.varLoaded_connection_matrix.get();
        self.parametersDict['connection_matrix_path'] = self.varLoad_Connections_Path.get();
        self.parametersDict['is_mem_cap'] = self.varIs_mem_cap.get();
        value = self.var_keep_max.get()
        if (value == "keep reading max"):
            self.parametersDict['is_keep_reading_max'] = True
            self.parametersDict['is_keep_cap_max'] = False
            self.parametersDict['is_without_correction'] = False
        elif (value == "keep CAP max"):
            self.parametersDict['is_keep_cap_max'] = True
            self.parametersDict['is_keep_reading_max'] = False
            self.parametersDict['is_without_correction'] = False
        elif (value == "Without correction"):
            self.parametersDict['is_keep_cap_max'] = False
            self.parametersDict['is_keep_reading_max'] = False
            self.parametersDict['is_without_correction'] = True
        # self.parametersDict['is_keep_reading_max'] = self.varIs_keep_reading_max.get()
        # self.parametersDict['is_keep_cap_max'] = self.varIs_keep_cap_max.get()
        # self.parametersDict['self_activation'] = self.varSelf_activation.get();
        self.parametersDict['forward_activation'] = self.varForward_activation.get();
        self.parametersDict['blank_cycle'] = self.varBlank_cycle.get();
        self.parametersDict['negative_connections'] = self.varNegative_connections.get();
        self.parametersDict['reduced_expectancy'] = self.varReduced_expectancy.get();
        if self.varSigmoid_connections.get() == 1:
            self.parametersDict['linear_function'] = False;
            self.parametersDict['sigmoid_connections'] = True;
        else:
            self.parametersDict['linear_function'] = True;
            self.parametersDict['sigmoid_connections'] = False;
        self.parametersDict['limit_cycles'] = self.varLimit_cycles.get();
        self.parametersDict['word_is_cycle'] = self.varWord_is_cycle.get();

        self.parametersDict['decimal_round'] = self.varDecimal_round.get();
        self.parametersDict['lsaChoice'] = self.varLsaChoice.get();
        self.parametersDict['iteration_limit'] = self.varIteration_limit.get();
        self.parametersDict['m'] = self.varM.get();
        self.parametersDict['activation_threshold'] = self.varActivation_threshold.get();
        self.parametersDict['connection_threshold'] = self.varConnection_threshold.get()
        self.parametersDict['working_mem_cap'] = self.varWorking_mem_cap.get();
        self.parametersDict['learning_rate'] = self.varLearning_rate.get();
        self.parametersDict['cohort'] = self.varCohort.get();
        self.parametersDict['decay'] = self.varDecay.get();
        self.parametersDict['semantic_weight'] = self.varSemantic_weight.get();
        self.parametersDict['episodic_weight'] = self.varEpisodic_weight.get();
        self.parametersDict['sigmoid_min'] = self.varSigmoid_min.get();
        self.parametersDict['sigmoid_max'] = self.varSigmoid_max.get();

        return self.parametersDict;

    def updateParamFromGUIOutputColumns(self):
        self.parametersDict['show_all_columns'] = self.varShow_All_Columns.get();
        i2 = 0;
        for i in self.varShowColumns:
            self.parametersDict['show_columns'][i2] = i.get();
            i2 = i2 + 1;
        return self.parametersDict;

    def updateParamFromGUIIgnoredWords(self):

        self.parametersDict['ignored_words'] = []
        self.parametersDict['ignored_words_inactive'] = []
        i = 0
        for w in self.varIgnored_Words_Boxes:
            wSTR = str(w.get())
            if len(wSTR) > 0:
                if self.varIgnored_Words_Check[i].get():
                    self.parametersDict['ignored_words'].append(wSTR)
                else:
                    self.parametersDict['ignored_words_inactive'].append(wSTR)
            i = i + 1
        return self.parametersDict;

    def updateParamFromGUIIgnoredChars(self):
        self.parametersDict['remove_apostrophe'] = False;
        self.parametersDict['ignored_chars'] = []
        self.parametersDict['ignored_chars_inactive'] = []
        i = 0
        for w in self.varIgnored_Chars_Boxes:
            wSTR = str(w.get())
            if wSTR == "\'" or (wSTR == "`" or wSTR == "’"):
                wSTR = "\'"
            if len(wSTR) > 0:
                if self.varIgnored_Chars_Check[i].get():
                    if wSTR == "\'":
                        self.parametersDict['remove_apostrophe'] = True;
                    else:
                        self.parametersDict['ignored_chars'].append(wSTR)
                else:
                    self.parametersDict['ignored_chars_inactive'].append(wSTR)
            i = i + 1
        return self.parametersDict;

    def updateParamFromGUIReplacedWords(self):
        self.parametersDict['numbers_replace'] = self.varNumbers_replace.get();
        self.parametersDict['ordinals_replace'] = self.varOrdinals_replace.get();
        self.parametersDict['contractions_replace'] = self.varContractions_replace.get();
        self.parametersDict['replaced_words'] = {}
        self.parametersDict['replaced_words_inactive'] = {}
        i = 0
        for w in self.varReplace_Words_1_Boxes:
            wSTR = str(w.get())
            if len(wSTR) > 0:
                if self.varReplace_Words_Check[i].get():
                    self.parametersDict['replaced_words'][wSTR] = str(self.varReplace_Words_2_Boxes[i].get())
                else:
                    self.parametersDict['replaced_words_inactive'][wSTR] = str(self.varReplace_Words_2_Boxes[i].get())
            i = i + 1
        return self.parametersDict;

    def updateParamFromGUIReplaceCharacters(self):
        self.parametersDict['replaced_chars'] = {}
        self.parametersDict['replaced_chars_inactive'] = {}
        i = 0
        for w in self.varReplace_Chars_1_Boxes:
            wSTR = str(w.get())
            if len(wSTR) > 0:
                if self.varReplace_Chars_Check[i].get():
                    self.parametersDict['replaced_chars'][wSTR] = str(self.varReplace_Chars_2_Boxes[i].get())
                else:
                    self.parametersDict['replaced_chars_inactive'][wSTR] = str(self.varReplace_Chars_2_Boxes[i].get())
            i = i + 1
        return self.parametersDict;


class VerticalScrolledFrame(Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling
    """

    def __init__(self, parent, *args, **kw):
        Frame.__init__(self, parent, *args, **kw)

        # create a canvas object and a vertical scrollbar for scrolling it
        self.vscrollbar = Scrollbar(self, orient=VERTICAL)
        self.vscrollbar.pack(fill=Y, side=RIGHT, expand=FALSE)
        canvas = Canvas(self, bd=0, highlightthickness=0,
                        yscrollcommand=self.vscrollbar.set)
        canvas.pack(side=LEFT, fill=BOTH, expand=TRUE)
        self.vscrollbar.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior,
                                           anchor=NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())

        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())

        canvas.bind('<Configure>', _configure_canvas)


def LSALevelCheck(sOriginal):
    ansBool = True
    s = sOriginal
    sLen = len(s)
    if sLen > 0:
        if not RepresentsInt(s):
            ansBool = False
        else:
            if not int(s) in [1, 3, 6, 12]:
                ansBool = False
    return ansBool


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def RepresentsFloat(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def NumberCheck(sOriginal, floatBool):
    ansBool = True
    s = sOriginal
    sLen = len(s)
    if sLen > 0:
        if sLen > 1:
            if floatBool:
                if not RepresentsFloat(s):
                    if s[0] == '-' or s[0] == '+':
                        s = s[1:sLen]
                        sLen = sLen - 1
                    if s[sLen - 1] == '.':
                        s = s[0:sLen - 1]
                        sLen = sLen - 1
                        if sLen > 0:
                            if not RepresentsInt(s):
                                ansBool = False
                    else:
                        if sLen > 0:
                            if not RepresentsFloat(s):
                                ansBool = False
            else:
                if s[0] == '+':
                    s = s[1:sLen]
                if not RepresentsInt(s):
                    ansBool = False
        else:
            if not RepresentsInt(s):
                if (s != '.' or not floatBool) and (s != '+' and s != '-'):
                    ansBool = False
    return ansBool


def sortListOfWords(list1, list2=[]):
    n = len(list1)
    useList2 = (len(list2) > n - 1)
    indexOrder1 = []
    if n > 0:
        indexOrder1 = []
        indexOrder2 = []
        for b in range(n):
            indexOrder1.append(b)
            indexOrder2.append(b)
        m = 1
        while m < n + 1:
            a = 0
            c = 0
            while a < n:
                i = a
                j = a + m
                while (i < a + m and i < n) and (j < a + 2 * m and j < n):
                    iW = list1[indexOrder1[i]].get()
                    jW = list1[indexOrder1[j]].get()
                    if iW == jW and useList2:
                        iW2 = list2[indexOrder1[i]].get()
                        jW2 = list2[indexOrder1[j]].get()
                        if iW2 != "" and jW2 != "":
                            jSmaller = (jW2 < iW2)
                        else:
                            jSmaller = (iW2 == "")
                    else:
                        if iW != "" and jW != "":
                            jSmaller = (jW < iW)
                        else:
                            jSmaller = (iW == "")
                    if jSmaller:
                        indexOrder2[c] = indexOrder1[j]
                        j = j + 1
                    else:
                        indexOrder2[c] = indexOrder1[i]
                        i = i + 1
                    c = c + 1
                while i < a + m and i < n:
                    indexOrder2[c] = indexOrder1[i]
                    i = i + 1
                    c = c + 1
                while j < a + 2 * m and j < n:
                    indexOrder2[c] = indexOrder1[j]
                    j = j + 1
                    c = c + 1
                a = a + 2 * m
            a = 0
            while a < n:
                indexOrder1[a] = indexOrder2[a]
                a = a + 1
            m = 2 * m
    return indexOrder1


errorFlag = False
iconsFlag = True
if not os.path.isfile("Default_Parameters1.txt"):
    params = lsaP.resetParams()
    if params['error_flag'] == 2:
        errorFlag = True
    else:
        errorFlag = lsaP.saveConfigurationFile("Default_Parameters1.txt", params)
resDocxDefault = lsaI.createDefaultDocx()
resIconDefault = lsaI.createIcons()
if resIconDefault != 1:
    iconsFlag = False

if not errorFlag:
    params = lsaP.configure("Default_Parameters1.txt")
    lsaGUIroot = Tk()
    mylsaGUI = LsaGUIForm(lsaGUIroot, params, iconsFlag)
    lsaGUIroot.option_add('*Dialog.msg.font', 'Helvetica 16')
    lsaGUIroot.wm_title("LS-R")
    w, h = lsaGUIroot.winfo_screenwidth(), lsaGUIroot.winfo_screenheight()
    wCenter = w // 2
    hCenter = h // 2
    w = (w * 4) // 5
    h = (h * 4) // 5
    lsaGUIroot.geometry("%dx%d+%d+%d" % (w, h, int(wCenter - w // 2), int(hCenter - h // 2)))
    # make it cover the entire screen
    print('\n\n... Loaded.\n\n')
    win = win32console.GetConsoleWindow()
    win32gui.ShowWindow(win, 0)
    lsaGUIroot.mainloop()
else:
    print("\n\nError: Cannot create parameters file.\n\n")
