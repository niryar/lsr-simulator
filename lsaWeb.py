import urllib.request
import urllib.parse
import openpyxl
from openpyxl import Workbook
from openpyxl.styles import Font, Alignment
from openpyxl.utils import get_column_letter

def space_choice(num):
    my_str = ''

    if num == 3:
        my_str = 'General_Reading_up_to_03rd_Grade (300 factors)'

    elif num == 6:
        my_str = 'General_Reading_up_to_06th_Grade (300 factors)'

    elif num == 9:
        my_str = 'General_Reading_up_to_09th_Grade (300 factors)'

    elif num == 12:
        my_str = 'General_Reading_up_to_12th_Grade (300 factors)'

    else:
        my_str = 'General_Reading_up_to_1st_year_college (300 factors)'

    return my_str


def download(wordsList, choice):
    # if('–' in words):
    #     words.remove('–')
    # if ('-' in words):
    #     words.remove('-')
    words = []
    for word in wordsList:
        getVals = list([val for val in word if val.isalpha() or val.isnumeric() or val == '–' or val == '-'])
        result = "".join(getVals)
        words.append(result)

    data = urllib.parse.urlencode({
         'txt1': '\r\n\r\n'.join(words),
         'LSAFactors': '300',

         # TODO add document to document option
         'CmpType': 'term2term',
         'LSAspace': space_choice(choice),
    }).encode("utf-8")
    lsaAns = urllib.request.urlopen('http://lsa.colorado.edu/cgi-bin/LSA-matrix-x.html', data)
    # lsaAns = urllib.request.urlopen('http://lsa.colorado.edu/cgi-bin/LSA-matrix.html', data)
    msg = lsaAns.info()

    code = lsaAns.getcode()
    wordRowIndex = 0
    rows = []
    for line in lsaAns:
        realRow = False
        wordColumnIndex = 0
        line = line.strip()
        line = bytes.decode(line)
        if line.startswith('<TR>'):
            line = line[4:]
            row = []
            for cell in line.split('<TD ALIGN=CENTER>'):
                cell = cell.strip()
                if cell:
                    # if cell is a number, convert to float and append
                    if cell != 'Document' and cell not in words:
                        realRow = True
                        row.append(cell)
                        wordColumnIndex = wordColumnIndex + 1
        if realRow:
            rows.append(row)
            wordRowIndex = wordRowIndex + 1
    return rows

def loadMatrix(words, path, caseSensitive=False):
    wordNum = len(words)
    wsName = 'Connections'
    try:
        wb = openpyxl.load_workbook(path)
    except Exception:
        return []
    if not wsName in wb.sheetnames:
        wsName = 'connections'
        if not wsName in wb.sheetnames:
            wsName = 'Sheet1'
            if not wsName in wb.sheetnames:
                wsName = ''
    if wsName == '':
        ws = wb.active
    else:
        ws= wb[wsName]
    wordsColumn = {}
    wordsRow = {}
    rowMax = ws.max_row
    columnMax = ws.max_column
    rowOffset = ws.max_row
    columnOffset = ws.max_column
    contBool = True
    while rowOffset > 0 and contBool:
        contBool = ws.cell(row=rowOffset, column=columnMax).value
        if contBool:
            rowOffset = rowOffset - 1
    if contBool:
        rowOffset = rowOffset + 1
    contBool = True
    while columnOffset > 0 and contBool:
        contBool = ws.cell(row=rowMax, column=columnOffset).value
        if contBool:
            columnOffset = columnOffset - 1
    if contBool:
        columnOffset = columnOffset + 1

    while columnMax > columnOffset - 1:
        currentWord = str(ws.cell(row=rowOffset, column=columnMax).value)
        if not caseSensitive:
            currentWord = currentWord.lower()
        wordsColumn[currentWord] = columnMax
        columnMax = columnMax - 1
    while rowMax > rowOffset - 1:
        currentWord = str(ws.cell(row=rowMax, column=columnOffset).value)
        if not caseSensitive:
            currentWord = currentWord.lower()
        wordsRow[currentWord] = rowMax
        rowMax = rowMax - 1
        
    rows =[ ['N/A'] * wordNum for i in range(wordNum)]
    i = 0
    while i < wordNum:
        if words[i] in wordsRow:
            iRow = wordsRow[words[i]]
            j = 0
            while j < wordNum:
                if words[j] in wordsColumn:
                    jColumn = wordsColumn[words[j]]
                    rows[i][j] = str(ws.cell(row=iRow, column=jColumn).value)
                j = j + 1
        i = i + 1
    return rows
